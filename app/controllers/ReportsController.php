<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\Model\Query;

class ReportsController extends Controller 
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}
	
	public function indexAction()
	{
		$this->view->setVar('header_title', "Reports");


		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////TRUCK LIST
      
        $truck      = Trucks::find();

        foreach ($truck as $dbtruck) {
        	$data_trucks[] = array(
				'plateno'=> $dbtruck->plateno ,
				'length' => $dbtruck->length,
				'width' => $dbtruck->width,
				'heigth' => $dbtruck->heigth,
				'company' => $dbtruck->company,
				);
        }
      
        $this->view->dbtruck= json_decode(json_encode($data_trucks));


        function ls_trucks($model,$column){
            $sand = $model::find();
            $data = array();
            foreach ($sand as $m) {
                $data[] = array(
                    "id" => $m->id,
                    $column => $m->$column,
                );
            }
            return json_decode(json_encode($data));
        }
        $this->view->data_company = ls_trucks("Company","campname"); //LIST SAND TYPE
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /////DAILY REPORT
         $this->view->from ="";//DISPLAY VALUE from********************
         $this->view->to ="";//DISPLAY VALUE from********************
         $this->view->dspval ="";//DISPLAY VALUE from********************
         $this->view->disabled = "disabled" ;//DISABLED BOTTN********************
         $this->view->note = "" ;//Notification********************
         if ($this->request->isPost('showrpt') == true) {
     		 
         	$from= $this->request->getPost('from');//POST from
		 	$this->view->from = $from ;//DISPLAY VALUE from********************
		 	$to= $this->request->getPost('to');//POST to
		 	$this->view->to = $to ;//DISPLAY VALUE to********************

		 	 $this->view->dspval ="displaydaily";//DISPLAY VALUE from********************

		 	// echo $from ."<br>". $to."<br>";

		 	if($from>$to){
		 		$this->view->note = "To Date Must Be Greater than From Date" ;
		 		$this->view->dborder= json_decode(json_encode($data_orders=array()));
		 	}else{
		 		$this->view->disabled = "";
		 		$query  = $this->modelsManager->createQuery("SELECT * FROM orders WHERE orders.date >= '$from' and orders.date <= '$to'");
				$orderdate   = $query->execute();
				

		 		// $orderdate=Orders::find(array($condition));
		 		// // echo count($orderdate);
		 		foreach ( $orderdate as $daily){
		 			$data_orders[] = array(
					'plateno'=> $daily->plateno ,
					'company' => $daily->company,
					'sand' => $daily->sand,
					'cumtr' => $daily->cumtr,
					'pricecumtr' => $daily->pricecumtr,
					'cash'=> $daily->cash ,
					'poamount'=> $daily->poamount ,
					'discount'=> $daily->discount ,
					'drno'=> $daily->drno ,
					'time'=> $daily->time ,
					'date'=> $daily->date ,				);
		 		}
		 		$this->view->dborder= json_decode(json_encode($data_orders));
		 	}

         }else{
         		
         	$this->view->dborder= json_decode(json_encode($data_orders=array()));
         }


	}

}
