%{Phalcon_Text_4c920f648cca4243cd1116fd9fc828f8}%
================================

%{Phalcon_Text_93f3caa67be7b3f43bc08b2548ba5eae}%

%{Phalcon_Text_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Text_04011a9167262c637a8a0828f9c0de3e}%

%{Phalcon_Text_18f7dbfdc8632fdf077c38aac27f8654}%

%{Phalcon_Text_d38367a562c5b66deebecdbb14b403de}%

%{Phalcon_Text_38254b7fef728c7429095822b6965117}%

%{Phalcon_Text_35a1b502874e971381fe8334c8d8aafa}%

%{Phalcon_Text_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Text_04e080613548e681cea7c7cf3547878e}%

%{Phalcon_Text_13d4f6155665fba6326ffe393b535f67}%

.. code-block:: php

    <?php

    echo Phalcon\Text::camelize('coco_bongo'); //{%Phalcon_Text_f2eeb7d17ef9facad361c97f784fbd4a%}





%{Phalcon_Text_dde140dd4ed492c7a523376075bb4f9f}%

%{Phalcon_Text_c92a7eb48e1a6f0057de4ede0590be7e}%

.. code-block:: php

    <?php

    echo Phalcon\Text::uncamelize('CocoBongo'); //{%Phalcon_Text_ba46771e30a408bd588e42e70002afd8%}





%{Phalcon_Text_e6f603a5ffe22963031b38f59de991ec}%

%{Phalcon_Text_8d99693d6f2320ba55ad15a7b8fea02c}%

.. code-block:: php

    <?php

    echo Phalcon\Text::increment("a"); // {%Phalcon_Text_46bd3ad6544029e9f153ca4d28fb959a%}
    echo Phalcon\Text::increment("a_1"); // {%Phalcon_Text_6c209e630a0375d3df0120ff39f2acf2%}





%{Phalcon_Text_ccf9ee069e9e52dd7b502a514dd0da4f}%

%{Phalcon_Text_5ca7b6b0b27175fc172bbc1597470280}%

.. code-block:: php

    <?php

    echo Phalcon\Text::random(Phalcon\Text::RANDOM_ALNUM); //{%Phalcon_Text_75b56023b4a599d1b42657cd2a2e68a4%}





%{Phalcon_Text_12b8bfcaac17033c1f46d86f700d8f9d}%

%{Phalcon_Text_61ce8c6bb39f3740ad80ad149fb8fa4c}%

.. code-block:: php

    <?php

    echo Phalcon\Text::startsWith("Hello", "He"); // {%Phalcon_Text_b326b5062b2f0e69046810717534cb09%}
    echo Phalcon\Text::startsWith("Hello", "he"); // {%Phalcon_Text_68934a3e9455fa72420237eb05902327%}
    echo Phalcon\Text::startsWith("Hello", "he", false); // {%Phalcon_Text_b326b5062b2f0e69046810717534cb09%}





%{Phalcon_Text_3173cd284daadc9ba09e13567de9fbdf}%

%{Phalcon_Text_04dad9f6b8422ebe6190f87f1bc4c25b}%

.. code-block:: php

    <?php

    echo Phalcon\Text::endsWith("Hello", "llo"); // {%Phalcon_Text_b326b5062b2f0e69046810717534cb09%}
    echo Phalcon\Text::endsWith("Hello", "LLO"); // {%Phalcon_Text_68934a3e9455fa72420237eb05902327%}
    echo Phalcon\Text::endsWith("Hello", "LLO", false); // {%Phalcon_Text_b326b5062b2f0e69046810717534cb09%}





%{Phalcon_Text_a7293be09a05bf930dcc4f4c6734cada}%

%{Phalcon_Text_7ec4a5e649121e8309bfc0f8a2d0d4a1}%

%{Phalcon_Text_19a653f84ad04c21bbf7224181334746}%

%{Phalcon_Text_d75fcccea170b2e688688d144db8ef65}%

