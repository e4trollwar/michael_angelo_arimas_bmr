%{Phalcon_Image_Adapter_17e37c760982f87bcd590731fba42f22}%
==========================================

%{Phalcon_Image_Adapter_e72c6bad6f1ae6f44b27956f36582611|:doc:`Phalcon\\Image\\AdapterInterface <Phalcon_Image_AdapterInterface>`}%

%{Phalcon_Image_Adapter_0ea025e017ccf499d3de8f34702bb330}%

%{Phalcon_Image_Adapter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Image_Adapter_9e368b82eeb26f2e9a2a41216cf2c3f0}%

%{Phalcon_Image_Adapter_bc3f4afc28cf64bcf4a2c1533342feda}%

%{Phalcon_Image_Adapter_8c8ad42ad2d8d25af3b967fdb78aafe7}%

%{Phalcon_Image_Adapter_9e9720e38ca04be3162ee675854b12ae}%

%{Phalcon_Image_Adapter_a9a166ee63af289d81d400e30fc16cb5}%

%{Phalcon_Image_Adapter_feae7c99ef013b87b02dd6315f82b081}%

%{Phalcon_Image_Adapter_476f2497c851eae8a78f73032ad317bb}%

%{Phalcon_Image_Adapter_01e98089aa2442ad0260a82f61c9e272}%

%{Phalcon_Image_Adapter_743e66688f22bcb4345827e0b6bfec2d}%

%{Phalcon_Image_Adapter_bcea4f38bd2aa3cedc96001ece4b6084}%

%{Phalcon_Image_Adapter_fef0bfb4cbf8d1e78f5929cd03c9d9ab}%

%{Phalcon_Image_Adapter_0b8aef9d269b4e47c6bb5755aa2fdd31}%

%{Phalcon_Image_Adapter_c51e110ab832300bfb60c3b5cb803e12|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_67f6473560ed054927ccd8bd4c6022b1}%

%{Phalcon_Image_Adapter_56781a3f7da04f6f515edd90ec629309|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_d15447ed4be4349529b874f73e9bf140}%

%{Phalcon_Image_Adapter_6bdd801113f4ccf5d7bf96bdb39b56db|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_a35195cd99d7d13daf538c3f3594fd71}%

%{Phalcon_Image_Adapter_c1ef8ed6f72c73d6ee07527900da8f5c|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_e00e71ca6ee23ce529d38d2208bafdf7}%

%{Phalcon_Image_Adapter_61a1a6a192109adf38806d2d0bcc4692|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_ee6db05cd386b5aa8caa5329b5941aa4}%

%{Phalcon_Image_Adapter_04c9bdb2d9de5e6e178d210d3c9c700d|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_35d6c150350a4435617e18fb30030f96}%

%{Phalcon_Image_Adapter_11c68fbc4a1aad6c3d5f3c7329861998|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_f7fcebc47d482a284953ee72539bb098}%

%{Phalcon_Image_Adapter_a830d37f182d34fb4c1b850bf021a839|:doc:`Phalcon\\Image\\AdapterInterface <Phalcon_Image_AdapterInterface>`}%

%{Phalcon_Image_Adapter_3a2d417c9d89eade0dc6f0a9422ad499}%

%{Phalcon_Image_Adapter_db50bee34c2fa081ff939f6277cfa411|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_816bcf1eed6f3beeb043f8cd89cc03fc}%

%{Phalcon_Image_Adapter_fd71bedcb031cab779853dc72269733f|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_04227e374bc19c99406bbd50ecf0d959}%

%{Phalcon_Image_Adapter_1043f516ebb72d117b9196cff7b25c41|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_dd872c7e134e6531c1d94dfe421b9dbc}%

%{Phalcon_Image_Adapter_76724ba912511a664191b2fe25749d30|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_2110446b65e9c3e878dabc1d8df0d8b0}%

%{Phalcon_Image_Adapter_5b2a8569a21834457de5d10b1cd6fa26|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_ecc5022fba3216779e50af03d420ad64}%

%{Phalcon_Image_Adapter_3cfccb63049c2a895401f80ce4995914}%

%{Phalcon_Image_Adapter_cba770832f99922c6ccd2b64fd158d65}%

%{Phalcon_Image_Adapter_13df3a4c686d09ced20f91c813d6d7c8|:doc:`Phalcon\\Image\\Adapter <Phalcon_Image_Adapter>`}%

%{Phalcon_Image_Adapter_d82d98067ee7bc0b11560c5b87cb52ee}%

%{Phalcon_Image_Adapter_3ceb339a7a96cebb713f52522e9b735e}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_4d7af4b9ba7a7da1190197d826e24e02}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_4b6679780cf852eb7658c50777d0f7c8}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_71415a362c44e142114cd0574910d35a}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_3e6f8f30b8000ebfbce7f183fc7bf233}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_b08ec7c6828f7bad3e7ae7a375607e25}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_536dcde8e387f28878b19bded15cdd89}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_3f38dc2f223d3eebcf3438570dc2989a}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_d70411c4389e5b8cc36d8234bb3335d3}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_a93c082812cc7b79736627d91920eb4a}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_b6169c71e11d2bfe81526f544ffc0812}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_cdcdbf24cfa4033853e80ecc718f12ef}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_e0394c09b4f6fe9c6e7bd8bbabfcc84e}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_24f304fe93823a0159792a57590c4202}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Image_Adapter_9791a6b70979a90c18e57a9ca29103b1}%

%{Phalcon_Image_Adapter_68cf0a3bff1e41b907cf955f570c5249}%

