%{Phalcon_CLI_Router_1622b26fd18fecb73d13e9822ef5fd22}%
==============================

%{Phalcon_CLI_Router_0b066e4bf73264afdc3e24f71410b010|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_CLI_Router_2bd906a5889745141477be3a89ad1cbc}%

.. code-block:: php

    <?php

    $router = new Phalcon\CLI\Router();
    $router->handle(array(
    	'module' => 'main',
    	'task' => 'videos',
    	'action' => 'process'
    ));
    echo $router->getTaskName();




%{Phalcon_CLI_Router_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_CLI_Router_a5d0ee958da5dcfe392fa79afb9d9a90}%

%{Phalcon_CLI_Router_fa682e7efb4d37f2522e264abe9d26d2}%

%{Phalcon_CLI_Router_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Router_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_CLI_Router_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Router_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_CLI_Router_ded52977ec30c1f8493d0c24ed4536d2}%

%{Phalcon_CLI_Router_d6dc30b314ef7afb8dbf713d2f52ef81}%

%{Phalcon_CLI_Router_ca6820ca4c1425cbc40c00d50435beb3}%

%{Phalcon_CLI_Router_1bddb8ba3418530b9db767f0161fc12d}%

%{Phalcon_CLI_Router_f089a0a397fa78c7218a0297585d6606}%

%{Phalcon_CLI_Router_2c676539e85e0e19da7b13c5801ba8a2}%

%{Phalcon_CLI_Router_d910e1ecc381974fa4546d0ef1b07613}%

%{Phalcon_CLI_Router_1a2f2038ddc6926f751850a98d5d1ee8}%

%{Phalcon_CLI_Router_b0d3976a845e6b6da6452c56c2723b2b}%

%{Phalcon_CLI_Router_b69ab75bc996147e9caae0a893dc2a8f}%

%{Phalcon_CLI_Router_11e24802090bba8a329205391b23c0aa}%

%{Phalcon_CLI_Router_11160926ba518408f010c977dda558df}%

%{Phalcon_CLI_Router_7653c5621328184a0ef6712f5be7b5b7}%

%{Phalcon_CLI_Router_144c193d836845c38e4575cf9aa411cb}%

%{Phalcon_CLI_Router_d88c3713331c25b77ed605f801609a8c}%

%{Phalcon_CLI_Router_d3bcfee76fc561dbf63a15021834edfc}%

