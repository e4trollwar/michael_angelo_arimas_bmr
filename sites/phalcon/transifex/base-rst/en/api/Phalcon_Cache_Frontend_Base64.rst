%{Phalcon_Cache_Frontend_Base64_520e21647b1d61f8bd539d4ee984e1f7}%
==========================================

%{Phalcon_Cache_Frontend_Base64_162bdfd7b52f9a8bbf25b7cb2701bc34|:doc:`Phalcon\\Cache\\Frontend\\Data <Phalcon_Cache_Frontend_Data>`}%

%{Phalcon_Cache_Frontend_Base64_d4caea1dc5a1b9f969431a18993f6133|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Frontend_Base64_b25c433955db90887eb146314de2df6c}%

.. code-block:: php

    <?php

     // {%Phalcon_Cache_Frontend_Base64_a462fb7f80d0156934740e45c5e97f22%}
     $frontCache = new Phalcon\Cache\Frontend\Base64(array(
        "lifetime" => 172800
     ));
    
     //{%Phalcon_Cache_Frontend_Base64_eb7a3fc32d22e8cc1c9ce92e660a2507%}
     $cache = new Phalcon\Cache\Backend\Mongo($frontCache, array(
    	'server' => "mongodb://{%Phalcon_Cache_Frontend_Base64_22ac3e0903d9afe095530ddf5ccb752f%}
          'db' => 'caches',
    	'collection' => 'images'
     ));
    
     // {%Phalcon_Cache_Frontend_Base64_9169ae03487f708bb58ee5f2e8437e3c%}
     $cacheKey = 'some-image.jpg.cache';
     $image    = $cache->get($cacheKey);
     if ($image === null) {
    
         // {%Phalcon_Cache_Frontend_Base64_bf5a7c91b76e645f02777b9af4ca3e68%}
         $cache->save($cacheKey, file_get_contents('tmp-dir/some-image.jpg'));
     }
    
     header('Content-Type: image/jpeg');
     echo $image;




%{Phalcon_Cache_Frontend_Base64_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Frontend_Base64_346147a9407546082e932b7555d6ae42}%

%{Phalcon_Cache_Frontend_Base64_6a72b5df70e820ebbbb658e307cfdd82}%

%{Phalcon_Cache_Frontend_Base64_bf8d76d841c9e0ff5bc2145390806e3e}%

%{Phalcon_Cache_Frontend_Base64_a3a637a6fb529c4b3e05a71568b34d5b}%

%{Phalcon_Cache_Frontend_Base64_08c177613301040bd72546ff68086470}%

%{Phalcon_Cache_Frontend_Base64_1ae81dbbfeea219e1d883f5ae0052e6a}%

%{Phalcon_Cache_Frontend_Base64_cb420b927c72015a1baed352fde19602}%

%{Phalcon_Cache_Frontend_Base64_790ba726a84e8cc7d11c01ae6aeda21b}%

%{Phalcon_Cache_Frontend_Base64_fb4ea0b507a29eca01a0f21e824f87cb}%

%{Phalcon_Cache_Frontend_Base64_1df518f9eabe9f817151b63df9f51b70}%

%{Phalcon_Cache_Frontend_Base64_722c2e1e16bfe6f0d096aae0c9166480}%

%{Phalcon_Cache_Frontend_Base64_1ff5bfe3b4c822dd286e8ce81aac1b8b}%

%{Phalcon_Cache_Frontend_Base64_b145d17dcbbadf9356b39116f7244b29}%

%{Phalcon_Cache_Frontend_Base64_e3e99c0a1d8cbb1a36ae911dcb475f20}%

%{Phalcon_Cache_Frontend_Base64_d9632a3c1f7822b67ab24ed1bf397249}%

%{Phalcon_Cache_Frontend_Base64_3ff217b55d51515949922d3d8f3004eb}%

