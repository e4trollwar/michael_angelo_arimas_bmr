%{Phalcon_Mvc_View_Simple_52f78149b3a969ac1c75c4e469afc5e5}%
====================================

%{Phalcon_Mvc_View_Simple_32f67d09941ad6b6963c440e030d031e|:doc:`Phalcon\\DI\\Injectable <Phalcon_DI_Injectable>`}%

%{Phalcon_Mvc_View_Simple_cb143a03062fad8680104c800ead4e79|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_View_Simple_937a4d23766be50c08958af820f31124}%

.. code-block:: php

    <?php

     $view = new Phalcon\Mvc\View\Simple();
     echo $view->render('templates/my-view', array('content' => $html));




%{Phalcon_Mvc_View_Simple_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_View_Simple_38d168e0be68a8fb5da8d35b168503c0}%

%{Phalcon_Mvc_View_Simple_43b61bfe2f34172cb8e4661eab2f2380}%

%{Phalcon_Mvc_View_Simple_d257c1b90d471621301f4774cbd86581}%

%{Phalcon_Mvc_View_Simple_f3bd3758c127a36f8c3c5f37fd1362e1}%

%{Phalcon_Mvc_View_Simple_a9db8035960f804122fab3edcdc26906}%

%{Phalcon_Mvc_View_Simple_5cb8356ba2a4cb3c42531d901b21cc6c}%

%{Phalcon_Mvc_View_Simple_67964bb8292162fdea857970c7849026}%

%{Phalcon_Mvc_View_Simple_dc053761cc0f4d07a7c906896ef9ab67}%

.. code-block:: php

    <?php

    $this->view->registerEngines(array(
      ".phtml" => "Phalcon\Mvc\View\Engine\Php",
      ".volt" => "Phalcon\Mvc\View\Engine\Volt",
      ".mhtml" => "MyCustomEngine"
    ));





%{Phalcon_Mvc_View_Simple_46261288d993a8902798a46d632bc97a}%

%{Phalcon_Mvc_View_Simple_40e808e88fcdca48db5c8457d17d5d67}%

%{Phalcon_Mvc_View_Simple_849ea7f36c775b798e83e079025de0f1}%

%{Phalcon_Mvc_View_Simple_c5fad474a79599299afb29ee02f12e73}%

%{Phalcon_Mvc_View_Simple_0ac1a7c1f94f525aaabcfc29b2a33490}%

%{Phalcon_Mvc_View_Simple_cd24f4d30d66f06738e2dffec0d2bdb0}%

%{Phalcon_Mvc_View_Simple_337867abf784792a89629a3b443671d0}%

%{Phalcon_Mvc_View_Simple_c144e59f94abc51f2914c8eeb2bc7da6}%

%{Phalcon_Mvc_View_Simple_0ac85fcc64878d3a7c767108032782c4}%

%{Phalcon_Mvc_View_Simple_5fb87964fde637a4d5a9021340c846f3}%

.. code-block:: php

    <?php

     	//{%Phalcon_Mvc_View_Simple_03ae2ce0d1403555685f00c92ada0a28%}
     	$this->partial('shared/footer');

.. code-block:: php

    <?php

     	//{%Phalcon_Mvc_View_Simple_525a51cf377d9a86611411ec4f7b0093%}
     	$this->partial('shared/footer', array('content' => $html));





%{Phalcon_Mvc_View_Simple_86d98dd7f89026d9b666d69f3dff0ae0|:doc:`Phalcon\\Mvc\\View\\Simple <Phalcon_Mvc_View_Simple>`}%

%{Phalcon_Mvc_View_Simple_3dc55fb457a8235466028a1638e643fe}%

%{Phalcon_Mvc_View_Simple_24c8666ac57585443dcb4a4660f27fb6}%

%{Phalcon_Mvc_View_Simple_a259a9338b0928ec483d1596bdcd3966}%

%{Phalcon_Mvc_View_Simple_90acf57c85190fe4d253a140e1dcca8b|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_View_Simple_ef7d466da826b95a531646e34c64ca81}%

%{Phalcon_Mvc_View_Simple_09b5c9851ac944b212045804108afe45|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_View_Simple_bd3e82819d1fdd7d992782a4603bd0e9}%

%{Phalcon_Mvc_View_Simple_d0db2a65c2f0c8f2b38f02843dc9af96|:doc:`Phalcon\\Mvc\\View\\Simple <Phalcon_Mvc_View_Simple>`}%

%{Phalcon_Mvc_View_Simple_d4a1fab2d9e9cf2fb183dd5a1da9d239}%

.. code-block:: php

    <?php

      $this->view->cache(array('key' => 'my-key', 'lifetime' => 86400));





%{Phalcon_Mvc_View_Simple_d9c8c9b23457a39dc75c5c3573b0b23e|:doc:`Phalcon\\Mvc\\View\\Simple <Phalcon_Mvc_View_Simple>`}%

%{Phalcon_Mvc_View_Simple_5666eb7f8db16aa0f9cb46e2cd45b1f5}%

.. code-block:: php

    <?php

    $this->view->setParamToView('products', $products);





%{Phalcon_Mvc_View_Simple_3fd5507a83c8c35148cc3221bbad0c79|:doc:`Phalcon\\Mvc\\View\\Simple <Phalcon_Mvc_View_Simple>`}%

%{Phalcon_Mvc_View_Simple_ef5badfdf820c54672609c5ce31d40d3}%

.. code-block:: php

    <?php

    $this->view->setVars(array('products' => $products));





%{Phalcon_Mvc_View_Simple_28063c324c4dd895a1fd459e1e3b3300|:doc:`Phalcon\\Mvc\\View\\Simple <Phalcon_Mvc_View_Simple>`}%

%{Phalcon_Mvc_View_Simple_a7c2d95e5c08800de66c815eb24b1bf7}%

.. code-block:: php

    <?php

    $this->view->setVar('products', $products);





%{Phalcon_Mvc_View_Simple_f280bc12a8b7dd226062887c75b3356c}%

%{Phalcon_Mvc_View_Simple_556f0eb6745826c2bec05d03f3cd6b8f}%

%{Phalcon_Mvc_View_Simple_0ef6adc104f7ff5d55626c20e3d39118}%

%{Phalcon_Mvc_View_Simple_75aff7a4f047fd34c7075c73592d353d}%

%{Phalcon_Mvc_View_Simple_5fb222febcdeed0480f2df614a5700ab|:doc:`Phalcon\\Mvc\\View\\Simple <Phalcon_Mvc_View_Simple>`}%

%{Phalcon_Mvc_View_Simple_e96a422159b67ba5238340257fee8bfb}%

.. code-block:: php

    <?php

    $this->view->setContent("<h1>hello</h1>");





%{Phalcon_Mvc_View_Simple_098b283de06b3d606bc941e79200d4a9}%

%{Phalcon_Mvc_View_Simple_6d1ab638ad0ffabae6d5473241d3e2d5}%

%{Phalcon_Mvc_View_Simple_c4504fe9fbe9dfb93bbd3cbf4ee751e0}%

%{Phalcon_Mvc_View_Simple_38b425be8f2830ec0b4d97a4c7878999}%

%{Phalcon_Mvc_View_Simple_4235429658e99d715448989955206c44}%

%{Phalcon_Mvc_View_Simple_cc332ac586888c98722310f153c980e7}%

.. code-block:: php

    <?php

    $this->view->products = $products;





%{Phalcon_Mvc_View_Simple_19cd8e6bc02bf443c177fa7a07f3c7c4}%

%{Phalcon_Mvc_View_Simple_0f4889ec0422f0973699057ce29d773e}%

.. code-block:: php

    <?php

    echo $this->view->products;





%{Phalcon_Mvc_View_Simple_d98a2143a4e9b08520a7df0009a78266|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_View_Simple_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_View_Simple_3e7e4ddacc724c78120150572bdab704|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_View_Simple_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_View_Simple_5e6f785f2df62ae1a2a672663fb6b2e4|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_View_Simple_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Mvc_View_Simple_8d859c2f0efce8383f08ae517e34a4af|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_View_Simple_fddf8905978e673c97a301a70bb79d53}%

