<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class OthersController extends Controller
{
    private function report($html){
        try
        {   
        // create an API client instance
            $client = new Pdfcrowd("kyben", "02eb43390601d8128d15156ecd13ef41");

        // convert a web page and store the generated PDF into a $pdf variable
            $pdf = $client->convertHtml($html);
        // set HTTP response headers
            header("Content-Type: application/pdf");
            header("Cache-Control: max-age=0");
            header("Accept-Ranges: none");
            header("Content-Disposition: attachment; filename=\"Daily_Report.pdf\"");

        // send the generated PDF 
            echo $pdf;
        }
        catch(PdfcrowdException $why)
        {
            echo "Pdfcrowd Error: " . $why;
        }
    }


    public function trucksearchAction()
    {
        $query= $this->request->getPost('query');

        $trucks  = Trucks::find("plateno = '" . $query . "'");

        $data=count($trucks);

        if(count($trucks)==0){
            $data=array(
                "truck_height"=>"",
                "truck_width"=>"",
                "truck_length"=>"",
                );
        }else{

            $data=array(
                "truck_height"=>$trucks[0]->heigth,
                "truck_width"=>$trucks[0]->width,
                "truck_length"=>$trucks[0]->length,
                "company"=>$trucks[0]->company,
                );

        }
        echo json_encode($data);
    }

    public function sandpriceAction()
    {  
        $compid= $this->request->getPost('compid');
        $sandid= $this->request->getPost('id');

        $setprice= Setprice::find(array("company='".$compid."' and  sand='".$sandid."'"));

        if(count($setprice)!=0){
            $data[]=array(
                "price"=>$setprice[0]->setprice,
                );

        }else{
            $sand  = Sand::find("id = '" . $sandid . "'");
            $data[]=array(
                "price"=>$sand[0]->price,
                );
        }
        


       // $data=$sandid;
        echo json_encode($data);
    }

    public function dailyrptAction($from,$to)
    {  
       $tr=''; $count=1;
        // The data set to paginate

       if(!isset($from) && !isset($to)){
             $query  = $this->modelsManager->createQuery("SELECT * FROM orders ");
       }else{
             $query  = $this->modelsManager->createQuery("SELECT * FROM orders WHERE orders.date >= '$from' and orders.date <= '$to'");
       }

       
        $orderdate   = $query->execute();
            // echo count($orderdate);
    foreach ( $orderdate as $daily) {
        if ($count % 2 == 0) {
            $color="#eee";
        }else{
            $color="#fff";
        }
        $count++;

        $tr.="<tr style=\" font:.7em sans-serif; background:".$color."\">
        <td style=\"padding:10px;\">".$daily->plateno."</td>
        <td style=\"padding:10px;\">".$daily->company."</td>
        <td style=\"padding:10px;\">".$daily->sand."</td>
        <td style=\"padding:10px;\">".$daily->cumtr."</td>
        <td style=\"padding:10px;\">".$daily->pricecumtr."</td>
        <td style=\"padding:10px;\">".$daily->cash."</td>
        <td style=\"padding:10px;\">".$daily->poamount."</td>
        <td style=\"padding:10px;\">".$daily->drno."</td>
        <td style=\"padding:10px;\">".$daily->date."</td>

        <tr>";

        }
        echo $html="
        <body style=\" width:900px; margin:auto;\">

           <h2 style=\" font:1.2em sans-serif; \">
               BMR AGGREGATES
           </h2>
           <div style=\" text-align:center;font:1em sans-serif;\">
            SALES  REPORT<br><br>
            FROM: ".$from." - T0: ".$to."
        </div>
        <div >
            <table style=\" width:100%\" >
                <thead style=\" font:.7em sans-serif ;background:#ddd;color:#333;\">

                    <tr style=\"text-align:center;\">
                        <th rowspan=\"2\">PLATE #</th>
                        <th rowspan=\"2\">COMPANY</th>
                        <th rowspan=\"2\">CLASS</th>
                        <th rowspan=\"2\">Cu-Mtr</th>
                        <th rowspan=\"2\">Price-Cu-Mtr</th>
                        <th colspan=\"2\">AMOUNT</th>
                     
                        <th rowspan=\"2\">DR</th>
                        <th rowspan=\"2\">DATE</th>
                    </tr>
                    <tr>
                        <th>CASH</th>
                        <th>PO</th>
                    </tr>
                </thead>
                <tbody>
                    ". $tr ."
                </tbody>
            </table>

        </div>
    </body>";
     $this->report($html);


}
public function trucklistAction()
{  
    require 'pdfcrowd.php';
    $tr='';
    $count=1;
        // The data set to paginate
    $data = array();

    $truck      = Trucks::find();
    foreach ($truck as $dbtruck) {
        ///////////////////////////////////////
        if ($count % 2 == 0) {
            $color="#eee";
        }else{
            $color="#fff";
        }
        $count++;
        //////////////////////////////////////
        foreach (Company::find() as $comp) {
         if($comp->id==$dbtruck->company){
            $compname=$comp->campname;
        }
    }
    $tr.="<tr style=\" font:.7em sans-serif; background:".$color."\">
    <td style=\"padding:10px;\">".$dbtruck->plateno."</td>
    <td style=\"padding:10px;\">".$dbtruck->length."</td>
    <td style=\"padding:10px;\">".$dbtruck->width."</td>
    <td style=\"padding:10px;\">".$dbtruck->heigth."</td>
    <td style=\"padding:10px;\">".$compname."</td>
    <tr style=\"padding:10px;\">";
    }

    echo $html="

    <body style=\" width:900px; margin:auto;\">

       <h2 style=\" font:1.2em sans-serif; \">
           BMR AGGREGATES
       </h2>
       <div style=\" text-align:center;font:1em sans-serif;\">
        TRUCK LIST REPORT
    </div>
    <div >
        <table style=\" width:100%\" >
            <thead style=\" font:.7em sans-serif ;background:#ddd;color:#333;\">
                <tr style=\"\">
                    <th style=\"padding:10px;\">Plate #</th>
                    <th style=\"padding:10px;\">Truck Length</th>
                    <th style=\"padding:10px;\">Truck Width</th>
                    <th style=\"padding:10px;\">Truck Height</th>
                    <th style=\"padding:10px;\">Company Owner</th>
                </tr>
            </thead>
            <tbody>
                ". $tr ."
            </tbody>
        </table>

    </div>


</body>";
$this->report($html);

}
}
