%{Phalcon_Loader_d62ddddc3ba02d61e2d4a38f3eee8164}%
=========================

%{Phalcon_Loader_29096607c19ecbe07a3a89dd7b99ff51|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`}%

%{Phalcon_Loader_bbbc844c4834f74b810b05bbbfa21333}%

.. code-block:: php

    <?php

     //{%Phalcon_Loader_4b4ba81e5d13f4105da1c536c17e790e%}
     $loader = new Phalcon\Loader();
    
     //{%Phalcon_Loader_b365b3098f8bada9ada29235f6405308%}
     $loader->registerNamespaces(array(
       'Example\Base' => 'vendor/example/base/',
       'Example\Adapter' => 'vendor/example/adapter/',
       'Example' => 'vendor/example/'
     ));
    
     //{%Phalcon_Loader_974a6d2fc8d2eda86afedcbe99ba9670%}
     $loader->register();
    
     //{%Phalcon_Loader_929a3da84877f143acaf0425a9bbd9a2%}
     $adapter = Example\Adapter\Some();




%{Phalcon_Loader_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Loader_a5d0ee958da5dcfe392fa79afb9d9a90}%

%{Phalcon_Loader_aa7a126741461b6425ce409146604a7f}%

%{Phalcon_Loader_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Loader_8d848637cfda8b4cc05eedcec9549316}%

%{Phalcon_Loader_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Loader_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Loader_f0d31004f3357e4944814e5958a4ab8c|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_612dad70a806f2096ace3b6740f19e0d}%

%{Phalcon_Loader_ef86635b733571c5795741ca674218f8}%

%{Phalcon_Loader_ccb2159a336a29d6ef7b8b78f4b09344}%

%{Phalcon_Loader_bd8dc900d3cc7121516f7309032396c4|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_6a1589821f7a0722037a177cc64f35a4}%

%{Phalcon_Loader_2c04688f6bcc676c32dbb6a61453a328}%

%{Phalcon_Loader_9f03b26eeaba582665c954d3f006d376}%

%{Phalcon_Loader_f8496591ffcdfb2b82a409984854dff0|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_fb077dd627df81790ab265421adcd374}%

%{Phalcon_Loader_95821303cd9289b64d44a03f8775470c}%

%{Phalcon_Loader_71d75304c158c11a3650ab726a714c69}%

%{Phalcon_Loader_aed208fa0c83409206c77780ef7d06e9|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_fb077dd627df81790ab265421adcd374}%

%{Phalcon_Loader_7a63ebeb99d1329343a5e64205dbcdb8}%

%{Phalcon_Loader_7233b0c5b921553ff7968024c88e94c8}%

%{Phalcon_Loader_b426e552d7d1147a85f9f84449f0c856|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_d5395507c833c16b5ce2134f20ba63dd}%

%{Phalcon_Loader_65536778ce824be5cb1247b688a724e3}%

%{Phalcon_Loader_3ba1fc17cdccf443ee95a6879313ce7e}%

%{Phalcon_Loader_9cec5c79c44600c0010fb50cdbce1192|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_6ef4ce0908aebfd5471cc871bf492a2b}%

%{Phalcon_Loader_8023c4b79c9a0c8fd7f9a900793dafcf|:doc:`Phalcon\\Loader <Phalcon_Loader>`}%

%{Phalcon_Loader_bc84d56ecd7e2332c703fa857dfb885b}%

%{Phalcon_Loader_c6a6caa1a54dc2a2960f0f5bf2374b8e}%

%{Phalcon_Loader_f04be7a14f2a614e19a679676b1b9f82}%

%{Phalcon_Loader_507939433db85716e026eea2f6c22224}%

%{Phalcon_Loader_10f5e8493d85a27f5239399a5305c5cf}%

%{Phalcon_Loader_5b36472ff29709a430c72e4df19e0f4c}%

%{Phalcon_Loader_0ae5396b46e8cc503d839274ce57da25}%

