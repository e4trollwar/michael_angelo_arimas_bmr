%{Phalcon_Mvc_Controller_1634932688568059e834d6ff4467501a}%
===========================================

%{Phalcon_Mvc_Controller_32f67d09941ad6b6963c440e030d031e|:doc:`Phalcon\\DI\\Injectable <Phalcon_DI_Injectable>`}%

%{Phalcon_Mvc_Controller_e5ca2c84097f9631fbe961046f79c42f|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Mvc\\ControllerInterface <Phalcon_Mvc_ControllerInterface>`}%

%{Phalcon_Mvc_Controller_c0b7e520f5be10c6fe4248593f4b10a5}%

.. code-block:: php

    <?php

    class PeopleController extends \Phalcon\Mvc\Controller
    {
    
      //{%Phalcon_Mvc_Controller_83c53c0baa74bee3cacc39d1eccca0d3%}
      public function indexAction()
      {
    
      }
    
      public function findAction()
      {
    
      }
    
      public function saveAction()
      {
       //{%Phalcon_Mvc_Controller_b8ed348177991be84886aca190e5cea3%}
       return $this->dispatcher->forward(array('controller' => 'people', 'action' => 'index'));
      }
    
    }




%{Phalcon_Mvc_Controller_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Controller_c64ee8eb8113969d2574e6ce00e1caec}%

%{Phalcon_Mvc_Controller_98e470c2d493324884f59eedd4e55d52}%

%{Phalcon_Mvc_Controller_d98a2143a4e9b08520a7df0009a78266|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Controller_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_Controller_3e7e4ddacc724c78120150572bdab704|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Controller_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_Controller_5e6f785f2df62ae1a2a672663fb6b2e4|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Controller_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Mvc_Controller_8d859c2f0efce8383f08ae517e34a4af|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Controller_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Mvc_Controller_aa385da180999c3488ab22e043b6eb9d}%

%{Phalcon_Mvc_Controller_a8ec8322461cb1dce1953c9378484594}%

