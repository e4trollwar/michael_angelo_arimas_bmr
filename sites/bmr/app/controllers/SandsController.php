<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class SandsController extends Controller
{

	public function initialize(){
		$this->view->setTemplateAfter('template');
	}
	
	public function addsandAction()
	{	
		//VARIABLES
		$this->view->setVar('header_title', "Add New Sand");
		$this->view->setVar('notification', "");
		//ADD SAND TYPE AND CATEGORY FUNCTION 
		function add($model,$column,$data){
			$add = new $model();
		 	$add->$column 	= $data;

		 	if ($add->save() == false) { 
		 		return "Umh, We can store data: ";
		 	} else {
		 		return "Great, a new data was saved successfully!";
		 	}
		}
		//END ADD SAND TYPE AND CATEGORY FUNCTION 

		//DISPLAY FUNCTION
		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
				);
			}
			return json_decode(json_encode($data));
		}
		//END DISPLAY FUNCTION
		if ($this->request->isPost('savetype') == true){  add("Sandtype","type",$this->request->getPost('sandtype'));}; //ADD SAND TYPE
		if ($this->request->isPost('savecateg') == true){ add("Sandcateg","category",$this->request->getPost('sandcateg'));}; //ADD SAND CATEGORY
        $this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY

		///ADD NEW SAND
		if ($this->request->isPost('savesand') == true){
			$type= $this->request->getPost('type');
		 	$categ= $this->request->getPost('categ') ;
		 	$price= $this->request->getPost('price') ;


		 	$add = new Sand();
		 	$add->sandtype 	= $type;
		 	$add->sandcateg = $categ;
		 	$add->price 	= $price;

		 	if ($add->save() == false) { 
		 		echo "Umh, We can store data: ";
		 	} else {
		 		echo "Great, a new data was saved successfully!";
		 	}


		}; //ADD SAND TYPE
	}
	public function sandListAction()
	{
		$this->view->setVar('header_title', "Product Sand List");


		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
				);
			}
			return json_decode(json_encode($data));
		}
		$this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY



		// Current page to show
		// In a controller this can be:
		// $this->request->getQuery('page', 'int'); // GET
		// $this->request->getPost('page', 'int'); // POST
		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
		

		// The data set to paginate
		$robots      = Sand::find();

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator   = new PaginatorModel(
			array(
				"data"  => $robots,
				"limit" => 10,
				"page"  => $currentPage
				)
			);

		// Get the paginated results
		$this->view->page= $paginator->getPaginate();;
	}

}
