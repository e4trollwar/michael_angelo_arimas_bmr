%{Phalcon_Tag_fdce4e1269ef2ca636f1f9641f8f57e2}%
======================

%{Phalcon_Tag_ee9efc3c55e33df71a33d82ba293d1ab}%

%{Phalcon_Tag_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Tag_90b91fc8115028727bf634de06f168ba}%

%{Phalcon_Tag_10837c5f04896b2ca5cee5ed9eb87924}%

%{Phalcon_Tag_cd6a18a02f0951e88043588bdfff9910}%

%{Phalcon_Tag_5aa1e0bef39026521df9f12ffbd15daf}%

%{Phalcon_Tag_6f3f34b7ff2208cf6f1425ccec98e268}%

%{Phalcon_Tag_3265f472b15aa309950c2fb993a1ddb3}%

%{Phalcon_Tag_32e6a71c66490ca09010f28e50d866b1}%

%{Phalcon_Tag_5be07d9ac6c78411cf0b6bbbd493f065}%

%{Phalcon_Tag_6db85d70b85c0d7fc11b5b6c373cc0b0}%

%{Phalcon_Tag_677df30ac7fcce28d18b75e8aad736d5}%

%{Phalcon_Tag_59adb1842c83e897f8de163ed3e0d64f}%

%{Phalcon_Tag_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Tag_7824e04a50c893ac3658d28bcd792a77|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Tag_ef2f43efaf7664b8ee450061731ddc5d}%

%{Phalcon_Tag_9ee473ec87c85918af1bdffe238b54d7|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Tag_6593aa2712f21de12a368dd130b70231}%

%{Phalcon_Tag_20383c733461d0a3b9ad3f629f05aa6d|:doc:`Phalcon\\Mvc\\UrlInterface <Phalcon_Mvc_UrlInterface>`}%

%{Phalcon_Tag_eebb258b9d40dd8274b2ee7ad2cc086d}%

%{Phalcon_Tag_8f583bd5cc7dd5f229322629cd122790|:doc:`Phalcon\\EscaperInterface <Phalcon_EscaperInterface>`}%

%{Phalcon_Tag_50316f6b695899e946f0c0de42d17ab9}%

%{Phalcon_Tag_7d6ed457537aa6fa631dac5062808b9b}%

%{Phalcon_Tag_558a29d6bfc717e2cbc3774a00bf23c4}%

%{Phalcon_Tag_c8e69c6d37c480426f994e9e599d1351}%

%{Phalcon_Tag_18314b3b6f2b5e309cdc55ba5d16d2f6}%

%{Phalcon_Tag_95e973ea2562db129168617db6e7445d}%

%{Phalcon_Tag_364a86ebc73372368441bb963f9c8a35}%

.. code-block:: php

    <?php

     //{%Phalcon_Tag_b29d286343fcbca9fe5d01f8b8567f89%}
     Phalcon\Tag::setDefault("name", "peter");
    
     //{%Phalcon_Tag_501e60e97f66107188b27dd9f43a10e9%}
     echo Phalcon\Tag::textField("name"); //{%Phalcon_Tag_2e4b238b9bdf6b24a42bb6cb37e5f4c4%}





%{Phalcon_Tag_d38f48893964c163dd456984fd4225a5}%

%{Phalcon_Tag_364a86ebc73372368441bb963f9c8a35}%

.. code-block:: php

    <?php

     //{%Phalcon_Tag_b29d286343fcbca9fe5d01f8b8567f89%}
     Phalcon\Tag::setDefaults(array("name" => "peter"));
    
     //{%Phalcon_Tag_501e60e97f66107188b27dd9f43a10e9%}
     echo Phalcon\Tag::textField("name"); //{%Phalcon_Tag_2e4b238b9bdf6b24a42bb6cb37e5f4c4%}





%{Phalcon_Tag_20b38b7810816ded29d716c5495edf60}%

%{Phalcon_Tag_9c71751bbfabb86f3e1c85e715dbfd57}%

%{Phalcon_Tag_0a6bcd49cb62c384a2ac8b6eec5607e6}%

%{Phalcon_Tag_21984a11c2e335191de14c3d85e972d7}%

%{Phalcon_Tag_8b3d73a9771d216601eda903ca9d8d58}%

%{Phalcon_Tag_14e7053b99122a116f5ee92f78afe29d}%

%{Phalcon_Tag_83899792ad0db0020212f6fdb7955bfb}%

%{Phalcon_Tag_81aeb10947de4033d476acf5f3ded07f}%

%{Phalcon_Tag_161f2dce5d13d3dfa666334385117e2b}%

%{Phalcon_Tag_fc2ab88a3d7e3cf5d7b473b4b576ee25}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::linkTo('signup/register', 'Register Here!');
    echo Phalcon\Tag::linkTo(array('signup/register', 'Register Here!'));
    echo Phalcon\Tag::linkTo(array('signup/register', 'Register Here!', 'class' => 'btn-primary'));
    echo Phalcon\Tag::linkTo('http://phalconphp.com/', 'Google', FALSE);
    echo Phalcon\Tag::linkTo(array('http://phalconphp.com/', 'Phalcon Home', FALSE));
    echo Phalcon\Tag::linkTo(array('http://phalconphp.com/', 'Phalcon Home', 'local' =>FALSE));





%{Phalcon_Tag_6afae59fdf803bac1d7e0b438e96091a}%

%{Phalcon_Tag_077b7d040d6d45dc9e9100fc68e293f2}%

%{Phalcon_Tag_b613c87e991947020d64de2bab9ffeae}%

%{Phalcon_Tag_b5d20426b7e4228eb31d3d42b1fad868}%

%{Phalcon_Tag_d5e3b850f8a41505104d0d113be7ad82}%

%{Phalcon_Tag_b1bb96ddf617aafd3ad29a66a72caf2c}%

%{Phalcon_Tag_02f035ae9f042dc23d2e2e5f2548558c}%

%{Phalcon_Tag_2ec9347dd56abaa4b930417d78d47628}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::textField(array("name", "size" => 30));





%{Phalcon_Tag_41bbae9bf6924a4e8afdbaaf42b6d233}%

%{Phalcon_Tag_56903277910b465ea97f07073c166e65}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::numericField(array("price", "min" => "1", "max" => "5"));





%{Phalcon_Tag_922c4051bdf8be716b87dcc0c216b569}%

%{Phalcon_Tag_7eecbdb4e9d2f2c7aac70b78b4c7ecc2}%

%{Phalcon_Tag_50ca0606b45c4bb79b38beadaf6c43ec}%

%{Phalcon_Tag_708f14e0ce47f29dc9921097b3170af3}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::emailField("email");





%{Phalcon_Tag_7690dfa444c8c30806d132cd45fce682}%

%{Phalcon_Tag_e0c7daa70213364651366a128c13c5e0}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::dateField(array("born", "value" => "14-12-1980"))





%{Phalcon_Tag_7407ba4f653c952a7ca7fe68da0eee77}%

%{Phalcon_Tag_a20bbae678823618056bba77843b8275}%

%{Phalcon_Tag_9ebb5f034a3484f2f8fe13ecd13a50ae}%

%{Phalcon_Tag_a857f5b3c2e56c32acf48510d9b11746}%

%{Phalcon_Tag_344e74fb7719b41d95b35770963498f9}%

%{Phalcon_Tag_73a26a98be401c380386d6dc6941f5ce}%

%{Phalcon_Tag_6f3e4dcba73e77cb8ec247be8cda79ab}%

%{Phalcon_Tag_ac0c99c440d8a8fdacea775990152250}%

%{Phalcon_Tag_c70f4639aaea35ac7473cb8eb3a58d26}%

%{Phalcon_Tag_b89b7f407b98e691ab394bb2a384e0e6}%

%{Phalcon_Tag_fa37bea95b1c0e379e55573859c6624d}%

%{Phalcon_Tag_536ec385ef8248344459f7fc91926ee8}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::passwordField(array("name", "size" => 30));





%{Phalcon_Tag_c905b4ade70ebe69546ef218b682461d}%

%{Phalcon_Tag_be16c9b81f83217bafa280b60b597312}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::hiddenField(array("name", "value" => "mike"));





%{Phalcon_Tag_2863faa2f56a1be358290bd290f2a539}%

%{Phalcon_Tag_2da86897132b72b38e4202881f100b01}%

%{Phalcon_Tag_502c820c0514e8b7aff9fd9944efa31b}%

%{Phalcon_Tag_627cd0feb5c1303dbec1a97a543f5669}%

%{Phalcon_Tag_a1f39695eb5cda4b148e6998709f4667}%

%{Phalcon_Tag_586b47de16b77a69d972cefb6d476e1c}%

%{Phalcon_Tag_cbc572fde8cddd73f44a4a1fd129e4ef}%

%{Phalcon_Tag_68ea66b05b27d24916b662364e308bf2}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::fileField("file");





%{Phalcon_Tag_1372f15072fb6ec7344f297c56f57c78}%

%{Phalcon_Tag_4dea38ccaf2badc75bfc3865cd50f980}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::checkField(array("terms", "value" => "Y"));





%{Phalcon_Tag_f294b32037335de13878ba569e2f1178}%

%{Phalcon_Tag_d5faad99056a162833aa23b79b97241c}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::radioField(array("wheather", "value" => "hot"))


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ radio_field('Save') }}





%{Phalcon_Tag_1d3868a53f58d019f3c2924881751ddd}%

%{Phalcon_Tag_025a7b8f19c3252ef453faedb3e6fa10}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::imageInput(array("src" => "/img/button.png"));


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ image_input('src': '/img/button.png') }}





%{Phalcon_Tag_0f8dd084f9b2c831da30fc0a08ead7ab}%

%{Phalcon_Tag_23e8b750aa5e272f288c23f0903ed203}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::submitButton("Save")


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ submit_button('Save') }}





%{Phalcon_Tag_34ba2a7239af8dc4c1ae27841f13829b}%

%{Phalcon_Tag_b25eda8e5b704dfbbd0e9daa0b9d52e1}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::selectStatic("status", array("A" => "Active", "I" => "Inactive"))





%{Phalcon_Tag_7b4d7f11c30effc07a14d0873035cc0c}%

%{Phalcon_Tag_8170091f7b39fa14375c07656df9b500}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::select(array(
    	"robotId",
    	Robots::find("type = 'mechanical'"),
    	"using" => array("id", "name")
     	));


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ select("robotId", robots, "using": ["id", "name"]) }}





%{Phalcon_Tag_701d806cc7b22a8103118f2b8ab00ce5}%

%{Phalcon_Tag_a19c4cfe32c39c218d71fd396275107e}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::textArea(array("comments", "cols" => 10, "rows" => 4))


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ text_area("comments", "cols": 10, "rows": 4) }}





%{Phalcon_Tag_aca0e89d879fa1325991d951a08f5433}%

%{Phalcon_Tag_60dfef47850ec522aa88b38c505be2ed}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::form("posts/save");
     echo Phalcon\Tag::form(array("posts/save", "method" => "post"));


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ form("posts/save") }}
     {{ form("posts/save", "method": "post") }}





%{Phalcon_Tag_620b3d214f541802120d84f14dac9634}%

%{Phalcon_Tag_37e4771fc45b39797038de78f711dfb8}%

%{Phalcon_Tag_22f0850ea474d658637ea5db31aecfb9}%

%{Phalcon_Tag_34ac429157cdadcf0cf0b1ee21c760ba}%

.. code-block:: php

    <?php

     Phalcon\Tag::setTitle('Welcome to my Page');





%{Phalcon_Tag_2f732e936586e56cd7ace4e1124e460a}%

%{Phalcon_Tag_60508dab55ea4bbe2c951bee64f0e3a6}%

.. code-block:: php

    <?php

     Phalcon\Tag::setTitleSeparator('-');





%{Phalcon_Tag_4e59d7da773851546fe76d0d959a6e51}%

%{Phalcon_Tag_3a0fb96f3935b5024dbde4b11b94bb87}%

%{Phalcon_Tag_bc44b3bca8c6590630c78147f7dafaff}%

%{Phalcon_Tag_6f2b5663b93157d290f81b708e716e02}%

%{Phalcon_Tag_9e81fd6dc27c32cd5ae21c0d0c9a14e0}%

%{Phalcon_Tag_253fe74521e2ca7ada50182c00c8c279}%

.. code-block:: php

    <?php

     	echo Phalcon\Tag::getTitle();

.. code-block:: php

    <?php

     	{{ get_title() }}





%{Phalcon_Tag_8566655e489ba32631768b478b27021f}%

%{Phalcon_Tag_9a467a2d5be61a023d7d112236179ab4}%

.. code-block:: php

    <?php

     	echo Phalcon\Tag::getTitleSeparator();

.. code-block:: php

    <?php

     	{{ get_title_separator() }}





%{Phalcon_Tag_322b64ad1929e3f1c81af436b38006c0}%

%{Phalcon_Tag_22a81d0d8ff293e86473af39a1370fc5}%

.. code-block:: php

    <?php

     	echo Phalcon\Tag::stylesheetLink("http://fonts.googleapis.com/css?family=Rosario", false);
     	echo Phalcon\Tag::stylesheetLink("css/style.css");


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     	{{ stylesheet_link("http://{%Phalcon_Tag_cf68bb2dedfa37e577f0f9f69b7b3b4d%}
     	{{ stylesheet_link("css/style.css") }}





%{Phalcon_Tag_a8f1c6631032521c06cc1839084f4d16}%

%{Phalcon_Tag_0f37cb31e73c1da4cff65f12d35e6ad3}%

.. code-block:: php

    <?php

     	echo Phalcon\Tag::javascriptInclude("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false);
     	echo Phalcon\Tag::javascriptInclude("javascript/jquery.js");


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     {{ javascript_include("http://{%Phalcon_Tag_c684bea7f4b582a6228add6f5055d7fc%}
     {{ javascript_include("javascript/jquery.js") }}





%{Phalcon_Tag_d92c3d4a395fb847df1bf5be594b0ad8}%

%{Phalcon_Tag_0b694f0be605d97d683b57e1c72a6e97}%

.. code-block:: php

    <?php

     	echo Phalcon\Tag::image("img/bg.png");
     	echo Phalcon\Tag::image(array("img/photo.jpg", "alt" => "Some Photo"));


%{Phalcon_Tag_395bc90089fb5b8bce53dbcc58b4a116}%

.. code-block:: php

    <?php

     	{{ image("img/bg.png") }}
     	{{ image("img/photo.jpg", "alt": "Some Photo") }}
     	{{ image("http://{%Phalcon_Tag_a52c277d84d717d539df9b46d410065a%}





%{Phalcon_Tag_679f7b84d6a90b9c113c7d8c31c4f25d}%

%{Phalcon_Tag_faf994b778a7bcec9794b5f49848b3e7}%

.. code-block:: php

    <?php

     echo Phalcon\Tag::friendlyTitle('These are big important news', '-')





%{Phalcon_Tag_5b73cc59488dddf832d30b8aa6e84787}%

%{Phalcon_Tag_8657648c17bb0a18e1046ec4a878be01}%

%{Phalcon_Tag_3e64f639452ef404f77f1133567f2fd2}%

%{Phalcon_Tag_291b90913ad8f66ace78b77fe91010c9}%

%{Phalcon_Tag_44f2cf4ee05008671e26e8766b8b40aa}%

%{Phalcon_Tag_223b994e1b1abeabc055769874b3f748}%

.. code-block:: php

    <?php

    echo Phalcon\Tag::tagHtml($name, $parameters, $selfClose, $onlyStart, $eol);





%{Phalcon_Tag_adcc5646e912b9f7f8993443587856d4}%

%{Phalcon_Tag_ba6ead3a906a945e4e84c3d9d1f4ccc4}%

