%{Phalcon_DI_Service_bce4cb5f0ffbcaff1491a9c2eb5c50c3}%
==============================

%{Phalcon_DI_Service_584f6725423d9ce1b5331c069cab77d1|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_DI_Service_89207af384e8e8a56cf7e1b92d4641ae}%

.. code-block:: php

    <?php

     $service = new Phalcon\DI\Service('request', 'Phalcon\Http\Request');
     $request = $service->resolve();

.. code-block:: php

    <?php




%{Phalcon_DI_Service_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_DI_Service_8b09a6185bb217df1a9f1cffaa706f84}%

%{Phalcon_DI_Service_840aeffd910980c34a26e59333e34208}%

%{Phalcon_DI_Service_8b52883425393b6e40617949e7427852}%

%{Phalcon_DI_Service_70381128f54df276b9d128421592055e}%

%{Phalcon_DI_Service_364ea876227d6d2fb44da106423702a7}%

%{Phalcon_DI_Service_fac3f3fbc90243c72533b1de9546b836}%

%{Phalcon_DI_Service_a3c27adbab202878c49531613d6c8d69}%

%{Phalcon_DI_Service_3592139e8040ee4f4f2fd03b72537121}%

%{Phalcon_DI_Service_bc070bfdb646ddbdddfb5234aa703106}%

%{Phalcon_DI_Service_6099534bc9b30320cab0e3b217b424ee}%

%{Phalcon_DI_Service_dde10881156c912d4bf77eed51737190}%

%{Phalcon_DI_Service_67a685591a8864d666e2e247084bc9ae}%

%{Phalcon_DI_Service_27e04aeb4054c3fbc2bd0782049799e5}%

%{Phalcon_DI_Service_02031ca6eb3055040b27d93fee49aac9|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_DI_Service_cf1b119ab5b1db8965969ad1361abe05}%

%{Phalcon_DI_Service_1490925474d45a44cb574606dedcdb9b|:doc:`Phalcon\\DI\\Service <Phalcon_DI_Service>`}%

%{Phalcon_DI_Service_f3cc61409d9bdc21e020751eb1f914e0}%

%{Phalcon_DI_Service_70f2e3528443cca3153b53274d98e3a6}%

%{Phalcon_DI_Service_df581a0b2ae27996e10a6e948e5d844b}%

%{Phalcon_DI_Service_26244b1ebfd09ed677a870c844864550}%

%{Phalcon_DI_Service_dc7948459dcd807ae3b60f47323c5572}%

%{Phalcon_DI_Service_01ad17977d856c4882bd38efa9a00938|:doc:`Phalcon\\DI\\Service <Phalcon_DI_Service>`}%

%{Phalcon_DI_Service_d8085f97f04c138d078818f9cc89ddcd}%

