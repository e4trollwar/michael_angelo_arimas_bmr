%{Phalcon_Image_f320ac90a0eb2c715c254f41216d65f7}%
=================================

%{Phalcon_Image_b003776dbe8411732f79ad5c2ecfef16}%

.. code-block:: php

    <?php

    $image = new Phalcon\Image\Adapter\GD("upload/test.jpg");
    $image->resize(200, 200);
    $image->save();




%{Phalcon_Image_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Image_4ab0e7c3266b419acac6e244851ca094}%

%{Phalcon_Image_f5a23f9e0c3ccdf264c307afd0c27244}%

%{Phalcon_Image_a391555e3f7fa450792790d610fdba16}%

%{Phalcon_Image_b156882e6c885ce10f2d6c954c9f823d}%

%{Phalcon_Image_fdac4ce0571de92c40b4928d3e0d83ec}%

%{Phalcon_Image_16b7a8f5b074e8316278e944c77fd8c4}%

%{Phalcon_Image_1e091be0a22d3f4f5953ed8dc4cf5f33}%

%{Phalcon_Image_61157530b68d8d633e307f2f884260e9}%

%{Phalcon_Image_dbc17290cb26b07ecdc0649b015ca473}%

%{Phalcon_Image_2282227ebc7cb35c8e94d8ea00a72940}%

%{Phalcon_Image_1add126b8d63538f0e20c490c3521828}%

