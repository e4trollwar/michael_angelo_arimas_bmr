%{Phalcon_Db_Result_Pdo_0828083cdf5a632f12d3b145e5970207}%
==================================

%{Phalcon_Db_Result_Pdo_02435fbc4675285ebf9d773d1f5c8b5d}%

.. code-block:: php

    <?php

    $result = $connection->query("SELECT * FROM robots ORDER BY name");
    $result->setFetchMode(Phalcon\Db::FETCH_NUM);
    while ($robot = $result->fetchArray()) {
    	print_r($robot);
    }




%{Phalcon_Db_Result_Pdo_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Result_Pdo_69f3825729e6164837df82337f5d79df|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Db_Result_Pdo_95671becb0166b29512dc608a7094cee}%

%{Phalcon_Db_Result_Pdo_6cb95fee57d58d192b5d5c275661d466}%

%{Phalcon_Db_Result_Pdo_b526be733a758777a05fe2ee5c50c09f}%

%{Phalcon_Db_Result_Pdo_92e7ae2944c8193a41ff411498f58f2e}%

%{Phalcon_Db_Result_Pdo_406aab1996eebecfdcc6131391703c40}%

.. code-block:: php

    <?php

    $result = $connection->query("SELECT * FROM robots ORDER BY name");
    $result->setFetchMode(Phalcon\Db::FETCH_OBJ);
    while ($robot = $result->fetch()) {
    	echo $robot->name;
    }





%{Phalcon_Db_Result_Pdo_c956f69c81f1618c16c144e9dd784551}%

%{Phalcon_Db_Result_Pdo_ff5659def57722da08f806d343fb05e6}%

.. code-block:: php

    <?php

    $result = $connection->query("SELECT * FROM robots ORDER BY name");
    $result->setFetchMode(Phalcon\Db::FETCH_NUM);
    while ($robot = $result->fetchArray()) {
    	print_r($robot);
    }





%{Phalcon_Db_Result_Pdo_9a66609b9c098460014071b2f959fb0d}%

%{Phalcon_Db_Result_Pdo_0acf6e22d520ef749da0f899898aa149}%

.. code-block:: php

    <?php

    $result = $connection->query("SELECT * FROM robots ORDER BY name");
    $robots = $result->fetchAll();





%{Phalcon_Db_Result_Pdo_4704cd97d2087988f8c9b610439331ae}%

%{Phalcon_Db_Result_Pdo_27c5e99418bbc02320a4a25ca68fe1ce}%

.. code-block:: php

    <?php

    $result = $connection->query("SELECT * FROM robots ORDER BY name");
    echo 'There are ', $result->numRows(), ' rows in the resulset';





%{Phalcon_Db_Result_Pdo_1efb9bde4860fd153b55a780cdfea357}%

%{Phalcon_Db_Result_Pdo_a83610191f2de8beca7b5a96b1529347}%

.. code-block:: php

    <?php

    $result = $connection->query("SELECT * FROM robots ORDER BY name");
    $result->dataSeek(2); // {%Phalcon_Db_Result_Pdo_a85faa8f7e9f294bd50364b0d5f3ca4f%}
    $row = $result->fetch(); // {%Phalcon_Db_Result_Pdo_519cc71d343fb671dafe3f523625ba90%}





%{Phalcon_Db_Result_Pdo_efa34719536417da86785d31e11422bf}%

%{Phalcon_Db_Result_Pdo_8de8983086310b17c5c37bc3ca4d9db2}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Result_Pdo_811f3584d7da7092393581209bac9642%}
    $result->setFetchMode(Phalcon\Db::FETCH_NUM);
    
    //{%Phalcon_Db_Result_Pdo_6379afa0a06b4f1059e507a8e229c70f%}
    $result->setFetchMode(Phalcon\Db::FETCH_ASSOC);
    
    //{%Phalcon_Db_Result_Pdo_d8ab094b6141c91025e78868624fabcd%}
    $result->setFetchMode(Phalcon\Db::FETCH_BOTH);
    
    //{%Phalcon_Db_Result_Pdo_008db542a38a0e3b4e94e5d74fbb07b0%}
    $result->setFetchMode(Phalcon\Db::FETCH_OBJ);





%{Phalcon_Db_Result_Pdo_dd3c05eadefe55da301e6a56905aa4f3}%

%{Phalcon_Db_Result_Pdo_d8b9ef5e81f6cd5ab3a24d1df8b53802}%

%{Phalcon_Db_Result_Pdo_dcafee59cacdaa2a6b52c3f90c0d557a}%

%{Phalcon_Db_Result_Pdo_dbad6ac2067dc5970c7a20049b1eee1f}%

