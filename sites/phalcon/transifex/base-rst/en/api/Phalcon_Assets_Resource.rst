%{Phalcon_Assets_Resource_8014ae8b97539bbb5fe8a095577a18de}%
===================================

%{Phalcon_Assets_Resource_c3c12520dba6f402969e616293237d60}%

.. code-block:: php

    <?php

     $resource = new Phalcon\Assets\Resource('js', 'javascripts/jquery.js');




%{Phalcon_Assets_Resource_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Assets_Resource_c653ee76ed5cf908dafca7426a3d2989}%

%{Phalcon_Assets_Resource_efac386a47d75ea3b932035f0f5b70f8}%

%{Phalcon_Assets_Resource_5ea8343914ecc0e2b9d2155e486c1ddf|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_278e33f1a9f90cff220486db2c13a770}%

%{Phalcon_Assets_Resource_9b6af70c1c9980c497b487b7e2dbbccd}%

%{Phalcon_Assets_Resource_8a5633ae27520e670048978f114c8b0c}%

%{Phalcon_Assets_Resource_3edacf8924d30afd7568218360ad28b0|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_50af1fcd1a07f5c5a71b48c5477d8322}%

%{Phalcon_Assets_Resource_9417996226352ca1db0042f9b723781e}%

%{Phalcon_Assets_Resource_4646ae4ed1f40a3e3f646db27ca8ee4c}%

%{Phalcon_Assets_Resource_6107a1d8b549037bb63d04ef0f5cfe08|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_698fd0c3ef4201823c78dbb1b56ffc18}%

%{Phalcon_Assets_Resource_2451dcc98429165b9f937433e9b5365b}%

%{Phalcon_Assets_Resource_a9bbee79d2548820bbb8944d8f96e79c}%

%{Phalcon_Assets_Resource_5d2f8b83683064b70da92e30e75951aa|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_6cd85d183bac64f7f45634cd7b3a76cf}%

%{Phalcon_Assets_Resource_e41e53ece73ef2e5ac058769e9f5339f}%

%{Phalcon_Assets_Resource_f2a95d47a9452e8ea6269a4bd84bd5e9}%

%{Phalcon_Assets_Resource_e47792ec9a752243cc76878fe1a3b5b2|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_4899df532704910b3fd622388b455641}%

%{Phalcon_Assets_Resource_7a67fb8e2d5cd3fb40391d6545fe5a33}%

%{Phalcon_Assets_Resource_6f9176d6e27af41da8a4762fe4306bd7}%

%{Phalcon_Assets_Resource_a24b7d6863ab65215ed4ab112809e4c9|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_212382df8fe0643739164b498b3de1cd}%

%{Phalcon_Assets_Resource_0e58ec1db6a882ce35c3694af57f632c}%

%{Phalcon_Assets_Resource_c371ec70fb7994a5b9fe8e6c3e10f8ab}%

%{Phalcon_Assets_Resource_d5e315cce8b3eaa5516af10384b68bf6|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_41971b7293fa7dab7986660ba54a761e}%

%{Phalcon_Assets_Resource_f3867183e7e318eb62b356e3f0b3c2d4}%

%{Phalcon_Assets_Resource_be5dc25c52daece8afc15d755329f0e7}%

%{Phalcon_Assets_Resource_b877558599ddbb28381744908d2c3fbd|:doc:`Phalcon\\Assets\\Resource <Phalcon_Assets_Resource>`}%

%{Phalcon_Assets_Resource_462a62ce51e5c8ad20c7369299f8b4e8}%

%{Phalcon_Assets_Resource_3b91b04edfebf62fb8e976b756f8c10b}%

%{Phalcon_Assets_Resource_be5dc25c52daece8afc15d755329f0e7}%

%{Phalcon_Assets_Resource_f05e9a51b8d1503babccbf550dbc136e}%

%{Phalcon_Assets_Resource_fa46255611e0aae04f5a4e1b36d55863}%

%{Phalcon_Assets_Resource_99d32ecfe5452b534a009d6bf74d94de}%

%{Phalcon_Assets_Resource_f8547c0b8e36476422020802fc06f093}%

%{Phalcon_Assets_Resource_db11b5ef593d734aaafe564449875b67}%

%{Phalcon_Assets_Resource_32fea5592d7a8d8e6f214664fe0b69ff}%

%{Phalcon_Assets_Resource_705bb7e7c348a89c2ec8aba8d7a6b159}%

%{Phalcon_Assets_Resource_1bbeea56572e8d26d91a5b8bd95d5236}%

