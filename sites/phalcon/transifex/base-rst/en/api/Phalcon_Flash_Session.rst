%{Phalcon_Flash_Session_1351fc9ced941c9e7e990a8ee9cc4e36}%
=================================

%{Phalcon_Flash_Session_650e853ed528ad34ec155fc14dd2158c|:doc:`Phalcon\\Flash <Phalcon_Flash>`}%

%{Phalcon_Flash_Session_a18feb546f7f5b0797b6e5f1d4459efb|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Flash_Session_c4c7e29f91d00020a9f4ac11b132dd3d}%

%{Phalcon_Flash_Session_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Flash_Session_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Flash_Session_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Flash_Session_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Flash_Session_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Flash_Session_f4ba91ca15cf7e8b0d4ca8c202e04988}%

%{Phalcon_Flash_Session_90a21f9587c743c2a2613d9df56ebce0}%

%{Phalcon_Flash_Session_bd513ec0afad5818e4bbe53430ac3cde}%

%{Phalcon_Flash_Session_67063c22bf9ddc5a50c54898352d97b7}%

%{Phalcon_Flash_Session_4bcebc2251d463c7294519fcd3b15f5b}%

%{Phalcon_Flash_Session_ddce8de60de7656f5625ebead8a44a0b}%

%{Phalcon_Flash_Session_e86f71342ad94a01e2f1bea64138920c}%

%{Phalcon_Flash_Session_b203535f9433c712877dc1e87f861860}%

%{Phalcon_Flash_Session_0a61d4ab204a7f7ea5337d793dc97d37}%

%{Phalcon_Flash_Session_652676a5c06428a60c7624f11419adad}%

%{Phalcon_Flash_Session_568252f42275782ecd0f3ec7c26dedad}%

%{Phalcon_Flash_Session_06d3f18175e224a0d674920abed5055b}%

%{Phalcon_Flash_Session_7055f64235da4d2d063dbb0cdf056e82}%

%{Phalcon_Flash_Session_3527d7ad93db44794390a7e8cd6350dc}%

%{Phalcon_Flash_Session_d1a4e406d0f85ca72d6f64d56d18f5e7|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Session_e612daac8bf7fcddafa12e533e332a89}%

%{Phalcon_Flash_Session_36cf107f43e3e9d89a208fd76729bfd9|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Session_3bb4ebbf217a341db54dc9fc4b49b66e}%

%{Phalcon_Flash_Session_8fbb66fffb0d8814130ef9ce28472fa2|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Session_c14da91f6f71b7c71f50da51d2f1c14a}%

%{Phalcon_Flash_Session_f2c88f1773e1e571d65a97e113b9a832}%

%{Phalcon_Flash_Session_508f3a050e254578328bc958df18cb4a}%

.. code-block:: php

    <?php

     $flash->error('This is an error');





%{Phalcon_Flash_Session_075283601b82def0f7964940af2d2bfb}%

%{Phalcon_Flash_Session_f5bb0b1e9d9d0f9558960a1e42afcf04}%

.. code-block:: php

    <?php

     $flash->notice('This is an information');





%{Phalcon_Flash_Session_2778b44ae3d3a209cf9f94bea9d83efa}%

%{Phalcon_Flash_Session_26239272d000aa42450fb5cc48f612ae}%

.. code-block:: php

    <?php

     $flash->success('The process was finished successfully');





%{Phalcon_Flash_Session_3cbf30a72bda1909c61c7bffaba66e9d}%

%{Phalcon_Flash_Session_545dfddf25dafd6fe2ad13a5294c1da4}%

.. code-block:: php

    <?php

     $flash->warning('Hey, this is important');





%{Phalcon_Flash_Session_437d6c35e4ba290aa4e98338916ad0fc}%

%{Phalcon_Flash_Session_bb78ddd310175faeaab1d79dc924b66e}%

