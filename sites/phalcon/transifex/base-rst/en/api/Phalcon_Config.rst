%{Phalcon_Config_2b503f65b0ee7f83949a615b5fa2b662}%
=========================

%{Phalcon_Config_a657d49ad72f1e42396addc6c96a1425}%

%{Phalcon_Config_efd72e6f1501b08fcd5651053bdc6a3e}%

.. code-block:: php

    <?php

    $config = new Phalcon\Config(array(
    	"database" => array(
    		"adapter" => "Mysql",
    		"host" => "localhost",
    		"username" => "scott",
    		"password" => "cheetah",
    		"dbname" => "test_db"
    	),
    	"phalcon" => array(
    		"controllersDir" => "../app/controllers/",
    		"modelsDir" => "../app/models/",
    		"viewsDir" => "../app/views/"
    	)
     ));




%{Phalcon_Config_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Config_de93ac8bd144d23942c5090228426f86}%

%{Phalcon_Config_1b7ffa4f651ac33c59e384caa66569d3}%

%{Phalcon_Config_87480220bbdc87f5b55fced97c5105c6}%

%{Phalcon_Config_cef9ef423d3226f1df89e6c695359390}%

.. code-block:: php

    <?php

     var_dump(isset($config['database']));





%{Phalcon_Config_a7e10f1e18fa2a0de9f8015d263eb1c3}%

%{Phalcon_Config_0b8c0846e2f4b6b32161721525431ec3}%

.. code-block:: php

    <?php

     echo $config->get('controllersDir', '../app/controllers/');





%{Phalcon_Config_ff90f5a5915b3332269e3dd05e7bc770}%

%{Phalcon_Config_cb9d856c70df42bb2d3346be63f239cc}%

.. code-block:: php

    <?php

     print_r($config['database']);





%{Phalcon_Config_0115a5a335543024edd583c21c5ce327}%

%{Phalcon_Config_a8bce44be354ca47d470b0631b08fccf}%

.. code-block:: php

    <?php

     $config['database'] = array('type' => 'Sqlite');





%{Phalcon_Config_3644e01a19440baa19219140adc68825}%

%{Phalcon_Config_bee829db6cea0d7a64fd97770ff67610}%

.. code-block:: php

    <?php

     unset($config['database']);





%{Phalcon_Config_1dbe35695150e1fee50b2315c874eebd|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_cb6ab50e7931dd69b9e01ede416ae818}%

.. code-block:: php

    <?php

    $appConfig = new Phalcon\Config(array('database' => array('host' => 'localhost')));
    $globalConfig->merge($appConfig);





%{Phalcon_Config_94cda9aff69544f071844a8c2e06c127}%

%{Phalcon_Config_12680a2b6379e22d5147df5a8a4f0267}%

.. code-block:: php

    <?php

    print_r($config->toArray());





%{Phalcon_Config_dac5cec50eda033a74b25dcc1b83e1ba}%

%{Phalcon_Config_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_7440f57c1f9f040cb83d3d4b36f316c1}%

%{Phalcon_Config_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_b7d560c22b16e83e935512cf12962998|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_b23e852f6995564547321fb333c04912}%

%{Phalcon_Config_e53e39ef9124fdaf502df86665d52fe5}%

%{Phalcon_Config_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_0014d861097168e8f304d9a82973633a}%

%{Phalcon_Config_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_f8457b6e6bc7c32562b3138b58b22342}%

%{Phalcon_Config_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_19ed33479cdae693d8ef869f55426eb1}%

%{Phalcon_Config_68cf0a3bff1e41b907cf955f570c5249}%

