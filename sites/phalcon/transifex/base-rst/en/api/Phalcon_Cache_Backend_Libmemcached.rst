%{Phalcon_Cache_Backend_Libmemcached_bfe3803b91f88d16a15bd13570de0903}%
===============================================

%{Phalcon_Cache_Backend_Libmemcached_797e5d789434d10ddd0a11dc0d3a9e57|:doc:`Phalcon\\Cache\\Backend <Phalcon_Cache_Backend>`}%

%{Phalcon_Cache_Backend_Libmemcached_c1eac5b56048ee7e86f6a6737e5ede2a|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Cache_Backend_Libmemcached_6329c7e4eab2f471a5e49a87abdc4aa6}%

.. code-block:: php

    <?php

     // {%Phalcon_Cache_Backend_Libmemcached_77c80c813b109e091cbde98ceccddd0c%}
     $frontCache = new Phalcon\Cache\Frontend\Data(array(
        "lifetime" => 172800
     ));
    
     //{%Phalcon_Cache_Backend_Libmemcached_0748df875c42080145b360c2eb6a826b%}
     $cache = new Phalcon\Cache\Backend\Libmemcached($frontCache, array(
         'servers' => array(
             array('host' => 'localhost',
                   'port' => 11211,
                   'weight' => 1),
         ),
         'client' => array(
             Memcached::OPT_HASH => Memcached::HASH_MD5,
             Memcached::OPT_PREFIX_KEY => 'prefix.',
         )
     ));
    
     //{%Phalcon_Cache_Backend_Libmemcached_f3197c319b2e1bb4ba59291a6ad75fa9%}
     $cache->save('my-data', array(1, 2, 3, 4, 5));
    
     //{%Phalcon_Cache_Backend_Libmemcached_45b6351e81b39eda563b06fcc2f2b1ff%}
     $data = $cache->get('my-data');




%{Phalcon_Cache_Backend_Libmemcached_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Backend_Libmemcached_d0ebe8723e443cd9102587c6adecc491|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Backend_Libmemcached_03087d6bc28087ba41311f2099806a18}%

%{Phalcon_Cache_Backend_Libmemcached_8a0674d1f725f386bd115d5bcb5bd8a8}%

%{Phalcon_Cache_Backend_Libmemcached_777746fe2fd4688c247c55fd10219277}%

%{Phalcon_Cache_Backend_Libmemcached_051aecfbd55511e7172defa011547cfe}%

%{Phalcon_Cache_Backend_Libmemcached_3bff3193b42b39b97ac714131c9e7a9c}%

%{Phalcon_Cache_Backend_Libmemcached_b7167b7aa44b968e246ab55a957dc5ae}%

%{Phalcon_Cache_Backend_Libmemcached_4165fe6a657540be3737c4b4bea64980}%

%{Phalcon_Cache_Backend_Libmemcached_92a86099a531589aac19438e058b04bf}%

%{Phalcon_Cache_Backend_Libmemcached_5a3c26a229b51db139ac1ff3258ce404}%

%{Phalcon_Cache_Backend_Libmemcached_d073186049f74018e6ac389752e0231d}%

%{Phalcon_Cache_Backend_Libmemcached_0496bd63d7dd49a90bc039bf152277c2}%

%{Phalcon_Cache_Backend_Libmemcached_1802823d299420e98af3a958b7d763e9}%

%{Phalcon_Cache_Backend_Libmemcached_92afc5ac5ca4be06975fc9ff5ba4e195}%

%{Phalcon_Cache_Backend_Libmemcached_51b9f069d953ee0838b4029d1623b43f}%

%{Phalcon_Cache_Backend_Libmemcached_0e8880af8ead34ea21563440008906ce}%

%{Phalcon_Cache_Backend_Libmemcached_458a4b4698c693cac55078739b14aecd}%

%{Phalcon_Cache_Backend_Libmemcached_ab27e2da0452b8fdddf3ce3f31ddc8a0}%

%{Phalcon_Cache_Backend_Libmemcached_243f7860634f5addc7cc2cd15ec9587b}%

%{Phalcon_Cache_Backend_Libmemcached_5fbdd81239a6a360b99505e1ccc4c96a}%

%{Phalcon_Cache_Backend_Libmemcached_eb191cdb38d6b4e214a4267194982ed5}%

%{Phalcon_Cache_Backend_Libmemcached_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Cache_Backend_Libmemcached_ee227a18eb8ce66b10fcd49fbaedbcef}%

%{Phalcon_Cache_Backend_Libmemcached_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Cache_Backend_Libmemcached_4c1e2a7813fc357bd5b2961fe618cf57}%

%{Phalcon_Cache_Backend_Libmemcached_5aa5eebbc6e2e0ba1eb669cfcf9314ac}%

%{Phalcon_Cache_Backend_Libmemcached_f8bbbf10eb7b23d1ad6555244df95daa}%

%{Phalcon_Cache_Backend_Libmemcached_ee84c5ddac90bc82f87d82edd83f803d}%

%{Phalcon_Cache_Backend_Libmemcached_20b1a896cfa41a581678813a40ad294b}%

%{Phalcon_Cache_Backend_Libmemcached_a45d0bbd7f1d2ca9be308e066116571d}%

%{Phalcon_Cache_Backend_Libmemcached_f4c67e395756ed811b3d86ccd37680d7}%

%{Phalcon_Cache_Backend_Libmemcached_0a7c1cf9fc6cf16b0c6d365b453b8c9c}%

%{Phalcon_Cache_Backend_Libmemcached_6e3b53a9e2f02ef15c6e622b43ad7fd6}%

%{Phalcon_Cache_Backend_Libmemcached_2e2e8a1edb8ee6d249c3408ed9a13506}%

%{Phalcon_Cache_Backend_Libmemcached_f616a41f5f598297276e1c7ec3a0c216}%

%{Phalcon_Cache_Backend_Libmemcached_132cc48b9d06f81662724fafbcf5388d}%

%{Phalcon_Cache_Backend_Libmemcached_25608a5b33db734ab297a041c17d0afe}%

%{Phalcon_Cache_Backend_Libmemcached_4f309f2cbbf78c9cc272007b4b90e759}%

%{Phalcon_Cache_Backend_Libmemcached_d95154d7678637473d5bd3651a6b7981}%

%{Phalcon_Cache_Backend_Libmemcached_63ef827e599ff5e9ce28cac18e0a74de}%

%{Phalcon_Cache_Backend_Libmemcached_8f54d16975ae8256cc1f314641e21364}%

%{Phalcon_Cache_Backend_Libmemcached_7b222737002802a81ee0a69bdb497f3a}%

