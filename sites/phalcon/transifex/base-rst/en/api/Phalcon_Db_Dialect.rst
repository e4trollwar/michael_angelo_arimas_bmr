%{Phalcon_Db_Dialect_fbf5cd9033bb78e833c957a2927f5ac8}%
=======================================

%{Phalcon_Db_Dialect_63ac1a796a42ab6bf74eecef37fe105a|:doc:`Phalcon\\Db\\DialectInterface <Phalcon_Db_DialectInterface>`}%

%{Phalcon_Db_Dialect_8115f9d028c4a71d5460f6debfff771b}%

%{Phalcon_Db_Dialect_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Dialect_8840498d1022a28356e91127956e4f2b}%

%{Phalcon_Db_Dialect_52d966c8fc82475765b525c9678b710b}%

.. code-block:: php

    <?php

     $sql = $dialect->limit('SELECT * FROM robots', 10);
     echo $sql; // {%Phalcon_Db_Dialect_ca03c429c7fcbcb5e0abe54197ccf503%}





%{Phalcon_Db_Dialect_cf8e2d041b6c93ed62d2c1f329d795d0}%

%{Phalcon_Db_Dialect_7002de3890fd2c005a97f19d1eafbd78}%

.. code-block:: php

    <?php

     $sql = $dialect->forUpdate('SELECT * FROM robots');
     echo $sql; // {%Phalcon_Db_Dialect_c581ef57ed270e78b1ab818a742dacf6%}





%{Phalcon_Db_Dialect_cdbf0c7a0d7d45d33cb674d050537cf2}%

%{Phalcon_Db_Dialect_3581d9894b3b47e07a56f2a95f7ddbe9}%

.. code-block:: php

    <?php

     $sql = $dialect->sharedLock('SELECT * FROM robots');
     echo $sql; // {%Phalcon_Db_Dialect_247f3da6d75f482b27cc74d986547a3c%}





%{Phalcon_Db_Dialect_dcd0543b55d4946db59a4240b10ada74}%

%{Phalcon_Db_Dialect_bd1034394e086147db974565cd62c526}%

.. code-block:: php

    <?php

     echo $dialect->getColumnList(array('column1', 'column'));





%{Phalcon_Db_Dialect_76ecc18d831a73c3ebfeb4a53d034c3b}%

%{Phalcon_Db_Dialect_89d2b02c18418315d930416bf30d7abc}%

%{Phalcon_Db_Dialect_8853df14187a80463eae43a0195edd66}%

%{Phalcon_Db_Dialect_4a336b00cd8c57bdad466e8f7d1e085f}%

%{Phalcon_Db_Dialect_b99d31bddcc38cd470da30b847f0bbd6}%

%{Phalcon_Db_Dialect_920bf11618d4431c4cd50f652ad71973}%

%{Phalcon_Db_Dialect_edff408031b59778c9c1ece4bf9dd8ac}%

%{Phalcon_Db_Dialect_1565b67ca91624a4a4a823e8ed91dc9c}%

%{Phalcon_Db_Dialect_c865a2df9b468b68b4672b2ed5ad2796}%

%{Phalcon_Db_Dialect_f3ee491156b47a965d2f3e1245379ebb}%

%{Phalcon_Db_Dialect_4e7f9fbd7cbf64f30c7649ba976dcc4a}%

%{Phalcon_Db_Dialect_e5ced1df78c13c5c62d1e58f7cfd63a1}%

%{Phalcon_Db_Dialect_21a928469b8530d98afb8f7193a6bec9}%

%{Phalcon_Db_Dialect_1ed3c999236ff62f896692da89d43a51}%

%{Phalcon_Db_Dialect_ead17b7e9f1334d5cf67b3615a583e8a}%

%{Phalcon_Db_Dialect_6fbb66dee4faaf07d7914f9a5e1cb00e}%

%{Phalcon_Db_Dialect_c3b88e3b81b01cb7ff59375fe0bc07fa|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Dialect_5e766ca3ac23b9e250bcf56e50b0bf55}%

%{Phalcon_Db_Dialect_d6e2294837de613c5f7d20736e37da5f|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Dialect_25cfa707a466e80c5a1b79cab53e10d0}%

%{Phalcon_Db_Dialect_add128a874f10f16bdd3260978b52bb0|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Dialect_b058dfa0245ea7e8bd22985bea62b2c6}%

%{Phalcon_Db_Dialect_8d1f418bb780ed01a13e2b43df26b46f}%

%{Phalcon_Db_Dialect_c7c8b45ab9e084096ba63b247cbb5394}%

%{Phalcon_Db_Dialect_34619aadafa55b5bef57884cb0be3fd0|:doc:`Phalcon\\Db\\IndexInterface <Phalcon_Db_IndexInterface>`}%

%{Phalcon_Db_Dialect_b67c92d9bf1d1079045c7e4cee55a8af}%

%{Phalcon_Db_Dialect_828b1d41bd8f0d5a729c34663d218c1c}%

%{Phalcon_Db_Dialect_b73e7e0ddb6af63bf574d341516f3b28}%

%{Phalcon_Db_Dialect_96f456a055a0f94b50631625021fbae8|:doc:`Phalcon\\Db\\IndexInterface <Phalcon_Db_IndexInterface>`}%

%{Phalcon_Db_Dialect_dcf061f20dd16b8fec02dfc0e6b0cd85}%

%{Phalcon_Db_Dialect_deec700186a27b0969b378087ccb2137}%

%{Phalcon_Db_Dialect_887d9e3d8ff92a764c34c4da28a8cfbf}%

%{Phalcon_Db_Dialect_c28b240dcf381d64534faab818614450|:doc:`Phalcon\\Db\\ReferenceInterface <Phalcon_Db_ReferenceInterface>`}%

%{Phalcon_Db_Dialect_b67c92d9bf1d1079045c7e4cee55a8af}%

%{Phalcon_Db_Dialect_dc1c4322aa610eb88ee5a61c8b9ac102}%

%{Phalcon_Db_Dialect_524384868ab8de535006fce15c469bbe}%

%{Phalcon_Db_Dialect_71f9d13a0cd16f6789ab777be28b5c32}%

%{Phalcon_Db_Dialect_9701bd70d3519f4d2cfcf2c62bc865ca}%

%{Phalcon_Db_Dialect_eb1a83b6cdfc2d1c408e95627e4daed3}%

%{Phalcon_Db_Dialect_5ba9bf6b7553c075e551b79a056355bc}%

%{Phalcon_Db_Dialect_23a9100e7411ab46bae4c813d5e257ad}%

%{Phalcon_Db_Dialect_0eb2b37d3b3bf3de62d1651b960afd7c}%

%{Phalcon_Db_Dialect_3313ad07bbe965a100b4fc03b9b672c7}%

%{Phalcon_Db_Dialect_e958816ea7301c10ae61a3496f8ec523}%

%{Phalcon_Db_Dialect_adc144127f569430bd7d5a5626acf74a}%

%{Phalcon_Db_Dialect_e1cb8bf0d531979a026aa558da550fcd}%

%{Phalcon_Db_Dialect_95882d46d881d4bdce1605ef25e8c84a}%

%{Phalcon_Db_Dialect_1f3919bb87232f39dea5d14809509f6c}%

%{Phalcon_Db_Dialect_8ddbaaa0f369cb79ba4eb30027ec271d}%

%{Phalcon_Db_Dialect_2e6f6cde532cb4e2cfb882aa55e6a84e}%

%{Phalcon_Db_Dialect_9705f84268adf077b81e6e31bd07b72e}%

%{Phalcon_Db_Dialect_7c6c548d6cf998d8ee54f6ccd3227248}%

%{Phalcon_Db_Dialect_066f3fd0dc22a5cc3a816f23a9aa3bfa}%

%{Phalcon_Db_Dialect_1e84fb3bb2d773732d0cd493ed1678c9}%

%{Phalcon_Db_Dialect_5e99f1ad849028e2c48abf45b42b07ee}%

%{Phalcon_Db_Dialect_0733e0f2aac399bbc7af53ddd9ab3f5e}%

%{Phalcon_Db_Dialect_8b1d720de00790425c7f84c2acdfed14}%

%{Phalcon_Db_Dialect_f25e7cbd91858aa49a772a297227568f}%

%{Phalcon_Db_Dialect_6b62940ddc377e48b317d9e8cb18b22f}%

%{Phalcon_Db_Dialect_096c12d210232448b37199764a87a254}%

