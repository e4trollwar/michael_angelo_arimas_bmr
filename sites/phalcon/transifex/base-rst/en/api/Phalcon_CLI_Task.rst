%{Phalcon_CLI_Task_7e344ba856ef0344c9db14c9f9dfdb42}%
============================

%{Phalcon_CLI_Task_32f67d09941ad6b6963c440e030d031e|:doc:`Phalcon\\DI\\Injectable <Phalcon_DI_Injectable>`}%

%{Phalcon_CLI_Task_cb143a03062fad8680104c800ead4e79|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_CLI_Task_44e5a2e73f469285cfcf0dbe95db04b5}%

.. code-block:: php

    <?php

    class HelloTask extends \Phalcon\CLI\Task
    {
    
      //{%Phalcon_CLI_Task_83c53c0baa74bee3cacc39d1eccca0d3%}
      public function mainAction()
      {
    
      }
    
      public function findAction()
      {
    
      }
    
    }




%{Phalcon_CLI_Task_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_CLI_Task_d98a2143a4e9b08520a7df0009a78266|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Task_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_CLI_Task_3e7e4ddacc724c78120150572bdab704|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Task_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_CLI_Task_5e6f785f2df62ae1a2a672663fb6b2e4|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_CLI_Task_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_CLI_Task_8d859c2f0efce8383f08ae517e34a4af|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_CLI_Task_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_CLI_Task_aa385da180999c3488ab22e043b6eb9d}%

%{Phalcon_CLI_Task_a8ec8322461cb1dce1953c9378484594}%

