%{Phalcon_Security_624e1c7207078cd6a8234d255ff8ed9c}%
===========================

%{Phalcon_Security_0b066e4bf73264afdc3e24f71410b010|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Security_389478bd5aab7780df98f564c7d5a5f9}%

.. code-block:: php

    <?php

    $login = $this->request->getPost('login');
    $password = $this->request->getPost('password');
    
    $user = Users::findFirstByLogin($login);
    if ($user) {
    	if ($this->security->checkHash($password, $user->password)) {
    		//{%Phalcon_Security_d6c5533d83067194760edb4307f97b3d%}
    	}
    }




%{Phalcon_Security_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Security_590588f044766b3628a897e4921b54ea}%

%{Phalcon_Security_6954034beb171e6aa0b797d9d88fc09b}%

%{Phalcon_Security_f0e06769fb4c9ef166703f6bbb3fbcbd}%

%{Phalcon_Security_95d7222514ad4d425ead25a1f21af898}%

%{Phalcon_Security_2795afb1f6fa36065d0b64c4e354acb3}%

%{Phalcon_Security_ea4ee31103084d4be3e45abf7c157811}%

%{Phalcon_Security_edb5f5fa9ff6f8863b025f267c17729c}%

%{Phalcon_Security_fc76a9d3afde04f6cbd1e8a373be5d0d}%

%{Phalcon_Security_95a270042da1f85378aada2229cca77a}%

%{Phalcon_Security_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Security_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Security_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Security_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Security_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Security_cc831215dbb1f79a743b094acb6eeae0}%

%{Phalcon_Security_77fee9c506a5cbc49a7de26a8de1c90e}%

%{Phalcon_Security_838896220a9f70a0fe5b74cecacbef4b}%

%{Phalcon_Security_c5579b6d80d5ab67af857714757a67c7}%

%{Phalcon_Security_0f9e88dc52bfa52fad66c67d44084522}%

%{Phalcon_Security_1e387db7742cf15e20c9438b27b621d1}%

%{Phalcon_Security_d743321bd61d546ae52fa9fbab3a8145}%

%{Phalcon_Security_66e8bb81ed8d2c0a2b9a9385e55905d5}%

%{Phalcon_Security_071a1a20666fce1c97285c49d7433426}%

%{Phalcon_Security_9359f4bb466192bd6dd0e947d4340c3c}%

%{Phalcon_Security_27605ddcd036dec8c2889690489a1256}%

%{Phalcon_Security_324a65d2275fd885bae60182e54e100e}%

%{Phalcon_Security_ef558d4ed44e7d4c657518eabe9cf1b5}%

%{Phalcon_Security_b76b4ca14105a4b051a807bb5d3f2af0}%

%{Phalcon_Security_6a6238f7523a9598f345d94d04924cf2}%

%{Phalcon_Security_d8f795411e1c7ebe60633fa7ccb7c6e1}%

%{Phalcon_Security_17a5c77cec051b028914923fe20bf5da}%

%{Phalcon_Security_e2cd51dad1e19f0c15f2bab100382a38}%

%{Phalcon_Security_2ff82ba66b1c341c34f1f7db855edb20}%

%{Phalcon_Security_428b2f9ecca4681730747f00b509c870}%

%{Phalcon_Security_1d9066408f6fc2401fb65ce387392d83}%

%{Phalcon_Security_84b30c956e5f6f6bbf1fd47fc62a2bdb}%

%{Phalcon_Security_556113b22a57e5c2eaca0a3790e13566}%

%{Phalcon_Security_aeb790051d9b2f02d9addde9f4a6abce}%

%{Phalcon_Security_8bc8bc97979c5b78aa95cb516043c23f}%

%{Phalcon_Security_53e8794cd1152fa150a1a6ee32281d20}%

%{Phalcon_Security_61be3a2227c447588d77c6dec428bfcc}%

%{Phalcon_Security_bdfcba33c602f169ba5c9c2bfc8540fc}%

%{Phalcon_Security_220c4880254e9996eaed56b9d687fc85}%

%{Phalcon_Security_00d844fadba288bbc4d82f7645abc57d}%

%{Phalcon_Security_821e9c8c0849b83f4ccec3f1072a17e2}%

%{Phalcon_Security_2bc9091cbc90860b7aad446451d3dc1b}%

%{Phalcon_Security_d86ab5bc19a41d630173b71ee040e9f7}%

