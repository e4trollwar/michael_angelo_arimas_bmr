%{Phalcon_Cache_Backend_Mongo_92e746536cc350a7789712627eda5111}%
========================================

%{Phalcon_Cache_Backend_Mongo_797e5d789434d10ddd0a11dc0d3a9e57|:doc:`Phalcon\\Cache\\Backend <Phalcon_Cache_Backend>`}%

%{Phalcon_Cache_Backend_Mongo_c1eac5b56048ee7e86f6a6737e5ede2a|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Cache_Backend_Mongo_737f5c2225392fb30af3c103bb8440b5}%

.. code-block:: php

    <?php

     // {%Phalcon_Cache_Backend_Mongo_77c80c813b109e091cbde98ceccddd0c%}
     $frontCache = new Phalcon\Cache\Frontend\Base64(array(
    	"lifetime" => 172800
     ));
    
     //{%Phalcon_Cache_Backend_Mongo_eb7a3fc32d22e8cc1c9ce92e660a2507%}
     $cache = new Phalcon\Cache\Backend\Mongo($frontCache, array(
    	'server' => "mongodb://{%Phalcon_Cache_Backend_Mongo_22ac3e0903d9afe095530ddf5ccb752f%}
          'db' => 'caches',
    	'collection' => 'images'
     ));
    
     //{%Phalcon_Cache_Backend_Mongo_f3197c319b2e1bb4ba59291a6ad75fa9%}
     $cache->save('my-data', file_get_contents('some-image.jpg'));
    
     //{%Phalcon_Cache_Backend_Mongo_45b6351e81b39eda563b06fcc2f2b1ff%}
     $data = $cache->get('my-data');




%{Phalcon_Cache_Backend_Mongo_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Backend_Mongo_d0ebe8723e443cd9102587c6adecc491|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Backend_Mongo_592697730fe3639917c7adc9cd0e7c78}%

%{Phalcon_Cache_Backend_Mongo_09a578f57c2a54883dfafb0ba822f3e5}%

%{Phalcon_Cache_Backend_Mongo_47124c702afed7f98b2627813300836f}%

%{Phalcon_Cache_Backend_Mongo_051aecfbd55511e7172defa011547cfe}%

%{Phalcon_Cache_Backend_Mongo_3bff3193b42b39b97ac714131c9e7a9c}%

%{Phalcon_Cache_Backend_Mongo_b7167b7aa44b968e246ab55a957dc5ae}%

%{Phalcon_Cache_Backend_Mongo_302455c811d123d722817389f29cc4d9}%

%{Phalcon_Cache_Backend_Mongo_92a86099a531589aac19438e058b04bf}%

%{Phalcon_Cache_Backend_Mongo_5a3c26a229b51db139ac1ff3258ce404}%

%{Phalcon_Cache_Backend_Mongo_d073186049f74018e6ac389752e0231d}%

%{Phalcon_Cache_Backend_Mongo_0496bd63d7dd49a90bc039bf152277c2}%

%{Phalcon_Cache_Backend_Mongo_1802823d299420e98af3a958b7d763e9}%

%{Phalcon_Cache_Backend_Mongo_92afc5ac5ca4be06975fc9ff5ba4e195}%

%{Phalcon_Cache_Backend_Mongo_dddbd663c300ae3467ce914b9ff1906c}%

%{Phalcon_Cache_Backend_Mongo_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Cache_Backend_Mongo_51b9f069d953ee0838b4029d1623b43f}%

%{Phalcon_Cache_Backend_Mongo_dbd2956699d72e3ee7c7cda09a23041d}%

%{Phalcon_Cache_Backend_Mongo_458a4b4698c693cac55078739b14aecd}%

%{Phalcon_Cache_Backend_Mongo_fb4fc53706335672f646926bce873ee0}%

%{Phalcon_Cache_Backend_Mongo_4e4d70232e83016eb0c6ebfe427a2d2a}%

%{Phalcon_Cache_Backend_Mongo_5fbdd81239a6a360b99505e1ccc4c96a}%

%{Phalcon_Cache_Backend_Mongo_4c1e2a7813fc357bd5b2961fe618cf57}%

%{Phalcon_Cache_Backend_Mongo_5aa5eebbc6e2e0ba1eb669cfcf9314ac}%

%{Phalcon_Cache_Backend_Mongo_f8bbbf10eb7b23d1ad6555244df95daa}%

%{Phalcon_Cache_Backend_Mongo_ee84c5ddac90bc82f87d82edd83f803d}%

%{Phalcon_Cache_Backend_Mongo_20b1a896cfa41a581678813a40ad294b}%

%{Phalcon_Cache_Backend_Mongo_a45d0bbd7f1d2ca9be308e066116571d}%

%{Phalcon_Cache_Backend_Mongo_f4c67e395756ed811b3d86ccd37680d7}%

%{Phalcon_Cache_Backend_Mongo_0a7c1cf9fc6cf16b0c6d365b453b8c9c}%

%{Phalcon_Cache_Backend_Mongo_6e3b53a9e2f02ef15c6e622b43ad7fd6}%

%{Phalcon_Cache_Backend_Mongo_2e2e8a1edb8ee6d249c3408ed9a13506}%

%{Phalcon_Cache_Backend_Mongo_f616a41f5f598297276e1c7ec3a0c216}%

%{Phalcon_Cache_Backend_Mongo_132cc48b9d06f81662724fafbcf5388d}%

%{Phalcon_Cache_Backend_Mongo_25608a5b33db734ab297a041c17d0afe}%

%{Phalcon_Cache_Backend_Mongo_4f309f2cbbf78c9cc272007b4b90e759}%

%{Phalcon_Cache_Backend_Mongo_d95154d7678637473d5bd3651a6b7981}%

%{Phalcon_Cache_Backend_Mongo_63ef827e599ff5e9ce28cac18e0a74de}%

%{Phalcon_Cache_Backend_Mongo_8f54d16975ae8256cc1f314641e21364}%

%{Phalcon_Cache_Backend_Mongo_7b222737002802a81ee0a69bdb497f3a}%

