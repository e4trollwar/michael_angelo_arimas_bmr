%{Phalcon_Annotations_Adapter_Files_27f0e1de35670142694b9cb274b6e938}%
==============================================

%{Phalcon_Annotations_Adapter_Files_cd6c5ebbc3e0172ff63870851834495c|:doc:`Phalcon\\Annotations\\Adapter <Phalcon_Annotations_Adapter>`}%

%{Phalcon_Annotations_Adapter_Files_047eb41a6efa152e801aeae48af56fe2|:doc:`Phalcon\\Annotations\\AdapterInterface <Phalcon_Annotations_AdapterInterface>`}%

%{Phalcon_Annotations_Adapter_Files_7e0a9ef64f61ee5ce7fbbb0abd535ee0}%

.. code-block:: php

    <?php

     $annotations = new \Phalcon\Annotations\Adapter\Files(array(
        'annotationsDir' => 'app/cache/annotations/'
     ));




%{Phalcon_Annotations_Adapter_Files_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Annotations_Adapter_Files_38d168e0be68a8fb5da8d35b168503c0}%

%{Phalcon_Annotations_Adapter_Files_79888eed013f28000084abd3c94b818c}%

%{Phalcon_Annotations_Adapter_Files_e6556816f85cfc43d8fffa4aefc5b1c0|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_Files_6701a44fe1d78a2870965fbc61cf5b63}%

%{Phalcon_Annotations_Adapter_Files_2ee5e1ea1d2956533a54331b54dc68ff|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_Files_5164f141d02e3153f9eed7021745e709}%

%{Phalcon_Annotations_Adapter_Files_297969eeaa21aa7ebba0f0afb61dcbdf|:doc:`Phalcon\\Annotations\\ReaderInterface <Phalcon_Annotations_ReaderInterface>`}%

%{Phalcon_Annotations_Adapter_Files_2cb8a3ee7c6f6afa3aa1a49bbfb847e1}%

%{Phalcon_Annotations_Adapter_Files_99ad5a5b78bf5c861da567ee27e8d30a|:doc:`Phalcon\\Annotations\\ReaderInterface <Phalcon_Annotations_ReaderInterface>`}%

%{Phalcon_Annotations_Adapter_Files_a011f5eee693a258028f7f96c8b6beab}%

%{Phalcon_Annotations_Adapter_Files_a19692781e951687877c08b25cb64443|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_Files_14ce370b71a2b1e2abb49f73069cec7c}%

%{Phalcon_Annotations_Adapter_Files_977f62c281e3c99bf47d7eda3082ad40}%

%{Phalcon_Annotations_Adapter_Files_7f1166fe992a40497f8b1dac1d328c75}%

%{Phalcon_Annotations_Adapter_Files_cc9bd2c4a6d50d14d78e30a43b1dcabb|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Adapter_Files_161a3a4b34c43cf6d494ef7ab77aeda4}%

%{Phalcon_Annotations_Adapter_Files_c6a9f596bf371bd1c57c24f996d40fdc}%

%{Phalcon_Annotations_Adapter_Files_7f1166fe992a40497f8b1dac1d328c75}%

%{Phalcon_Annotations_Adapter_Files_1421db1c2872dc573e7ad7f0ca9fe948|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Adapter_Files_7e10db38670478d87b35d418bbd07401}%

