%{Phalcon_Http_Request_f937ac13d195953377958e619e905ae0}%
================================

%{Phalcon_Http_Request_a5aaa8a3a3b76e71d1886014c28b45ce|:doc:`Phalcon\\Http\\RequestInterface <Phalcon_Http_RequestInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Http_Request_0f640312ac9355954e103b6c372d8498}%

.. code-block:: php

    <?php

    $request = new Phalcon\Http\Request();
    if ($request->isPost() == true) {
    	if ($request->isAjax() == true) {
    		echo 'Request was made using POST and AJAX';
    	}
    }




%{Phalcon_Http_Request_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Http_Request_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Request_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Http_Request_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Request_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Http_Request_f8894e6b9c3fe557c8286fae0575f7f7}%

%{Phalcon_Http_Request_ba7b9529482225cbc63d9c62e24ed000}%

.. code-block:: php

    <?php

    //{%Phalcon_Http_Request_497fcb6f028ef166336f9fb608658805%}
    $userEmail = $request->get("user_email");
    
    //{%Phalcon_Http_Request_3a2431986494cee5548e24d1c81692ae%}
    $userEmail = $request->get("user_email", "email");





%{Phalcon_Http_Request_14e1a9fc9c1d2f72ac8842b9b4d9b944}%

%{Phalcon_Http_Request_e87b383e83c8ff00d1f211dea6f1ab9b}%

.. code-block:: php

    <?php

    //{%Phalcon_Http_Request_a97ccf567216e3dfcbe93bcac27bf9c1%}
    $userEmail = $request->getPost("user_email");
    
    //{%Phalcon_Http_Request_4f1cdba88b12b7ba4530b13d3e042840%}
    $userEmail = $request->getPost("user_email", "email");





%{Phalcon_Http_Request_286ee2375411f96d9707cb7ba3a0348b}%

%{Phalcon_Http_Request_c2842ccf401e480a57a92b576b5a6fcd}%

.. code-block:: php

    <?php

    $userEmail = $request->getPut("user_email");
    
    $userEmail = $request->getPut("user_email", "email");





%{Phalcon_Http_Request_f33cb287d11fc2edccfa67471ad7bcce}%

%{Phalcon_Http_Request_228b69efd1d62fb859788e505b10cb76}%

.. code-block:: php

    <?php

    //{%Phalcon_Http_Request_dbe4a7339cfb6afe9e0bda7564ce8532%}
    $id = $request->getQuery("id");
    
    //{%Phalcon_Http_Request_b306a0262aa105781f08b0231c5a97b7%}
    $id = $request->getQuery("id", "int");
    
    //{%Phalcon_Http_Request_28e62c55bb28c16bc984754be31e3f62%}
    $id = $request->getQuery("id", null, 150);





%{Phalcon_Http_Request_899590b6a23e6f64230f68c44f4ae2f4}%

%{Phalcon_Http_Request_a39ccdc1a7b5890b4cf116209e51ab3c}%

%{Phalcon_Http_Request_3b3b383f1dfee3a85fb1212e54de5f65}%

%{Phalcon_Http_Request_b86c43e24291bb4229b9a35ae4a5d3bb}%

%{Phalcon_Http_Request_fa0349f37a5c33b753856a5a0c0c3870}%

%{Phalcon_Http_Request_7903dd511c9e2a04ed3823190d1c82be}%

%{Phalcon_Http_Request_5b076375826bf114f65da0ebe819dda7}%

%{Phalcon_Http_Request_48ef4947ef993c2ff71cbda692b1808b}%

%{Phalcon_Http_Request_75c42e1236be2cb61137aed921eee731}%

%{Phalcon_Http_Request_410fe0b2fd01cbf19673c23941723903}%

%{Phalcon_Http_Request_33a996ee71dc88037ebb06bb3ccda991}%

%{Phalcon_Http_Request_d02e1d369aaa94ac63b3329cfef5eb77}%

%{Phalcon_Http_Request_07601cc25855cc214e1798878386806a}%

%{Phalcon_Http_Request_d1a37100bd078873a5645a401edd3b03}%

%{Phalcon_Http_Request_16ad5a8db6604bc35234a56c6a977213}%

%{Phalcon_Http_Request_a5f0aaee0eacf4377a6f534cf22cd8d6}%

%{Phalcon_Http_Request_0000b431abd513c6d8f6404adb7f7a16}%

%{Phalcon_Http_Request_7368f65372b63977a7907afec7ca0206}%

%{Phalcon_Http_Request_4c2779fae8cc94ec066d99bc72ae0f76}%

%{Phalcon_Http_Request_f31908dedb6277285877ae7979fc0890}%

%{Phalcon_Http_Request_5fa1f98e169b86de3adf84b6c67b9df2}%

%{Phalcon_Http_Request_6d73fe62a6c132e18e62df733a86e367}%

%{Phalcon_Http_Request_31223a55acd2cea523547f90b83fc27d}%

%{Phalcon_Http_Request_d663f72c3f56753b4cd2d8a072d9c232}%

%{Phalcon_Http_Request_975ba4296bdf134d26baae700d0a8c59}%

%{Phalcon_Http_Request_d9b299da35d54cb31a44437a6d52a7c7}%

%{Phalcon_Http_Request_728a12e49aa29ef26d9173e20535eb97}%

%{Phalcon_Http_Request_c7224adc6c1aa86eefb53a8b96977e23}%

%{Phalcon_Http_Request_8bd164a7fd3dc411cee1762c2e851253}%

%{Phalcon_Http_Request_8961dc90050058e22fdabf66df637849}%

%{Phalcon_Http_Request_ebf4a6841cff2610e42517afd8895b03}%

%{Phalcon_Http_Request_0bad561098b1245fb237de2fec7fef20}%

%{Phalcon_Http_Request_c940e334261e91da55ad7f52e9b502bf}%

%{Phalcon_Http_Request_489e4d021ddd6e882d3bf789ff9f8e7a}%

%{Phalcon_Http_Request_da846227c06cccb369e3dacbf60c4c70}%

%{Phalcon_Http_Request_4a23534795f9c61deb2dcce02938044e}%

%{Phalcon_Http_Request_ad078009519dd8488d1f75e82d6cec69}%

%{Phalcon_Http_Request_d04632a9b1aaa1910bfb5683ce38c4f1}%

%{Phalcon_Http_Request_ec1bc5c5bd126dadee219a5cf65b2a1a}%

%{Phalcon_Http_Request_ef16e1dbfc05319f31727142660ffac7}%

%{Phalcon_Http_Request_c9f8ae278c5a31b6d933999f05f4535e}%

%{Phalcon_Http_Request_f1537f41bea8119b70aebdc963cddfbc}%

%{Phalcon_Http_Request_d28dfed7711c27e9e438d951cae0f308}%

%{Phalcon_Http_Request_de003d30ed696d5caf3c9e05130ae205}%

%{Phalcon_Http_Request_c33e7083fa8f5bd4d47744f22ad446a5}%

%{Phalcon_Http_Request_196d1f200fa007e6070566e7f709f0f4}%

%{Phalcon_Http_Request_a23209b7bdc16e1a17d852ce86599901}%

%{Phalcon_Http_Request_aa4106f52089bcbc532e58a63837c655}%

%{Phalcon_Http_Request_7cf23678879f1d7abe6ba366bee1b7d5}%

%{Phalcon_Http_Request_087f91abb3f550a47c9791ad96d9c20c}%

%{Phalcon_Http_Request_111efe1a19f54c56ce75f6d3ebd0161a}%

%{Phalcon_Http_Request_b2fce870072f46ca6c09e906a306dd11}%

%{Phalcon_Http_Request_e0c8eaa469d98552c1cc9660735209b7}%

%{Phalcon_Http_Request_12f3ab35366154ab7db9311aa4d16bac}%

%{Phalcon_Http_Request_bb2474124aebea1389940223c8205f5e}%

%{Phalcon_Http_Request_324436b60a361208b072319f10baeedc}%

%{Phalcon_Http_Request_142d70d44c53a0a3f696673242dd59f9}%

%{Phalcon_Http_Request_fdfcb8eeea6e74ee4d40a14756ded41d}%

%{Phalcon_Http_Request_ac3b79398157a98ab11aa246f0c913da|:doc:`Phalcon\\Http\\Request\\File <Phalcon_Http_Request_File>`}%

%{Phalcon_Http_Request_649cf7a49cf7b7c29eb8e04e14583913}%

%{Phalcon_Http_Request_b060ac18a8aee5c621006da66742fa8b}%

%{Phalcon_Http_Request_6cba0159240c1d52cd3d386b5dec0f22}%

%{Phalcon_Http_Request_aa40ac512d47f4d6f4b5c2c855c663c0}%

%{Phalcon_Http_Request_d62911adb5da9a5834f941b4b631def6}%

%{Phalcon_Http_Request_0681a2638036dbaa8619cd260e20edaf}%

%{Phalcon_Http_Request_0bf93cd04b3b8d40f7701b39e7ead035}%

%{Phalcon_Http_Request_99dea02f1039298855a495811ba677c0}%

%{Phalcon_Http_Request_c936617e77d96c7a9bae7ac7d1e6eac6}%

%{Phalcon_Http_Request_3b4dce91f1a2bef8fb1bb6a28d2370cc}%

%{Phalcon_Http_Request_bdfbd19f07d5d9f9a5645904f0164df5}%

%{Phalcon_Http_Request_06d55d12bdd04db737451f998cf58a73}%

%{Phalcon_Http_Request_b5c0324b3da486a034e18869dc37260d}%

%{Phalcon_Http_Request_e754ebd5e6f9765a36b6604c30f683db}%

%{Phalcon_Http_Request_24ff788727c18f5f37d7cf15d26daeb7}%

%{Phalcon_Http_Request_7a8fc252736c61e06fe5f0b91ad5c707}%

%{Phalcon_Http_Request_7afec13dc93bb0810011f371b2b68bd5}%

%{Phalcon_Http_Request_de22f4df5dea271bec40dd665edc6beb}%

%{Phalcon_Http_Request_433fcee1a143b1c1e98212c196a1e098}%

%{Phalcon_Http_Request_1468107e7424f532e6c2f0bc68d97013}%

%{Phalcon_Http_Request_49164d18e40ba7be244284d8c258db83}%

%{Phalcon_Http_Request_c33ec3979250e8c26d90836c950e6c20}%

%{Phalcon_Http_Request_51189aa1252896dfb479222b65fb374d}%

%{Phalcon_Http_Request_b0fad24401caa32f80d298c3930ee28a}%

%{Phalcon_Http_Request_a5106d0ec803a36fa4042c9b55412b3a}%

