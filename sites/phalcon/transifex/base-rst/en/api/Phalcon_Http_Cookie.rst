%{Phalcon_Http_Cookie_bbc22a99c0dd69d68821befe9ea84419}%
===============================

%{Phalcon_Http_Cookie_0b066e4bf73264afdc3e24f71410b010|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Http_Cookie_c3975bbfa180591e4de561db4e818179}%

%{Phalcon_Http_Cookie_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Http_Cookie_d26e05b36969fea41b1937ad6f99b0f6}%

%{Phalcon_Http_Cookie_4b428119eb32211499e0d35ff419a866}%

%{Phalcon_Http_Cookie_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Cookie_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Http_Cookie_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Http_Cookie_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Http_Cookie_6360a86646f7b9f0cc7aed55b4fb1e91}%

%{Phalcon_Http_Cookie_7c921565e7976f28d855cef5a32e99de}%

%{Phalcon_Http_Cookie_cf6a49b1a04df65efc14eb8089d12ce5}%

%{Phalcon_Http_Cookie_9772e83386feb41491e83d8d7b5b6588}%

%{Phalcon_Http_Cookie_73a2d1d4ff389abc161109c71eb759c9|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_7b3a25d2361bbbb359f9d13840e5efcb}%

%{Phalcon_Http_Cookie_d49ef1fa96bd516a0889d7fad78707db|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_21d6324b93cbb30caf2393ff6333d4ae}%

%{Phalcon_Http_Cookie_d266ae2606b8141afca162f5ceacc81f}%

%{Phalcon_Http_Cookie_f87e1ecb56153c6358923b1bcfd87d6d}%

%{Phalcon_Http_Cookie_d7c71cb8679008f707238987a306de8b|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_2eca4fec951f5d7a9ccd8712787baeb5}%

%{Phalcon_Http_Cookie_86c89b47d439c046fef3fb56cf6624cc}%

%{Phalcon_Http_Cookie_e3e30a5932e0d1b0fc97e7c1166c1142}%

%{Phalcon_Http_Cookie_8a6a4d34a958f30ab1c365d674cbe028|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_51ce3277e17f01b8a06f53dc27115bfc}%

%{Phalcon_Http_Cookie_895d67dd46b4f63b6bf375c4841366b1}%

%{Phalcon_Http_Cookie_b9f5f6d9a8b102f079579317b0afa532}%

%{Phalcon_Http_Cookie_79daa09fa426260980bb9491f4540ebe|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_d528323040a607d96e7304fc7a624349}%

%{Phalcon_Http_Cookie_9417996226352ca1db0042f9b723781e}%

%{Phalcon_Http_Cookie_61232111f6cd1892b541b9fff69d53bb}%

%{Phalcon_Http_Cookie_639b35af904b9ef30458435720c0bc16|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_9641ed45d9a44ebb4f88db6099402fa9}%

%{Phalcon_Http_Cookie_7e57b43b939cb7bcbd55463467e599a2}%

%{Phalcon_Http_Cookie_b20281e59ec9d7891b1ce879e0056d99}%

%{Phalcon_Http_Cookie_987a48884b2e1024a115053b43fbf11f|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_1274c3e668d65a35b523de34b49319d7}%

%{Phalcon_Http_Cookie_347120dffa17f520de1e7967045514fe}%

%{Phalcon_Http_Cookie_c7ea814d4e9b8da441dfaa1ddc360854}%

%{Phalcon_Http_Cookie_9fd94fb8a5bf9e4173af3d9a13fcf39c|:doc:`Phalcon\\Http\\Cookie <Phalcon_Http_Cookie>`}%

%{Phalcon_Http_Cookie_90c2eb86062ee5664d934c9bb7b1f776}%

%{Phalcon_Http_Cookie_e765e95e1e57fd3cd2f7f45039f09820}%

%{Phalcon_Http_Cookie_a4c8bc01dc9fe4b6ab0d06b05bd790ee}%

%{Phalcon_Http_Cookie_fae4cf61de846e7ed73155b29ce1ac4b}%

%{Phalcon_Http_Cookie_7f802ad50d56c8ec5e1cbaa46de9f716}%

