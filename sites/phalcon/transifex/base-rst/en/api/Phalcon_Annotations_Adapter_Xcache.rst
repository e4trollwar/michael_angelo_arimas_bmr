%{Phalcon_Annotations_Adapter_Xcache_af1613c04233e037a765f233b2d34fd0}%
===============================================

%{Phalcon_Annotations_Adapter_Xcache_cd6c5ebbc3e0172ff63870851834495c|:doc:`Phalcon\\Annotations\\Adapter <Phalcon_Annotations_Adapter>`}%

%{Phalcon_Annotations_Adapter_Xcache_047eb41a6efa152e801aeae48af56fe2|:doc:`Phalcon\\Annotations\\AdapterInterface <Phalcon_Annotations_AdapterInterface>`}%

%{Phalcon_Annotations_Adapter_Xcache_cb857cc41d971d02dbb417d4131083b3}%

.. code-block:: php

    <?php

     $annotations = new \Phalcon\Annotations\Adapter\Xcache();




%{Phalcon_Annotations_Adapter_Xcache_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Annotations_Adapter_Xcache_e6556816f85cfc43d8fffa4aefc5b1c0|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_Xcache_c39ca6c7a99d9e276280178600753400}%

%{Phalcon_Annotations_Adapter_Xcache_2ee5e1ea1d2956533a54331b54dc68ff|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_Xcache_346d72cc49cf738be0e94ead958bec7e}%

%{Phalcon_Annotations_Adapter_Xcache_297969eeaa21aa7ebba0f0afb61dcbdf|:doc:`Phalcon\\Annotations\\ReaderInterface <Phalcon_Annotations_ReaderInterface>`}%

%{Phalcon_Annotations_Adapter_Xcache_2cb8a3ee7c6f6afa3aa1a49bbfb847e1}%

%{Phalcon_Annotations_Adapter_Xcache_99ad5a5b78bf5c861da567ee27e8d30a|:doc:`Phalcon\\Annotations\\ReaderInterface <Phalcon_Annotations_ReaderInterface>`}%

%{Phalcon_Annotations_Adapter_Xcache_a011f5eee693a258028f7f96c8b6beab}%

%{Phalcon_Annotations_Adapter_Xcache_a19692781e951687877c08b25cb64443|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_Xcache_14ce370b71a2b1e2abb49f73069cec7c}%

%{Phalcon_Annotations_Adapter_Xcache_977f62c281e3c99bf47d7eda3082ad40}%

%{Phalcon_Annotations_Adapter_Xcache_7f1166fe992a40497f8b1dac1d328c75}%

%{Phalcon_Annotations_Adapter_Xcache_cc9bd2c4a6d50d14d78e30a43b1dcabb|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Adapter_Xcache_161a3a4b34c43cf6d494ef7ab77aeda4}%

%{Phalcon_Annotations_Adapter_Xcache_c6a9f596bf371bd1c57c24f996d40fdc}%

%{Phalcon_Annotations_Adapter_Xcache_7f1166fe992a40497f8b1dac1d328c75}%

%{Phalcon_Annotations_Adapter_Xcache_1421db1c2872dc573e7ad7f0ca9fe948|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Adapter_Xcache_7e10db38670478d87b35d418bbd07401}%

