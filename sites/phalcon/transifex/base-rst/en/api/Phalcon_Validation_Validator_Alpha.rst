%{Phalcon_Validation_Validator_Alpha_a988b4b0c9e7599fb42cb7b4b40620bc}%
===============================================

%{Phalcon_Validation_Validator_Alpha_24302afb0327181552d5097efe21d21a|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_Alpha_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_Alpha_d9eaf55b61cb58d07fe74bd4f9a1fd3a}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\Alpha as AlphaValidator;
    
    $validator->add('username', new AlphaValidator(array(
       'message' => ':field must contain only letters'
    )));




%{Phalcon_Validation_Validator_Alpha_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_Alpha_9a9ac34e57881f672b6419517f5ca372}%

%{Phalcon_Validation_Validator_Alpha_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_Alpha_5009115a2e23762a2cebed7a090308f5}%

%{Phalcon_Validation_Validator_Alpha_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_Alpha_fbdf133d897ae25a2586e5ee3f08ccce}%

%{Phalcon_Validation_Validator_Alpha_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_Alpha_08d6177f98e66f04346768bf61e2e6ca}%

%{Phalcon_Validation_Validator_Alpha_f2c1e7b80644ef8d34a17b0c2a367fff}%

%{Phalcon_Validation_Validator_Alpha_ba312e603be421ffda5e8b7ddba88d42}%

%{Phalcon_Validation_Validator_Alpha_2e7772bfbad31074c960e8f21b33e650}%

