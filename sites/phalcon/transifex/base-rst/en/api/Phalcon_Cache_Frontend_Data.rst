%{Phalcon_Cache_Frontend_Data_e0dda35c7122379f8b03a5a61a536d3b}%
========================================

%{Phalcon_Cache_Frontend_Data_d4caea1dc5a1b9f969431a18993f6133|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Frontend_Data_16ffdc4e8b6ea499ad208febf5a0b3fa}%

.. code-block:: php

    <?php

    // {%Phalcon_Cache_Frontend_Data_6a929840227fcdc8bb3d4b16b53e599e%}
    $frontCache = new Phalcon\Cache\Frontend\Data(array(
    	"lifetime" => 172800
    ));
    
    // {%Phalcon_Cache_Frontend_Data_989062527cdac2872e9cffda4653ecd8%}
    // {%Phalcon_Cache_Frontend_Data_b6981c87706285da49e15242b7d785bf%}
    // {%Phalcon_Cache_Frontend_Data_985becda271eb01dd0940ab4705aa629%}
    $cache = new Phalcon\Cache\Backend\File($frontCache, array(
    	"cacheDir" => "../app/cache/"
    ));
    
    // {%Phalcon_Cache_Frontend_Data_d99623f9040482f1edf8fed520e01ef6%}
    $cacheKey = 'robots_order_id.cache';
    $robots    = $cache->get($cacheKey);
    if ($robots === null) {
    
    	// {%Phalcon_Cache_Frontend_Data_d9ff820b01c02d3c4e136e3aaa28dd27%}
    	// {%Phalcon_Cache_Frontend_Data_4aa8dcff400337e4dd2ef094fb66e362%}
    	$robots = Robots::find(array("order" => "id"));
    
    	// {%Phalcon_Cache_Frontend_Data_f3762eaf6f2e3ac209ccfb08fd036c9c%}
    	$cache->save($cacheKey, $robots);
    }
    
    // {%Phalcon_Cache_Frontend_Data_062c759655f7a03e81a39817083b59bb%}
    foreach ($robots as $robot) {
    	echo $robot->name, "\n";
    }




%{Phalcon_Cache_Frontend_Data_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Frontend_Data_b8c38e5ce31cf6a5e383ba61685de98c}%

%{Phalcon_Cache_Frontend_Data_1ae81dbbfeea219e1d883f5ae0052e6a}%

%{Phalcon_Cache_Frontend_Data_9c95fde9fd5b5cc68dba068eb7ea3bb0}%

%{Phalcon_Cache_Frontend_Data_790ba726a84e8cc7d11c01ae6aeda21b}%

%{Phalcon_Cache_Frontend_Data_af5e066e55f22b07f85f34b6ad7cc84a}%

%{Phalcon_Cache_Frontend_Data_1df518f9eabe9f817151b63df9f51b70}%

%{Phalcon_Cache_Frontend_Data_229317f58ccf31222a22a92e5cee6a1f}%

%{Phalcon_Cache_Frontend_Data_1ff5bfe3b4c822dd286e8ce81aac1b8b}%

%{Phalcon_Cache_Frontend_Data_098b283de06b3d606bc941e79200d4a9}%

%{Phalcon_Cache_Frontend_Data_e3e99c0a1d8cbb1a36ae911dcb475f20}%

%{Phalcon_Cache_Frontend_Data_050f30cd1b1c4a7535f2642b675b5351}%

%{Phalcon_Cache_Frontend_Data_3ff217b55d51515949922d3d8f3004eb}%

%{Phalcon_Cache_Frontend_Data_346147a9407546082e932b7555d6ae42}%

%{Phalcon_Cache_Frontend_Data_6a72b5df70e820ebbbb658e307cfdd82}%

%{Phalcon_Cache_Frontend_Data_bf8d76d841c9e0ff5bc2145390806e3e}%

%{Phalcon_Cache_Frontend_Data_a3a637a6fb529c4b3e05a71568b34d5b}%

