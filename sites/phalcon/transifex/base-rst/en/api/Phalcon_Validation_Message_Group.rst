%{Phalcon_Validation_Message_Group_ae5e640576c4f86e47ac73acadefc7dc}%
=============================================

%{Phalcon_Validation_Message_Group_e5e73595f7da075711d9e6ef59636927}%

%{Phalcon_Validation_Message_Group_73ddb44c9a86db57b841c2332712977b}%

%{Phalcon_Validation_Message_Group_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Message_Group_ba2aea25b413184ed337e69346ec4370}%

%{Phalcon_Validation_Message_Group_655b970ca58863d14a659a5978a684ef}%

%{Phalcon_Validation_Message_Group_bf1d178dd8eb6f3b6ca0171bae06ef03|:doc:`Phalcon\\Validation\\Message <Phalcon_Validation_Message>`}%

%{Phalcon_Validation_Message_Group_aad4f0eac67cea0c6fd5805b63332239}%

.. code-block:: php

    <?php

     print_r($messages[0]);





%{Phalcon_Validation_Message_Group_bfd83a9ada3533bf977ceeb45fe3e443|:doc:`Phalcon\\Validation\\Message <Phalcon_Validation_Message>`}%

%{Phalcon_Validation_Message_Group_a8bce44be354ca47d470b0631b08fccf}%

.. code-block:: php

    <?php

     $messages[0] = new Phalcon\Validation\Message('This is a message');





%{Phalcon_Validation_Message_Group_904fabe4f66a5d8e7cda163acd2c5527}%

%{Phalcon_Validation_Message_Group_3b51f1329f2e0db61d54bf9bad01e1d6}%

.. code-block:: php

    <?php

     var_dump(isset($message['database']));





%{Phalcon_Validation_Message_Group_1f388ff696e740e69ee6ef39ba24a882}%

%{Phalcon_Validation_Message_Group_19c2bffb196c366d57c7c23ed1ba3aa5}%

.. code-block:: php

    <?php

     unset($message['database']);





%{Phalcon_Validation_Message_Group_a7a6ca7f4c5ea935a2f8314e61a700a8|:doc:`Phalcon\\Validation\\Message <Phalcon_Validation_Message>`}%

%{Phalcon_Validation_Message_Group_bb8d4190f10e12b9a599c953f9789419}%

.. code-block:: php

    <?php

     $messages->appendMessage(new Phalcon\Validation\Message('This is a message'));





%{Phalcon_Validation_Message_Group_143dbd6dffda3d7009339759cb8fdc2d}%

%{Phalcon_Validation_Message_Group_93e7c78f0b911df88ab49ad36e2e10bb}%

.. code-block:: php

    <?php

     $messages->appendMessages($messagesArray);





%{Phalcon_Validation_Message_Group_3a69f30231004321c9e2c6c2a97db066}%

%{Phalcon_Validation_Message_Group_89d8d79999480f15ccc72ef5d8392f4b}%

%{Phalcon_Validation_Message_Group_e3d7fb712da63af712a3f3fe13d5d75b}%

%{Phalcon_Validation_Message_Group_9c60738fe2057a00333df6b727da3d81}%

%{Phalcon_Validation_Message_Group_d07d7e709333e5a70beedd81cc47a746}%

%{Phalcon_Validation_Message_Group_42eb5481bc0261b966348270f2fe11ae}%

%{Phalcon_Validation_Message_Group_f0bb634f74bc2a8cff1359eb7f29b084|:doc:`Phalcon\\Validation\\Message <Phalcon_Validation_Message>`}%

%{Phalcon_Validation_Message_Group_66fe2bbfaec6b2b1d41c840a4de7a41f}%

%{Phalcon_Validation_Message_Group_78ee93ae348293a9c99ee9d4078b1577}%

%{Phalcon_Validation_Message_Group_7c67d920c2ea65d2656d11b61ac0d28b}%

%{Phalcon_Validation_Message_Group_2b503cefe62dd539e45818e4fd0f9833}%

%{Phalcon_Validation_Message_Group_f56916981236195b86b3ae34241eee38}%

%{Phalcon_Validation_Message_Group_93689708a609a9f89674723a8c1f18bd}%

%{Phalcon_Validation_Message_Group_d88f58a57a3709325db2b279f9bb6ffa}%

%{Phalcon_Validation_Message_Group_a599191d92c74e1bbf8e47a5719581bd}%

%{Phalcon_Validation_Message_Group_4bc5b66bcc715a97611e48d57afeac8e}%

