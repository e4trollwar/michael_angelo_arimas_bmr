%{Phalcon_Annotations_Collection_ac9d745088c11c4073d9cb4e3fa077d2}%
==========================================

%{Phalcon_Annotations_Collection_9de357e288367337773f7196871a912a}%

%{Phalcon_Annotations_Collection_df1dd7e83e70e0543eb8c9d9f634f661}%

.. code-block:: php

    <?php

     //{%Phalcon_Annotations_Collection_971b6a08003d59dd00e3359908e18a13%}
     foreach ($classAnnotations as $annotation) {
         echo 'Name=', $annotation->getName(), PHP_EOL;
     }
    
     //{%Phalcon_Annotations_Collection_37b37a0106b4e81b0139176ac69b99e1%}
     var_dump($classAnnotations->has('Cacheable'));
    
     //{%Phalcon_Annotations_Collection_ecf891d3f879f155352bee1c6de708df%}
     $annotation = $classAnnotations->get('Cacheable');




%{Phalcon_Annotations_Collection_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Annotations_Collection_d36de06ef843b4f5a7d703936a25cb04}%

%{Phalcon_Annotations_Collection_296f33013f8951c4bfde29408300881d}%

%{Phalcon_Annotations_Collection_e3d7fb712da63af712a3f3fe13d5d75b}%

%{Phalcon_Annotations_Collection_2a32736e67e8e9e130fe797001f522d9}%

%{Phalcon_Annotations_Collection_d07d7e709333e5a70beedd81cc47a746}%

%{Phalcon_Annotations_Collection_42eb5481bc0261b966348270f2fe11ae}%

%{Phalcon_Annotations_Collection_16ba2dac9319451ad04f44f0ef198cdb|:doc:`Phalcon\\Annotations\\Annotation <Phalcon_Annotations_Annotation>`}%

%{Phalcon_Annotations_Collection_a3bff1376e8bf0b6683f60cc38bfa655}%

%{Phalcon_Annotations_Collection_78ee93ae348293a9c99ee9d4078b1577}%

%{Phalcon_Annotations_Collection_7c67d920c2ea65d2656d11b61ac0d28b}%

%{Phalcon_Annotations_Collection_2b503cefe62dd539e45818e4fd0f9833}%

%{Phalcon_Annotations_Collection_f56916981236195b86b3ae34241eee38}%

%{Phalcon_Annotations_Collection_93689708a609a9f89674723a8c1f18bd}%

%{Phalcon_Annotations_Collection_f54954ad9cbb71b1f6fa134eb86eecc8}%

%{Phalcon_Annotations_Collection_f8ade5655d696f796fe1caea4e98d868|:doc:`Phalcon\\Annotations\\Annotation <Phalcon_Annotations_Annotation>`}%

%{Phalcon_Annotations_Collection_33a002122660f12f8faa7a18bcf5bf44}%

%{Phalcon_Annotations_Collection_36d7b8c4a04e97b80b50c1b4581f1dda|:doc:`Phalcon\\Annotations\\Annotation <Phalcon_Annotations_Annotation>`}%

%{Phalcon_Annotations_Collection_aae40631126ae1071a0a78bf5ee48948}%

%{Phalcon_Annotations_Collection_f4288555be3ab712643341850fbd439e|:doc:`Phalcon\\Annotations\\Annotation <Phalcon_Annotations_Annotation>`}%

%{Phalcon_Annotations_Collection_a3f131e48c9dd680c816d8aebfc0fe00}%

%{Phalcon_Annotations_Collection_3b3b383f1dfee3a85fb1212e54de5f65}%

%{Phalcon_Annotations_Collection_b7abd12a647ec6ff0e67f9007cbd370e}%

