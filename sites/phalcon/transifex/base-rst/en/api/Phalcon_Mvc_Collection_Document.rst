%{Phalcon_Mvc_Collection_Document_fe7d8c24ff0cb03a6aeebcc10d78a5dc}%
============================================

%{Phalcon_Mvc_Collection_Document_99734ab701335d0a422216c8b40e5011}%

%{Phalcon_Mvc_Collection_Document_76ec870812962804c71927085b187d64}%

%{Phalcon_Mvc_Collection_Document_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Collection_Document_590d12d5a0926efcec1e445fe749f942}%

%{Phalcon_Mvc_Collection_Document_2b95914c255b9350083158e0a40489a7}%

%{Phalcon_Mvc_Collection_Document_f902e44a5bedbda3f00e2f37187f0a78}%

%{Phalcon_Mvc_Collection_Document_f6c8df8004569ab0d304bc9bebe355cb}%

%{Phalcon_Mvc_Collection_Document_c95065724b6907e8c4f032b759ee3929|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Collection_Document_801c038dc2cfec599e2ab12aee4426f2}%

%{Phalcon_Mvc_Collection_Document_107cf4e7bef4da3d40f98c4247efb8fd}%

%{Phalcon_Mvc_Collection_Document_5bac82925e087de7fc582dfc56ef7b5e}%

%{Phalcon_Mvc_Collection_Document_7b793f1f8fc0c0c603c6f68d21385cde}%

%{Phalcon_Mvc_Collection_Document_541ca63c8f0e9cdb167f3c5dff81b126}%

.. code-block:: php

    <?php

    echo $robot->readAttribute('name');





%{Phalcon_Mvc_Collection_Document_5edaecf6057a10ae28a47003da1f9669}%

%{Phalcon_Mvc_Collection_Document_7c2a8da2a9aeb392f8b2784da24c73a8}%

