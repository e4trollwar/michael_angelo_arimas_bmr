%{Phalcon_Acl_Adapter_e958133e7dc49947fece8a480fa73a4b}%
========================================

%{Phalcon_Acl_Adapter_c1f3db89d0c708fcfabb86f7fbf229f1|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\Acl\\AdapterInterface <Phalcon_Acl_AdapterInterface>`}%

%{Phalcon_Acl_Adapter_9d04f2eaf22351d508a44b1f0bfd6079}%

%{Phalcon_Acl_Adapter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Acl_Adapter_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Acl_Adapter_8d848637cfda8b4cc05eedcec9549316}%

%{Phalcon_Acl_Adapter_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Acl_Adapter_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Acl_Adapter_b43ff6faa1e6c7b38c1ab05dd844439c}%

%{Phalcon_Acl_Adapter_b10c9be1e424bd19d57e310368637b0f}%

%{Phalcon_Acl_Adapter_037b8de4282a18e1503458ea1456859d}%

%{Phalcon_Acl_Adapter_3e1e4baf364140e8f8548b42b0ccbb7d}%

%{Phalcon_Acl_Adapter_8f32f359411225e3d8f5d27cd6a3eb97}%

%{Phalcon_Acl_Adapter_e3d71cc3797785953c7630fb0d188efe}%

%{Phalcon_Acl_Adapter_c85364849ce592562a31407353fe915e}%

%{Phalcon_Acl_Adapter_06239d22f4081c55534c6b4f598d4df0}%

%{Phalcon_Acl_Adapter_689ddf7a4c7384d101ffc57559faf535}%

%{Phalcon_Acl_Adapter_33e7b3b2869fa8692dd3a4ad610d8696}%

%{Phalcon_Acl_Adapter_d60b9e6fa8bdbd137daab74798a27319|:doc:`Phalcon\\Acl\\RoleInterface <Phalcon_Acl_RoleInterface>`}%

%{Phalcon_Acl_Adapter_4a67175c64759a17e2065c78d1728c61}%

%{Phalcon_Acl_Adapter_3aa048ce50114687831b9e3d951ad5c6}%

%{Phalcon_Acl_Adapter_c29c990c8502ba975c3265e71fd86ca3}%

%{Phalcon_Acl_Adapter_953de0e5a2833efbd6525e47d28b609f}%

%{Phalcon_Acl_Adapter_a9e16e00a6b68bc5fd20a46b8393b8bc}%

%{Phalcon_Acl_Adapter_ae75bb828dc55d8c726e921d97e7720f}%

%{Phalcon_Acl_Adapter_aa389d4e5f59cbc4e9ac929319d0ea22}%

%{Phalcon_Acl_Adapter_f523bf45357d8cd9b1fbdb620d13915f|:doc:`Phalcon\\Acl\\ResourceInterface <Phalcon_Acl_ResourceInterface>`}%

%{Phalcon_Acl_Adapter_7dd8a31afa5b62b09d92e81be85852ab}%

%{Phalcon_Acl_Adapter_70bbac12aee670b624471af41260fec8}%

%{Phalcon_Acl_Adapter_a77ccd42a2cc058e5711c6d7e0b03c7a}%

%{Phalcon_Acl_Adapter_b65789f8ba7c8a2fe7c5b811cb131402}%

%{Phalcon_Acl_Adapter_408af9580ebe974df581764a93778bb1}%

%{Phalcon_Acl_Adapter_8cb1d2079d2b79fb8359f4a6eec75c53}%

%{Phalcon_Acl_Adapter_df11d319ce7bdbc9bfffdb1cc4d12c8f}%

%{Phalcon_Acl_Adapter_5257e98986448c522ea03129179c1101}%

%{Phalcon_Acl_Adapter_f7f117791f274893c34b6025ea6cc218}%

%{Phalcon_Acl_Adapter_46fa2396d48cd8652712a7e77c0744da}%

%{Phalcon_Acl_Adapter_2d394e372730bf53e295fd999d73e799}%

%{Phalcon_Acl_Adapter_b28ff6078d3390ffae4542c5964ef6b2|:doc:`Phalcon\\Acl\\RoleInterface <Phalcon_Acl_RoleInterface>`}%

%{Phalcon_Acl_Adapter_087310f87165b8132cc3099dd854ae04}%

%{Phalcon_Acl_Adapter_9dd4e4f6759248161420c8772e9cb18e|:doc:`Phalcon\\Acl\\ResourceInterface <Phalcon_Acl_ResourceInterface>`}%

%{Phalcon_Acl_Adapter_9d5616cc0d5f7b2a3ce535de445a349a}%

