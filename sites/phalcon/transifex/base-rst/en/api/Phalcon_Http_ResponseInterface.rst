%{Phalcon_Http_ResponseInterface_16a77c2024d3d4c466e5f8259858f1c3}%
==============================================

%{Phalcon_Http_ResponseInterface_fb3495f85e91ff0ee76afc2a03c08fc5}%

%{Phalcon_Http_ResponseInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Http_ResponseInterface_abcb46da2152f43052d7edd9a039819a|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_f5bf57ed594342d3389f0f90c26903ae}%

%{Phalcon_Http_ResponseInterface_81ced6fcdc6032d3bf6fb8307234e2a4|:doc:`Phalcon\\Http\\Response\\Headers <Phalcon_Http_Response_Headers>`}%

%{Phalcon_Http_ResponseInterface_f237a27f2a14bf894e12d1e11fe3be47}%

%{Phalcon_Http_ResponseInterface_e208c37830057960d1c134d55ad1dce8|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_fe42aa6024866ceb98b4ed350a697d4d}%

%{Phalcon_Http_ResponseInterface_668444a6462104a21637d1315439d138|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_5349be5a5061599fe9c330f600dcea86}%

%{Phalcon_Http_ResponseInterface_0e6720d2c8d129242051fb167f888f66|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_72ea85a5beb6771e50aef50ff418ebf2}%

%{Phalcon_Http_ResponseInterface_dc7fe052e443720676ae12fa8f0f5e66|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_3913ca162fd4155456ca49afef29293e}%

%{Phalcon_Http_ResponseInterface_763b5bac88a1d0dbafd2f2d95739c6d0|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_b32022b0ac7fb7906174677f0bf12157}%

%{Phalcon_Http_ResponseInterface_c8f1b9a54ad2cad7bd2588666b5a94e9|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_a8c06963d1170b1e44bd26f01b1a03e0}%

%{Phalcon_Http_ResponseInterface_ea6f04c1e0d328430687aed2c39f7b58|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_a7155b91ecaeb3abab02a432b133fd03}%

%{Phalcon_Http_ResponseInterface_9758d8dfcba2de02dca00b51c463dd55|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_0789455ff1dddcd9838e20d9e419eafc}%

%{Phalcon_Http_ResponseInterface_6ee5a1733dbd726606d70bc958e9c5df|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_32f66805172ec27aca9184ce2ce505a7}%

.. code-block:: php

    <?php

    $response->setJsonContent(array("status" => "OK"));





%{Phalcon_Http_ResponseInterface_d5a2dd35739f7d5a1b86e1685751e9ee|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_f3d56b7967c79a14a998c125af31df4b}%

%{Phalcon_Http_ResponseInterface_d8df69a00f53915151ce2cfd4b6489bc}%

%{Phalcon_Http_ResponseInterface_747a092609cbec75f48b5fef50713c3b}%

%{Phalcon_Http_ResponseInterface_a900958916d3d13a02b74b6bd135aa47|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_444b1912c07d9862a0b096787d55dd15}%

%{Phalcon_Http_ResponseInterface_d80e69c5769c260102669a16bc4d121a|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_3c08474bb26c9adc1688023efef41bb0}%

%{Phalcon_Http_ResponseInterface_70918dd0a912a4eaf14916bab2cae4ff|:doc:`Phalcon\\Http\\ResponseInterface <Phalcon_Http_ResponseInterface>`}%

%{Phalcon_Http_ResponseInterface_2367c18633dbaf3106d8e843a569fbe1}%

%{Phalcon_Http_ResponseInterface_b9dacf7069b056fd0e606acdcbad8785}%

%{Phalcon_Http_ResponseInterface_dd29378305e2f0a39ddbcaac63aec8b8}%

