%{Phalcon_Acl_AdapterInterface_298798b1992c25df0fc52b46c1209830}%
============================================

%{Phalcon_Acl_AdapterInterface_405a4e375fbc55942c9da70d406a67f9}%

%{Phalcon_Acl_AdapterInterface_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Acl_AdapterInterface_b48d86406dde79c7518168d8663e944b}%

%{Phalcon_Acl_AdapterInterface_b10c9be1e424bd19d57e310368637b0f}%

%{Phalcon_Acl_AdapterInterface_3f4632bff83c4725a09898b2f79aacff}%

%{Phalcon_Acl_AdapterInterface_3e1e4baf364140e8f8548b42b0ccbb7d}%

%{Phalcon_Acl_AdapterInterface_1464b8f336f19df7eb686a2d5ebbcc18|:doc:`Phalcon\\Acl\\RoleInterface <Phalcon_Acl_RoleInterface>`}%

%{Phalcon_Acl_AdapterInterface_4a67175c64759a17e2065c78d1728c61}%

%{Phalcon_Acl_AdapterInterface_ab7726e95a1fa30c74dc8e17e8ec4dad}%

%{Phalcon_Acl_AdapterInterface_c29c990c8502ba975c3265e71fd86ca3}%

%{Phalcon_Acl_AdapterInterface_de3d3489e390affd05227c8167bdde22}%

%{Phalcon_Acl_AdapterInterface_a9e16e00a6b68bc5fd20a46b8393b8bc}%

%{Phalcon_Acl_AdapterInterface_9c706b7b95e8411c38b0177a8a8fc3c2}%

%{Phalcon_Acl_AdapterInterface_aa389d4e5f59cbc4e9ac929319d0ea22}%

%{Phalcon_Acl_AdapterInterface_5fe0a9d5ac628a5bad9b1c646dd2244a|:doc:`Phalcon\\Acl\\ResourceInterface <Phalcon_Acl_ResourceInterface>`}%

%{Phalcon_Acl_AdapterInterface_7dd8a31afa5b62b09d92e81be85852ab}%

%{Phalcon_Acl_AdapterInterface_4f7e0a4bc28faf77b95da6e9aa5ea062}%

%{Phalcon_Acl_AdapterInterface_a77ccd42a2cc058e5711c6d7e0b03c7a}%

%{Phalcon_Acl_AdapterInterface_b9b214385122579dad75a63fe7279357}%

%{Phalcon_Acl_AdapterInterface_408af9580ebe974df581764a93778bb1}%

%{Phalcon_Acl_AdapterInterface_b97e30f0801ce8e4633b5037d6cc8c07}%

%{Phalcon_Acl_AdapterInterface_df11d319ce7bdbc9bfffdb1cc4d12c8f}%

%{Phalcon_Acl_AdapterInterface_0b872d7cf0e3bcf74dca35a32d3c55a5}%

%{Phalcon_Acl_AdapterInterface_f7f117791f274893c34b6025ea6cc218}%

%{Phalcon_Acl_AdapterInterface_5f3cd9df1955cfb635d6a937c98de6d5}%

%{Phalcon_Acl_AdapterInterface_2d394e372730bf53e295fd999d73e799}%

%{Phalcon_Acl_AdapterInterface_18eebb9f350c04f8ff6ec422a7602656}%

%{Phalcon_Acl_AdapterInterface_e3d71cc3797785953c7630fb0d188efe}%

%{Phalcon_Acl_AdapterInterface_9abbd69262decc75c3518e8cf8e752f3}%

%{Phalcon_Acl_AdapterInterface_06239d22f4081c55534c6b4f598d4df0}%

%{Phalcon_Acl_AdapterInterface_f952b74d5075b1301c78de533bfefe56}%

%{Phalcon_Acl_AdapterInterface_33e7b3b2869fa8692dd3a4ad610d8696}%

%{Phalcon_Acl_AdapterInterface_e5cf2a613724ef2cc498c2636c9fa533|:doc:`Phalcon\\Acl\\RoleInterface <Phalcon_Acl_RoleInterface>`}%

%{Phalcon_Acl_AdapterInterface_087310f87165b8132cc3099dd854ae04}%

%{Phalcon_Acl_AdapterInterface_acaa7480a128dce97f78b53481e0e8ce|:doc:`Phalcon\\Acl\\ResourceInterface <Phalcon_Acl_ResourceInterface>`}%

%{Phalcon_Acl_AdapterInterface_9d5616cc0d5f7b2a3ce535de445a349a}%

