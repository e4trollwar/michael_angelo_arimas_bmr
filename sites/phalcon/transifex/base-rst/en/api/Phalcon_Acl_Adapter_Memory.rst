%{Phalcon_Acl_Adapter_Memory_a71e8ca12785a795dd27c37b5e8a66ad}%
=======================================

%{Phalcon_Acl_Adapter_Memory_b8b683d6ad63989562b329932d744583|:doc:`Phalcon\\Acl\\Adapter <Phalcon_Acl_Adapter>`}%

%{Phalcon_Acl_Adapter_Memory_447a90027feed06e079868f604f02d19|:doc:`Phalcon\\Acl\\AdapterInterface <Phalcon_Acl_AdapterInterface>`|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`}%

%{Phalcon_Acl_Adapter_Memory_2aaa584f84c3e683f0f8ee35c0cfbb2b}%

.. code-block:: php

    <?php

    $acl = new Phalcon\Acl\Adapter\Memory();
    
    $acl->setDefaultAction(Phalcon\Acl::DENY);
    
    //{%Phalcon_Acl_Adapter_Memory_c0da7a1d3435e953913e49f74bcd24f9%}
    $roles = array(
    	'users' => new Phalcon\Acl\Role('Users'),
    	'guests' => new Phalcon\Acl\Role('Guests')
    );
    foreach ($roles as $role) {
    	$acl->addRole($role);
    }
    
    //{%Phalcon_Acl_Adapter_Memory_78783a7f5c7828b5905a474ba6fd8b9e%}
      $privateResources = array(
    	'companies' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete'),
    	'products' => array('index', 'search', 'new', 'edit', 'save', 'create', 'delete'),
    	'invoices' => array('index', 'profile')
    );
    foreach ($privateResources as $resource => $actions) {
    	$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
    }
    
    //{%Phalcon_Acl_Adapter_Memory_206242695c593c3b808cf7a04bdcc01a%}
    $publicResources = array(
    	'index' => array('index'),
    	'about' => array('index'),
    	'session' => array('index', 'register', 'start', 'end'),
    	'contact' => array('index', 'send')
    );
      foreach ($publicResources as $resource => $actions) {
    	$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
    }
    
      //{%Phalcon_Acl_Adapter_Memory_d7e8f62a25954bc85f63c86ed94f1be4%}
    foreach ($roles as $role){
    	foreach ($publicResources as $resource => $actions) {
    		$acl->allow($role->getName(), $resource, '*');
    	}
    }
    
    //{%Phalcon_Acl_Adapter_Memory_dec4d7effeb4ce5af9bfd88c431480c5%}
      foreach ($privateResources as $resource => $actions) {
     		foreach ($actions as $action) {
    		$acl->allow('Users', $resource, $action);
    	}
    }




%{Phalcon_Acl_Adapter_Memory_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Acl_Adapter_Memory_a5d0ee958da5dcfe392fa79afb9d9a90}%

%{Phalcon_Acl_Adapter_Memory_9be65047921b64e8d2927cbf1e42545e}%

%{Phalcon_Acl_Adapter_Memory_3490af7907c7f437a9ec6b432bbd254d|:doc:`Phalcon\\Acl\\RoleInterface <Phalcon_Acl_RoleInterface>`}%

%{Phalcon_Acl_Adapter_Memory_0ee5b2dfc3817b7e0aacb15f12ff52d0}%

.. code-block:: php

    <?php

     	$acl->addRole(new Phalcon\Acl\Role('administrator'), 'consultant');
     	$acl->addRole('administrator', 'consultant');





%{Phalcon_Acl_Adapter_Memory_46d37b1b44e2cb938ece7735e6450c92}%

%{Phalcon_Acl_Adapter_Memory_c29c990c8502ba975c3265e71fd86ca3}%

%{Phalcon_Acl_Adapter_Memory_199001e43756c3b859ac0a49724c2f74}%

%{Phalcon_Acl_Adapter_Memory_a9e16e00a6b68bc5fd20a46b8393b8bc}%

%{Phalcon_Acl_Adapter_Memory_e97d319459a283ec992750afe1f79af3}%

%{Phalcon_Acl_Adapter_Memory_aa389d4e5f59cbc4e9ac929319d0ea22}%

%{Phalcon_Acl_Adapter_Memory_f5b4b1079ed2429483f61b226bd4f536|:doc:`Phalcon\\Acl\\Resource <Phalcon_Acl_Resource>`}%

%{Phalcon_Acl_Adapter_Memory_17d330425d11b02a6a8cf6aaf21f8da8}%

.. code-block:: php

    <?php

     //{%Phalcon_Acl_Adapter_Memory_c67fd5426d6501a47d6e71305a80f77c%}
     $acl->addResource(new Phalcon\Acl\Resource('customers'), 'search');
     $acl->addResource('customers', 'search');
    
     //{%Phalcon_Acl_Adapter_Memory_3366dfb6895d000cb412ce4f99ebcf4f%}
     $acl->addResource(new Phalcon\Acl\Resource('customers'), array('create', 'search'));
     $acl->addResource('customers', array('create', 'search'));





%{Phalcon_Acl_Adapter_Memory_07b4cc18390eed57216ee33436c35478}%

%{Phalcon_Acl_Adapter_Memory_a77ccd42a2cc058e5711c6d7e0b03c7a}%

%{Phalcon_Acl_Adapter_Memory_9008b3e8125ed525790928724dec9923}%

%{Phalcon_Acl_Adapter_Memory_408af9580ebe974df581764a93778bb1}%

%{Phalcon_Acl_Adapter_Memory_66054b086279a917554adf98fb983df0}%

%{Phalcon_Acl_Adapter_Memory_7ebc1e96ef3130c6ae335190c81c85d6}%

%{Phalcon_Acl_Adapter_Memory_1894db53061fd27fd97b2ca85b23bc28}%

%{Phalcon_Acl_Adapter_Memory_150ef58fd6347afd3944712e8fe9b1c8}%

.. code-block:: php

    <?php

     //{%Phalcon_Acl_Adapter_Memory_d37da820334ae27c58eeaf1e996f096a%}
     $acl->allow('guests', 'customers', 'search');
    
     //{%Phalcon_Acl_Adapter_Memory_dbaf6d60bd637674cb815ed07af3c379%}
     $acl->allow('guests', 'customers', array('search', 'create'));
    
     //{%Phalcon_Acl_Adapter_Memory_419ad5cbd2af5160dd3547532c9b2948%}
     $acl->allow('*', 'products', 'browse');
    
     //{%Phalcon_Acl_Adapter_Memory_5d3fb7ab03668a1f491db2836896b10b%}
     $acl->allow('*', '*', 'browse');





%{Phalcon_Acl_Adapter_Memory_f24b986821c60c97d4144331e83c7978}%

%{Phalcon_Acl_Adapter_Memory_81e3786e6c95093b51608022354d1f38}%

.. code-block:: php

    <?php

     //{%Phalcon_Acl_Adapter_Memory_cc23d5ee8a30158bd2baa506ec45178e%}
     $acl->deny('guests', 'customers', 'search');
    
     //{%Phalcon_Acl_Adapter_Memory_c4575ae2230e93ce95b00b19580f66b8%}
     $acl->deny('guests', 'customers', array('search', 'create'));
    
     //{%Phalcon_Acl_Adapter_Memory_304af99c926d7fe85121ec9f5fcd8630%}
     $acl->deny('*', 'products', 'browse');
    
     //{%Phalcon_Acl_Adapter_Memory_fc4b20bbea6fd20082027715f417f293%}
     $acl->deny('*', '*', 'browse');





%{Phalcon_Acl_Adapter_Memory_047f16115006016117a0ea7d8153457b}%

%{Phalcon_Acl_Adapter_Memory_bbf2b5a922a0669ea7e1a26d47c55d4c}%

.. code-block:: php

    <?php

     //{%Phalcon_Acl_Adapter_Memory_510ef20eb028af44d40ea2b9677f97aa%}
     $acl->isAllowed('andres', 'Products', 'create');
    
     //{%Phalcon_Acl_Adapter_Memory_f58ebd430b054ac3c1df84c8a6ad0907%}
     $acl->isAllowed('guests', '*', 'edit');





%{Phalcon_Acl_Adapter_Memory_9e41d3b4cf357ce2f4d5990351f64a5f|:doc:`Phalcon\\Acl\\Role <Phalcon_Acl_Role>`}%

%{Phalcon_Acl_Adapter_Memory_087310f87165b8132cc3099dd854ae04}%

%{Phalcon_Acl_Adapter_Memory_458da3cf9e5dec0f0e5b95ae4b5fd7f4|:doc:`Phalcon\\Acl\\Resource <Phalcon_Acl_Resource>`}%

%{Phalcon_Acl_Adapter_Memory_9d5616cc0d5f7b2a3ce535de445a349a}%

%{Phalcon_Acl_Adapter_Memory_c30229aa81e4fda10083efe0f66069bb|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Acl_Adapter_Memory_8d848637cfda8b4cc05eedcec9549316}%

%{Phalcon_Acl_Adapter_Memory_263cf36a34d4dcf448f3bcd039951326|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Acl_Adapter_Memory_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Acl_Adapter_Memory_1b9c6706363c7c8a1a95f6bdb13dcb55}%

%{Phalcon_Acl_Adapter_Memory_b10c9be1e424bd19d57e310368637b0f}%

%{Phalcon_Acl_Adapter_Memory_804b37e2838405d2bb407fc3bba41c3c}%

%{Phalcon_Acl_Adapter_Memory_3e1e4baf364140e8f8548b42b0ccbb7d}%

%{Phalcon_Acl_Adapter_Memory_e3e849ff8fe8078586a1ae3d6bf15234}%

%{Phalcon_Acl_Adapter_Memory_e3d71cc3797785953c7630fb0d188efe}%

%{Phalcon_Acl_Adapter_Memory_70fcf390fcc39a314ae7edab12d2d6e3}%

%{Phalcon_Acl_Adapter_Memory_06239d22f4081c55534c6b4f598d4df0}%

%{Phalcon_Acl_Adapter_Memory_20b2073ae9cfb06caefba3c400f808f8}%

%{Phalcon_Acl_Adapter_Memory_33e7b3b2869fa8692dd3a4ad610d8696}%

