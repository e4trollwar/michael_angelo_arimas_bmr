%{Phalcon_Validation_Validator_ExclusionIn_710726a872e115ff67feb5a5275cab3a}%
=====================================================

%{Phalcon_Validation_Validator_ExclusionIn_cac0f4e36b6c7c0d12c2ea9fa8f05795|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_ExclusionIn_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_ExclusionIn_2981a12ec9d0c9003a807dc0e969f458}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\ExclusionIn;
    
    $validator->add('status', new ExclusionIn(array(
       'message' => 'The status must not be A or B',
       'domain' => array('A', 'B')
    )));




%{Phalcon_Validation_Validator_ExclusionIn_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_ExclusionIn_11680e0816c2b5612e05aaf250906b3f|:doc:`Phalcon\\Validation <Phalcon_Validation>`}%

%{Phalcon_Validation_Validator_ExclusionIn_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_ExclusionIn_083f8660fec7a850b97c924c6ec8b3b5}%

%{Phalcon_Validation_Validator_ExclusionIn_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_ExclusionIn_81b7291a5b45df34e31addf4c20fb319}%

%{Phalcon_Validation_Validator_ExclusionIn_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_ExclusionIn_b0f96a5c6a69977cb2d6d47e77611416}%

%{Phalcon_Validation_Validator_ExclusionIn_6685002cddbd3be00d9dc4ed9cea3a75}%

%{Phalcon_Validation_Validator_ExclusionIn_74181c24b3c3cfcd1f72f02a5f4b5ae3}%

%{Phalcon_Validation_Validator_ExclusionIn_2e7772bfbad31074c960e8f21b33e650}%

