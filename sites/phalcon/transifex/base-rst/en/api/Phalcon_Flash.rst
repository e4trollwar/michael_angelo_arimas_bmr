%{Phalcon_Flash_6378c8da3cd5e76745d16660c61b054e}%
=================================

%{Phalcon_Flash_9448cfd4e47a9c128216f6ff4f48819e}%

.. code-block:: php

    <?php

     $flash->success("The record was successfully deleted");
     $flash->error("Cannot open the file");




%{Phalcon_Flash_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Flash_aa844735e95a3790f61673658fb36381}%

%{Phalcon_Flash_3527d7ad93db44794390a7e8cd6350dc}%

%{Phalcon_Flash_2d90d21f52c42f90aa316a9386a39542|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_e612daac8bf7fcddafa12e533e332a89}%

%{Phalcon_Flash_deb825328ec15f515b4d97bb84bc4f02|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_3bb4ebbf217a341db54dc9fc4b49b66e}%

%{Phalcon_Flash_9265190b0d59acb5498f4c252a25ec30|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_c14da91f6f71b7c71f50da51d2f1c14a}%

%{Phalcon_Flash_48264ee8af86f98c7d0e1684ab2c587e}%

%{Phalcon_Flash_508f3a050e254578328bc958df18cb4a}%

.. code-block:: php

    <?php

     $flash->error('This is an error');





%{Phalcon_Flash_58ea1517e678a58f8c260b342c44c2ab}%

%{Phalcon_Flash_f5bb0b1e9d9d0f9558960a1e42afcf04}%

.. code-block:: php

    <?php

     $flash->notice('This is an information');





%{Phalcon_Flash_49c6ec63cad026d6b50d0530088cf048}%

%{Phalcon_Flash_26239272d000aa42450fb5cc48f612ae}%

.. code-block:: php

    <?php

     $flash->success('The process was finished successfully');





%{Phalcon_Flash_54738445a2a888de925abe5863e9b848}%

%{Phalcon_Flash_545dfddf25dafd6fe2ad13a5294c1da4}%

.. code-block:: php

    <?php

     $flash->warning('Hey, this is important');





%{Phalcon_Flash_e8e7ac0760dbb3739abd0a9771e76509}%

%{Phalcon_Flash_bb78ddd310175faeaab1d79dc924b66e}%

