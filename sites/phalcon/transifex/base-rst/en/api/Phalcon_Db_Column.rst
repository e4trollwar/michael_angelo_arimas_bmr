%{Phalcon_Db_Column_a7e1dcc4d0ddeb3fdb3c16c1411cb737}%
=============================

%{Phalcon_Db_Column_10061d097f03ebfe8dadad360b44755f|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Column_ec7eec7493e51dea3893b69a6094fccc}%

.. code-block:: php

    <?php

    use Phalcon\Db\Column as Column;
    
     //{%Phalcon_Db_Column_4ccf0f62c35ce6f4ad47c504191cbc48%}
     $column = new Column("id", array(
       "type" => Column::TYPE_INTEGER,
       "size" => 10,
       "unsigned" => true,
       "notNull" => true,
       "autoIncrement" => true,
       "first" => true
     ));
    
     //{%Phalcon_Db_Column_0e875d7fbf7469b3a2a096defb4a63b9%}
     $connection->addColumn("robots", null, $column);




%{Phalcon_Db_Column_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Db_Column_1082ee13c68085c163c43cde9fb49245}%

%{Phalcon_Db_Column_9346dbab7b679232da8a7b09bbe369bc}%

%{Phalcon_Db_Column_c7c9a81cbe117836cbb1b1b36994ea76}%

%{Phalcon_Db_Column_0ff9447d13aa051ba03935750eef55d8}%

%{Phalcon_Db_Column_ef830a2a9f5456aa9e8c24110b5d0b76}%

%{Phalcon_Db_Column_56e51ba52b34922b2292b0ec4ce4bb30}%

%{Phalcon_Db_Column_60b3624926d2266d30a3241b15e14265}%

%{Phalcon_Db_Column_23a17e313a58cb103ef384c7c4dae91a}%

%{Phalcon_Db_Column_90b6ccfb9da95d21478e5084628b24cf}%

%{Phalcon_Db_Column_c97aeda17f47a4e2a2a75551534de302}%

%{Phalcon_Db_Column_dbbe9642e94bd21ac7ba6ad4deb4a639}%

%{Phalcon_Db_Column_4ee0046c58959b7c4fe3390d9661ad50}%

%{Phalcon_Db_Column_ff61f4bd65c323cd2f618726c98358a2}%

%{Phalcon_Db_Column_4edf30cd423802afc49fd23b737e17c5}%

%{Phalcon_Db_Column_2240c13baeb722176a32326e163f21ba}%

%{Phalcon_Db_Column_64f18cded9d00f70db80c8b86fbad51c}%

%{Phalcon_Db_Column_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Column_73e32e2ba8c583666b2482c813b66812}%

%{Phalcon_Db_Column_8a5279ec4aa3dd2fa14c6330d40e3779}%

%{Phalcon_Db_Column_0874a2d6a938f656aaf55a3f04ff8f91}%

%{Phalcon_Db_Column_7527b7d9a815ae851a6ee16d5e515891}%

%{Phalcon_Db_Column_bad4450b78127e6c6b52db7413174386}%

%{Phalcon_Db_Column_e582ae362cfd4e3366ada23c66fd5ca3}%

%{Phalcon_Db_Column_476f2497c851eae8a78f73032ad317bb}%

%{Phalcon_Db_Column_1106144a1bc4a680ccdea1308a62dd25}%

%{Phalcon_Db_Column_6ff51604371a9e42725d7f9994350974}%

%{Phalcon_Db_Column_6f3421435f68a3b1a248cab77a7e99ca}%

%{Phalcon_Db_Column_8d9b8ff215ca1896147f9617779a1756}%

%{Phalcon_Db_Column_18a6fe5ac48f5b3a556bda2b7b785786}%

%{Phalcon_Db_Column_396b5261cebe6140ad2fb2e00cfee3f4}%

%{Phalcon_Db_Column_d883eb42613693d39b7ab8271e477c5d}%

%{Phalcon_Db_Column_20a6ab539e35e7b649f2e3bf9d3b8c1b}%

%{Phalcon_Db_Column_846082b7a1cccd51a8ed1531118d1284}%

%{Phalcon_Db_Column_7e5a3914d72763075dac9421afa09eaf}%

%{Phalcon_Db_Column_c0bbb2fbb4340a5b419ecd4e68a10db0}%

%{Phalcon_Db_Column_42b7247bd9fa4ae9bfd4660f93659f75}%

%{Phalcon_Db_Column_a3737575ca99d3f254eb45b99516b207}%

%{Phalcon_Db_Column_388279ecee95324210cf2a6c16b54037}%

%{Phalcon_Db_Column_1df975d10031b63ffe6defdb1ad209b3}%

%{Phalcon_Db_Column_32cad7d3d5e71dd924a3ab4065733813}%

%{Phalcon_Db_Column_3f4b01c5a78ae349a73bfd0d7792a324}%

%{Phalcon_Db_Column_646e1d653ab8b1574d2a5622f9ad77ef}%

%{Phalcon_Db_Column_4a792853e8f469d3416b38b0236442c9}%

%{Phalcon_Db_Column_5b3ff66ac56880765c23c1c1224da065}%

%{Phalcon_Db_Column_391718ac8aa4ad958ddef296d7d72392}%

%{Phalcon_Db_Column_bd5c0ba58ed79a9d70826584c74520a5}%

%{Phalcon_Db_Column_8ba2f6a20cb4aa469f34dd2772d1b1f2}%

