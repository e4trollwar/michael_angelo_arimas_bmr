%{Phalcon_Escaper_c7d5bd95b26638b65b86d5d8825dcef0}%
==========================

%{Phalcon_Escaper_0e025d391a3b2210875960a4d231675a|:doc:`Phalcon\\EscaperInterface <Phalcon_EscaperInterface>`}%

%{Phalcon_Escaper_d6fb4fa978b38ea26f6aa7ef2f69eb17}%

.. code-block:: php

    <?php

    $escaper = new Phalcon\Escaper();
    $escaped = $escaper->escapeCss("font-family: <Verdana>");
    echo $escaped; // {%Phalcon_Escaper_601ce232b332c318d0849070771fa764%}




%{Phalcon_Escaper_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Escaper_6889f239208ebcb8eb17545d13027f9a}%

%{Phalcon_Escaper_3b1461abc1a433fa04bf2a63ab274f1f}%

.. code-block:: php

    <?php

     $escaper->setEncoding('utf-8');





%{Phalcon_Escaper_a1af1a03857211816269e85af41dc667}%

%{Phalcon_Escaper_8ff5b6a1f5b47c06a70d81107c29713f}%

%{Phalcon_Escaper_b17cf16c751665ea946d2b36467d9884}%

%{Phalcon_Escaper_1da30aeff7a536c4e48abcbade7d26f0}%

.. code-block:: php

    <?php

     $escaper->setHtmlQuoteType(ENT_XHTML);





%{Phalcon_Escaper_6458395e6ce2ed4af99aab69b95fd3e8}%

%{Phalcon_Escaper_ec82d380063fac39d8b16ef660c5157b}%

%{Phalcon_Escaper_612aa540cde3237983c82182432d8a6b}%

%{Phalcon_Escaper_d43c0a432d3f5d39a13cbd07f2a20904}%

%{Phalcon_Escaper_3571c704ebf0eb8a1b071d4b20d98b5d}%

%{Phalcon_Escaper_70bbc52b38f38acf4977498ff8ce909b}%

%{Phalcon_Escaper_f77eed1aa939ba970da6105cfd8d7d7f}%

%{Phalcon_Escaper_630987fe56b4a0813b2545d8b644d441}%

%{Phalcon_Escaper_ca16655064a775b6d6b6ecd8910cd93b}%

%{Phalcon_Escaper_5a90eb20e9fc592ae4d84e74acf5832d}%

%{Phalcon_Escaper_8d8d229e93c5d3e5b932cc10e289d2b1}%

%{Phalcon_Escaper_24b4433d6239a750bd0477ba881cc9e3}%

%{Phalcon_Escaper_42c11c96526bd6360f9dcb16dcbac434}%

%{Phalcon_Escaper_9d20c5558a6da2884ea5d8a568522087}%

