%{Phalcon_Paginator_Adapter_QueryBuilder_4e8b768ab55535211084b19d15f3c118}%
===================================================

%{Phalcon_Paginator_Adapter_QueryBuilder_07d515ab63df29c1deb305348e631c5a|:doc:`Phalcon\\Paginator\\AdapterInterface <Phalcon_Paginator_AdapterInterface>`}%

%{Phalcon_Paginator_Adapter_QueryBuilder_6e970311a35c752a4c7c118e57f3f59c}%

.. code-block:: php

    <?php

      $builder = $this->modelsManager->createBuilder()
                       ->columns('id, name')
                       ->from('Robots')
                       ->orderBy('name');
    
      $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
          "builder" => $builder,
          "limit"=> 20,
          "page" => 1
      ));




%{Phalcon_Paginator_Adapter_QueryBuilder_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Paginator_Adapter_QueryBuilder_57ceaea4079c7ff3deb8a0d9cc79d9cb}%

%{Phalcon_Paginator_Adapter_QueryBuilder_282e2f7ac198967ea80103a8eaa78efe}%

%{Phalcon_Paginator_Adapter_QueryBuilder_3df4a0a909a65cde86c9c9793c2a9d49}%

%{Phalcon_Paginator_Adapter_QueryBuilder_b117ef5bd366cae1e58774c03c348639|:doc:`Phalcon\\Paginator\\Adapter\\QueryBuilder <Phalcon_Paginator_Adapter_QueryBuilder>`}%

%{Phalcon_Paginator_Adapter_QueryBuilder_2bad754f9d22b02f5f8812d56eb7cf24}%

%{Phalcon_Paginator_Adapter_QueryBuilder_fca2096fce63bec299d9815cf078baf2}%

%{Phalcon_Paginator_Adapter_QueryBuilder_90cddee2810cf758354ecd2b94c32afb}%

%{Phalcon_Paginator_Adapter_QueryBuilder_521b5ca8048862791607a1117fb02e7b}%

%{Phalcon_Paginator_Adapter_QueryBuilder_6f88b1a9195d526acac45cd163513e6d}%

%{Phalcon_Paginator_Adapter_QueryBuilder_a0faa7d136d260b0c7ebcbd05479b6b3}%

%{Phalcon_Paginator_Adapter_QueryBuilder_dd6b599f9cb42c6cdac5e37c3737e5c7}%

%{Phalcon_Paginator_Adapter_QueryBuilder_a176da34448a5a060e0fd4f0c624bc43|:doc:`Phalcon\\Paginator\\Adapter\\QueryBuilder <Phalcon_Paginator_Adapter_QueryBuilder>`}%

%{Phalcon_Paginator_Adapter_QueryBuilder_679820566451636fa8ccb7f4fe51a5e4}%

%{Phalcon_Paginator_Adapter_QueryBuilder_522261cb8a5ea914696088ccb518817b|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Paginator_Adapter_QueryBuilder_586d09bbe179d41d08ad281c7649e6a4}%

