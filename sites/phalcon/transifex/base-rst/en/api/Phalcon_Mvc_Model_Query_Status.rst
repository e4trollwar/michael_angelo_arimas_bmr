%{Phalcon_Mvc_Model_Query_Status_3f628efba81447a6a109089f318fa76c}%
============================================

%{Phalcon_Mvc_Model_Query_Status_3622500dc84f594f214c65b5d3a98b72|:doc:`Phalcon\\Mvc\\Model\\Query\\StatusInterface <Phalcon_Mvc_Model_Query_StatusInterface>`}%

%{Phalcon_Mvc_Model_Query_Status_046386c01dd48e465d82e7df2f548e0a}%

.. code-block:: php

    <?php

    $phql = "UPDATE Robots SET name = :name:, type = :type:, year = :year: WHERE id = :id:";
    $status = $app->modelsManager->executeQuery($phql, array(
       'id' => 100,
       'name' => 'Astroy Boy',
       'type' => 'mechanical',
       'year' => 1959
    ));
    
     //{%Phalcon_Mvc_Model_Query_Status_8ab3ca4e66f75dfe8e77b0328f02966a%}
     if ($status->success() == true) {
       echo 'OK';
     }




%{Phalcon_Mvc_Model_Query_Status_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Query_Status_7aae41575703feab1a20363edae0fb29|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Query_Status_8d796dbf856a5f0c87b2875a175170c5|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Query_Status_e7d1b640ea931800bb0340bdc9cccdf0}%

%{Phalcon_Mvc_Model_Query_Status_0275cb947f8840a688e0d40528dc8dcd|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Model_Query_Status_445b8fb19d3d35f3be5b1b77b1c2d742}%

%{Phalcon_Mvc_Model_Query_Status_fcf4d5cbf0e0cb1ec20d2d65827bd503}%

%{Phalcon_Mvc_Model_Query_Status_d88eafd2719e864ba99421d2be7a19d9}%

