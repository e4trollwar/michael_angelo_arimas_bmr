%{Phalcon_Mvc_Micro_c8980ee0423dfcd0fbb546afaf317102}%
=============================

%{Phalcon_Mvc_Micro_32f67d09941ad6b6963c440e030d031e|:doc:`Phalcon\\DI\\Injectable <Phalcon_DI_Injectable>`}%

%{Phalcon_Mvc_Micro_4f9d9cd107991437545b0b68e7c798cf|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Micro_b3d2b730352a0489b79bd2520efc25bd}%

.. code-block:: php

    <?php

     $app = new Phalcon\Mvc\Micro();
    
     $app->get('/say/welcome/{name}', function ($name) {
        echo "<h1>Welcome $name!</h1>";
     });
    
     $app->handle();




%{Phalcon_Mvc_Micro_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Micro_1f8893fbb5649d8903d8a3b4cf2f1522|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Micro_3c68978a4a489e359c13039b7df3f273}%

%{Phalcon_Mvc_Micro_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Micro_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Micro_740a6be8937dad87566c165a4b6a8c88|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_a8dfa1d5fd7ad1871188535ab381917f}%

%{Phalcon_Mvc_Micro_5aa0bcce799133e1813b95ceb95ddab2|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_f6de54e51c2d392ef831a96d9b79e021}%

%{Phalcon_Mvc_Micro_8591aa176c07003a0f79d758d4e2c01b|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_0232581065af0bae3a056c661f259e7b}%

%{Phalcon_Mvc_Micro_f3ded34fa72f66d5c8450ace374ea0b8|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_40d7f27c4f232dfdabda1507adb45f0c}%

%{Phalcon_Mvc_Micro_4c67c25ba3367252030ad975b02739bd|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_11e7efdfc40c792183ea3a0a25bc2b1a}%

%{Phalcon_Mvc_Micro_9b76ccf5ed5d55cbe950b5fd04524b62|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_9ff9d0f097137fe71c60c343c639f362}%

%{Phalcon_Mvc_Micro_852c053bd65c15e7a46d9981a66df845|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_9ce8e9cfdc4f1e99014a11a1c9ba5708}%

%{Phalcon_Mvc_Micro_20317fb4fd11cc0da97fcdb64d3975d0|:doc:`Phalcon\\Mvc\\Router\\RouteInterface <Phalcon_Mvc_Router_RouteInterface>`}%

%{Phalcon_Mvc_Micro_a674cd33cea531814c921719a89fe67b}%

%{Phalcon_Mvc_Micro_bc51517bee1aa0c08a394ea8b70c681c|:doc:`Phalcon\\Mvc\\Micro <Phalcon_Mvc_Micro>`|:doc:`Phalcon\\Mvc\\Collection <Phalcon_Mvc_Collection>`}%

%{Phalcon_Mvc_Micro_fb758395e60ad4485f62bb6ed6bf061b}%

%{Phalcon_Mvc_Micro_02189e0d08da0c8c167f7cbd125fd0c8|:doc:`Phalcon\\Mvc\\Micro <Phalcon_Mvc_Micro>`}%

%{Phalcon_Mvc_Micro_11fb6d256bc1173202e0856e5b37fafb}%

%{Phalcon_Mvc_Micro_1d1265300e44cd55e391ac6d934e89e4|:doc:`Phalcon\\Mvc\\RouterInterface <Phalcon_Mvc_RouterInterface>`}%

%{Phalcon_Mvc_Micro_4a1799429dd6c3246a753792e3e655e4}%

%{Phalcon_Mvc_Micro_fffa98c912a6c1b305d9a0b07b427f5c|:doc:`Phalcon\\DI\\ServiceInterface <Phalcon_DI_ServiceInterface>`}%

%{Phalcon_Mvc_Micro_a449993ee3393abc350c184ad6357ed8}%

%{Phalcon_Mvc_Micro_92373b9a5a2d3becfb6824fa3813988e}%

%{Phalcon_Mvc_Micro_184a72032317da488efec3c0543de94c}%

%{Phalcon_Mvc_Micro_4bd1cf3a8fa8d4f120de29473481a32b}%

%{Phalcon_Mvc_Micro_37f27f2457805d96c4bcf8f19ca94bcd}%

%{Phalcon_Mvc_Micro_d32101c53c8fe155967f964f2e281ac9}%

%{Phalcon_Mvc_Micro_ba99c28aa6fcaf0356cba562cfa5fc45}%

%{Phalcon_Mvc_Micro_22d4352f6785c9cce2d1839882ad5f25}%

%{Phalcon_Mvc_Micro_8774ece26e0f521c5d0631c385791f0f}%

%{Phalcon_Mvc_Micro_050f30cd1b1c4a7535f2642b675b5351}%

%{Phalcon_Mvc_Micro_9c67f71b16d2a4634cbd4720fdeafe8c}%

%{Phalcon_Mvc_Micro_5be23b840da4002f4065d9d763a168ac}%

%{Phalcon_Mvc_Micro_7db235ea67a1677dd29a12807b23f15b}%

%{Phalcon_Mvc_Micro_76bc5f92abfca2697b61e0f18c7abb86}%

%{Phalcon_Mvc_Micro_8ee422e7f6652edbe6eba33f4e5999d4}%

%{Phalcon_Mvc_Micro_0548ddf7c0b721fadb15d2e79460990c}%

%{Phalcon_Mvc_Micro_0b355e746d3f7bcb5e4887d93a66eddf}%

%{Phalcon_Mvc_Micro_8d12691ffb102540e2db03292b26c6e5}%

%{Phalcon_Mvc_Micro_df4dc139eb60857ed5fc204daabf031e}%

%{Phalcon_Mvc_Micro_7a47a31e35d942fc742fa1a5efd7bae5}%

%{Phalcon_Mvc_Micro_03b35bfdfc80ae7ddf1673b2e7ada337}%

.. code-block:: php

    <?php

    $app['request'] = new Phalcon\Http\Request();





%{Phalcon_Mvc_Micro_364cfb82552880599fda4d35ae0f6561}%

%{Phalcon_Mvc_Micro_b00dc8f634ac173bbc907073522f7290}%

.. code-block:: php

    <?php

    var_dump($app['request']);





%{Phalcon_Mvc_Micro_4a3b044d65d32b015e96e4275627e63b}%

%{Phalcon_Mvc_Micro_9adcd4f65d6c84d06323498dbfa55637}%

%{Phalcon_Mvc_Micro_b2374793c775449cbb35464f0d61a5b3|:doc:`Phalcon\\Mvc\\Micro <Phalcon_Mvc_Micro>`}%

%{Phalcon_Mvc_Micro_65f242cb9b222ec8ec9976e43308bd2b}%

%{Phalcon_Mvc_Micro_5dbb8d3287ddafd575b5873cc036276d|:doc:`Phalcon\\Mvc\\Micro <Phalcon_Mvc_Micro>`}%

%{Phalcon_Mvc_Micro_a73c5321742892ee9a8e0569c23bf4b1}%

%{Phalcon_Mvc_Micro_a7bf792d9f2957f9136b5690950a8f6c|:doc:`Phalcon\\Mvc\\Micro <Phalcon_Mvc_Micro>`}%

%{Phalcon_Mvc_Micro_e3c33b14fbb3cfc20b6d9ae2092c0eab}%

%{Phalcon_Mvc_Micro_82330635ad623454f51a0dbc0d4834ca}%

%{Phalcon_Mvc_Micro_1c3fd028814a268209d06625203093a6}%

%{Phalcon_Mvc_Micro_3e7e4ddacc724c78120150572bdab704|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Micro_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_Micro_5e6f785f2df62ae1a2a672663fb6b2e4|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Micro_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Mvc_Micro_8d859c2f0efce8383f08ae517e34a4af|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Micro_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Mvc_Micro_aa385da180999c3488ab22e043b6eb9d}%

%{Phalcon_Mvc_Micro_a8ec8322461cb1dce1953c9378484594}%

