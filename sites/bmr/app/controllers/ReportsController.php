<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class ReportsController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}
	
	public function indexAction()
	{
		$this->view->setVar('header_title', "Reports");
	}

}
