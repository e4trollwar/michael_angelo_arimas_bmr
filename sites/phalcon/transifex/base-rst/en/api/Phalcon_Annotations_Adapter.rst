%{Phalcon_Annotations_Adapter_eb4b8fea644c03c226ba8acd557017ef}%
================================================

%{Phalcon_Annotations_Adapter_047eb41a6efa152e801aeae48af56fe2|:doc:`Phalcon\\Annotations\\AdapterInterface <Phalcon_Annotations_AdapterInterface>`}%

%{Phalcon_Annotations_Adapter_979136e9d9b10a728b2e2ed3a74d79dc}%

%{Phalcon_Annotations_Adapter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Annotations_Adapter_3b070013e6548d7d1cce170eab8c8551|:doc:`Phalcon\\Annotations\\ReaderInterface <Phalcon_Annotations_ReaderInterface>`}%

%{Phalcon_Annotations_Adapter_2cb8a3ee7c6f6afa3aa1a49bbfb847e1}%

%{Phalcon_Annotations_Adapter_8a0b111415fd6e0bf19ec2099996c09c|:doc:`Phalcon\\Annotations\\ReaderInterface <Phalcon_Annotations_ReaderInterface>`}%

%{Phalcon_Annotations_Adapter_a011f5eee693a258028f7f96c8b6beab}%

%{Phalcon_Annotations_Adapter_021f890efbb32ad405cd70bb2f3b1429|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_14ce370b71a2b1e2abb49f73069cec7c}%

%{Phalcon_Annotations_Adapter_3d2163a2260b2d3872a1302d53eabae0}%

%{Phalcon_Annotations_Adapter_7f1166fe992a40497f8b1dac1d328c75}%

%{Phalcon_Annotations_Adapter_340900056032ffec116dbb4bbc403fc7|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Adapter_161a3a4b34c43cf6d494ef7ab77aeda4}%

%{Phalcon_Annotations_Adapter_80bf295e581d1c983d37ee6cb62f6232}%

%{Phalcon_Annotations_Adapter_7f1166fe992a40497f8b1dac1d328c75}%

%{Phalcon_Annotations_Adapter_9882b515c920adc7498ea7fce9a99ec5|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Adapter_7e10db38670478d87b35d418bbd07401}%

%{Phalcon_Annotations_Adapter_d0dbcca6029a7cb53b768356eaa61620|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_1a3350f1b513c275a7d5c89ccd988dfe}%

%{Phalcon_Annotations_Adapter_084e7d24560a30fec0876545038674eb|:doc:`Phalcon\\Annotations\\Reflection <Phalcon_Annotations_Reflection>`}%

%{Phalcon_Annotations_Adapter_3503318f332d8e974cb9a1673bb44a6b}%

