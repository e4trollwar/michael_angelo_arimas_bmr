%{Phalcon_Cache_Frontend_Igbinary_8498e23bf670fdf0bd82eca710b0773b}%
============================================

%{Phalcon_Cache_Frontend_Igbinary_162bdfd7b52f9a8bbf25b7cb2701bc34|:doc:`Phalcon\\Cache\\Frontend\\Data <Phalcon_Cache_Frontend_Data>`}%

%{Phalcon_Cache_Frontend_Igbinary_d4caea1dc5a1b9f969431a18993f6133|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Frontend_Igbinary_fa60d4d3525b752ce65c43d2be1f801a}%

.. code-block:: php

    <?php

    // {%Phalcon_Cache_Frontend_Igbinary_9cd95ece87c577ea3f0b4aa6508d59a4%}
    $frontCache = new Phalcon\Cache\Frontend\Igbinary(array(
    	"lifetime" => 172800
    ));
    
    // {%Phalcon_Cache_Frontend_Igbinary_ea92649ba53d9fc518c5ebfcf3a1892a%}
    // {%Phalcon_Cache_Frontend_Igbinary_b6981c87706285da49e15242b7d785bf%}
    // {%Phalcon_Cache_Frontend_Igbinary_985becda271eb01dd0940ab4705aa629%}
    $cache = new Phalcon\Cache\Backend\File($frontCache, array(
    	"cacheDir" => "../app/cache/"
    ));
    
    // {%Phalcon_Cache_Frontend_Igbinary_d99623f9040482f1edf8fed520e01ef6%}
    $cacheKey  = 'robots_order_id.cache';
    $robots    = $cache->get($cacheKey);
    if ($robots === null) {
    
    	// {%Phalcon_Cache_Frontend_Igbinary_88a8950b2ac85243c71666423bcd4a85%}
    	// {%Phalcon_Cache_Frontend_Igbinary_4aa8dcff400337e4dd2ef094fb66e362%}
    	$robots = Robots::find(array("order" => "id"));
    
    	// {%Phalcon_Cache_Frontend_Igbinary_f3762eaf6f2e3ac209ccfb08fd036c9c%}
    	$cache->save($cacheKey, $robots);
    }
    
    // {%Phalcon_Cache_Frontend_Igbinary_062c759655f7a03e81a39817083b59bb%}
    foreach ($robots as $robot) {
    	echo $robot->name, "\n";
    }




%{Phalcon_Cache_Frontend_Igbinary_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Frontend_Igbinary_346147a9407546082e932b7555d6ae42}%

%{Phalcon_Cache_Frontend_Igbinary_6a72b5df70e820ebbbb658e307cfdd82}%

%{Phalcon_Cache_Frontend_Igbinary_bf8d76d841c9e0ff5bc2145390806e3e}%

%{Phalcon_Cache_Frontend_Igbinary_a3a637a6fb529c4b3e05a71568b34d5b}%

%{Phalcon_Cache_Frontend_Igbinary_08c177613301040bd72546ff68086470}%

%{Phalcon_Cache_Frontend_Igbinary_1ae81dbbfeea219e1d883f5ae0052e6a}%

%{Phalcon_Cache_Frontend_Igbinary_cb420b927c72015a1baed352fde19602}%

%{Phalcon_Cache_Frontend_Igbinary_790ba726a84e8cc7d11c01ae6aeda21b}%

%{Phalcon_Cache_Frontend_Igbinary_fb4ea0b507a29eca01a0f21e824f87cb}%

%{Phalcon_Cache_Frontend_Igbinary_1df518f9eabe9f817151b63df9f51b70}%

%{Phalcon_Cache_Frontend_Igbinary_722c2e1e16bfe6f0d096aae0c9166480}%

%{Phalcon_Cache_Frontend_Igbinary_1ff5bfe3b4c822dd286e8ce81aac1b8b}%

%{Phalcon_Cache_Frontend_Igbinary_b145d17dcbbadf9356b39116f7244b29}%

%{Phalcon_Cache_Frontend_Igbinary_e3e99c0a1d8cbb1a36ae911dcb475f20}%

%{Phalcon_Cache_Frontend_Igbinary_d9632a3c1f7822b67ab24ed1bf397249}%

%{Phalcon_Cache_Frontend_Igbinary_3ff217b55d51515949922d3d8f3004eb}%

