<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class TrucksController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}

	public function addtruckAction()
	{
		$this->view->setVar('header_title', "Add New Truck");
		///LIST COMPANY
		$table_company = Company::find();
        foreach ($table_company as $m) {
            $data[] = array(
            	'id' => $m->id ,
                'campname' => $m->campname ,
                );
        }
        $data =json_encode($data);
        $this->view->list_comp =json_decode($data) ;
        ///END LIST COMPANY


        //EMPTY VARIABLES}
        $this->view->company = "" ;
        $this->view->plateno = "" ;
        $this->view->length = "" ;
        $this->view->width = "" ;
        $this->view->heigth = "" ;

        if ($this->request->isPost('save') == true) {
		 	//VARIABLE
		 	$company= $this->request->getPost('company');//POST COMPANY
		 	$this->view->company = $company ;//DISPLAY VALUE COMPANY********************
		 	$plateno= $this->request->getPost('plateno') ;//POST PLATE NO.
		 	$this->view->plateno = $plateno ;//DISPLAY VALUE PLATE NO.********************
		 	$length= $this->request->getPost('length') ;//POST LENGTH
		 	$this->view->length = $length ;//DISPLAY VALUE LENGTH********************
		 	$width= $this->request->getPost('width') ;//POST WIDTH
		 	$this->view->width = $width ;//DISPLAY VALUE WIDTH********************
		 	$heigth= $this->request->getPost('heigth') ;//POST HEIGHT
		 	$this->view->heigth = $heigth ;//DISPLAY VALUE COMPANYHEIGHT********************
		 	//ADD SAVE NEW DATA ENTRY

		 	
		 	$add = new Trucks();
		 	$add->plateno 	= $plateno;
		 	$add->length 	= $length;
		 	$add->width 	= $width;
		 	$add->heigth 	= $heigth;
		 	$add->company 	= $company;
		 	if ($add->save() == false) {
		 		echo "Umh, We can store data: ";
		 		
		 	} else {
		 		$this->view->company = "" ;
                $this->view->plateno = "" ;
                $this->view->length = "" ;
                $this->view->width = "" ;
                $this->view->heigth = "" ;
                header('Location: ../orders');
            }
        }
    }
    public function truckListAction($num="",$page="",$keyword="")
    {
      $this->view->setVar('header_title', "Truck Lists");

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      if(!isset($_GET["page"])){
       $currentPage=0;
   }else{
    $currentPage = (int) $_GET["page"];
}
        // The data set to paginate
if ($this->request->isPost('search') == true) {
    $search_q= $this->request->getPost('query');
    $query  = $this->modelsManager->createQuery("SELECT * FROM trucks WHERE trucks.plateno LIKE '%$search_q%'  ");
    $trucks      = $query->execute();  
}else{
    $query  = $this->modelsManager->createQuery("SELECT * FROM trucks  ");
    $trucks      = $query->execute();
}

$paginator   = new PaginatorModel(
    array(
        "data"  => $trucks,
        "limit" => 20,
        "page"  => $currentPage
        )
    );
$this->view->page= $paginator->getPaginate();


function display($model,$column){
    $sand = $model::find();
    $data = array();
    foreach ($sand as $m) {
        $data[] = array(
            "id" => $m->id,
            $column => $m->$column,
            );
    }
    return json_decode(json_encode($data));
}
        $this->view->data_company = display("Company","campname"); //LIST SAND TYPE

    }
    public function edittruckAction($id){
        $this->view->setVar('header_title', "Truck Lists");

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $sand = Trucks::findFirst(array("id='".$id."'"));
        $this->view->company =$sand->company ;
        $this->view->plateno = $sand->plateno ;
        $this->view->length = $sand->length ;
        $this->view->width = $sand->width ;
        $this->view->heigth = $sand->heigth ;


        ////update
        if ($this->request->isPost('update') == true) {
           

            $plateno= $this->request->getPost('plateno') ;//POST PLATE NO.
            $length= $this->request->getPost('length') ;//POST LENGTH
            $width= $this->request->getPost('width') ;//POST WIDTH
            $heigth= $this->request->getPost('heigth') ;//POST HEIGHT

           $update = Trucks::findFirst('id='.$id.'');
           $update->plateno   = $plateno;
           $update->length   = $length;
           $update->width   = $width;
           $update->heigth   = $heigth;

           if ($update->save() == false) {
            echo "Umh, We can store data: ";
            foreach ($update->getMessages() as $message) {
                echo $message;
            }
        } else {
           header('Location: ../trucks/edittruck/'.$id.'');
        }


        }
    }
    public function dlttruckAction($id){
        $this->view->setVar('header_title', "Truck Lists");

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $sand = Trucks::findFirst(array("id='".$id."'"));
        $this->view->company =$sand->company ;
        $this->view->plateno = $sand->plateno ;
        $this->view->length = $sand->length ;
        $this->view->width = $sand->width ;
        $this->view->heigth = $sand->heigth ;


        ////update
        if ($this->request->isPost('delete') == true) {

                $dltPhoto = Trucks::findFirst('id='.$id.' ');
                $data = array('error' => 'Not Found');
                if ($dltPhoto) {
                    if($dltPhoto->delete()){
                       header('Location: ../truckList');
                    }
                }

        }
    }

}
