%{Phalcon_Acl_5138afcce4a91eb1cb756e079f59bbf8}%
===============================

%{Phalcon_Acl_2b7d430cf4f7df7eb2b2d3e005acb9b6}%

.. code-block:: php

    <?php

    $acl = new Phalcon\Acl\Adapter\Memory();
    
    //{%Phalcon_Acl_a2b321754df456f64c5e1ab2fbd3bb6d%}
    $acl->setDefaultAction(Phalcon\Acl::DENY);
    
    //{%Phalcon_Acl_21e051ef1d092d0b1a5ddec0bef33b9c%}
    $roleAdmins = new Phalcon\Acl\Role('Administrators', 'Super-User role');
    $roleGuests = new Phalcon\Acl\Role('Guests');
    
    //{%Phalcon_Acl_e497d9691f5d8e2fc5cb41d685a2145a%}
    $acl->addRole($roleGuests);
    
    //{%Phalcon_Acl_b40368f5dd3b3b8b0b4aea19cfeec73b%}
    $acl->addRole('Designers');
    
    //{%Phalcon_Acl_f274c5592fac24c04d7113d3844cbdd9%}
    $customersResource = new Phalcon\Acl\Resource('Customers', 'Customers management');
    
    //{%Phalcon_Acl_bc727b5dce45b9c2738dbf9fa8f93676%}
    $acl->addResource($customersResource, 'search');
    $acl->addResource($customersResource, array('create', 'update'));
    
    //{%Phalcon_Acl_4b17262ed636f44d00c8e917dacad39e%}
    $acl->allow('Guests', 'Customers', 'search');
    $acl->allow('Guests', 'Customers', 'create');
    $acl->deny('Guests', 'Customers', 'update');
    
    //{%Phalcon_Acl_ce3445ff79c3445a6db89250e0049b3f%}
    $acl->isAllowed('Guests', 'Customers', 'edit'); //{%Phalcon_Acl_0b2da28a441d04619e64bdbd1693747c%}
    $acl->isAllowed('Guests', 'Customers', 'search'); //{%Phalcon_Acl_691d9ca32d3773a83b41f05322d4c409%}
    $acl->isAllowed('Guests', 'Customers', 'create'); //{%Phalcon_Acl_691d9ca32d3773a83b41f05322d4c409%}




%{Phalcon_Acl_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Acl_b9aeb51833719838a7ca4879f17c91db}%

%{Phalcon_Acl_b37fe1449e470346371261b7c70c2efc}%

