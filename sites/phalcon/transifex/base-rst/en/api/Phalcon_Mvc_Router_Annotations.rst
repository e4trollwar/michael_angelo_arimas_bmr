%{Phalcon_Mvc_Router_Annotations_e8de461ba019cb642f7b1137e13b6791}%
===========================================

%{Phalcon_Mvc_Router_Annotations_1450cbd8734e7b310268f61c2161ba4e|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_70af9b21b1dd3a7a4fbbc8400de499aa|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Mvc\\RouterInterface <Phalcon_Mvc_RouterInterface>`}%

%{Phalcon_Mvc_Router_Annotations_8970414a269d170edd23fc4d6fe39209}%

.. code-block:: php

    <?php

     $di['router'] = function() {
    
    	//{%Phalcon_Mvc_Router_Annotations_b9d666c79f94f3cfada1ecd17e50f52d%}
    	$router = new \Phalcon\Mvc\Router\Annotations(false);
    
    	//{%Phalcon_Mvc_Router_Annotations_f6069f0276da087288c2a99af5dddf9a%}
     		$router->addResource('Robots', '/robots');
    
     		return $router;
    };




%{Phalcon_Mvc_Router_Annotations_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Router_Annotations_35e1f3e13330db4a804fe23eb5719b8d}%

%{Phalcon_Mvc_Router_Annotations_0db2857a54ac9b47b8f5b9fb24e084c0}%

%{Phalcon_Mvc_Router_Annotations_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Router_Annotations_dc287b9cc0e195093aacc9368bd97f9a|:doc:`Phalcon\\Mvc\\Router\\Annotations <Phalcon_Mvc_Router_Annotations>`}%

%{Phalcon_Mvc_Router_Annotations_cdd3f8722771d3245e6867fc86c47fe4}%

%{Phalcon_Mvc_Router_Annotations_24ad991223cf6d42289f1a1c6e710d8f|:doc:`Phalcon\\Mvc\\Router\\Annotations <Phalcon_Mvc_Router_Annotations>`}%

%{Phalcon_Mvc_Router_Annotations_9ae19c47e7ac3fbe8606b8802fd16cd8}%

%{Phalcon_Mvc_Router_Annotations_d769b6ffce91447d799d522b1d93ca6a}%

%{Phalcon_Mvc_Router_Annotations_3135042795e774569b4d9924611aa581}%

%{Phalcon_Mvc_Router_Annotations_b352b4a2ac5b2fcecb56c46803686a5b}%

%{Phalcon_Mvc_Router_Annotations_7fc0681bd94373af06a3ba15ea65c17b}%

%{Phalcon_Mvc_Router_Annotations_e0e0395789064b4e39df81d70b1420ab|:doc:`Phalcon\\Annotations\\Annotation <Phalcon_Annotations_Annotation>`}%

%{Phalcon_Mvc_Router_Annotations_07e8ecd1504bc9683c19eb971037f707}%

%{Phalcon_Mvc_Router_Annotations_accb9473489d3137a588aa5f887d5692}%

%{Phalcon_Mvc_Router_Annotations_b5ad8f8a07dc22f85dd7f6e6dc2fa106}%

%{Phalcon_Mvc_Router_Annotations_1a8114f1c4dc1f4efbf891d453fc0137}%

%{Phalcon_Mvc_Router_Annotations_5c291192fc803b7904fc5c04051f4513}%

%{Phalcon_Mvc_Router_Annotations_9feed4ae978edd4eb1adde4089d9f43b}%

%{Phalcon_Mvc_Router_Annotations_373bb47f262535f32859926cf02d7be5}%

%{Phalcon_Mvc_Router_Annotations_f12ccc23b3862c3dadcf9ed28b190fd9}%

%{Phalcon_Mvc_Router_Annotations_d9ef0d1963c00de67c0b29fcf6398005}%

%{Phalcon_Mvc_Router_Annotations_a2c2f5b3bf4fa27e8484e1c891473c11|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Router_Annotations_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_Router_Annotations_ad77ad752aee4b0a6f6b668d3a2b3ee5|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Router_Annotations_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_Router_Annotations_ed4c9e3ff8faaf00d8ff00996f67619e}%

%{Phalcon_Mvc_Router_Annotations_63e633b52b68f4e941aff5e61ccfbac3}%

%{Phalcon_Mvc_Router_Annotations_217f37e16b11ec5f83b2d6f16b82b3ff|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_1e659dd8ac820457014ab3db8d16336a}%

.. code-block:: php

    <?php

    $router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);





%{Phalcon_Mvc_Router_Annotations_d5790b64736677c0551c70befa0d312f|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_9ce76f9ec221486accf4a189909d8eb0}%

%{Phalcon_Mvc_Router_Annotations_c112755cf73b722db9bde92b1861f2b8|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_f8961d275872562f2af28060a262fd04}%

%{Phalcon_Mvc_Router_Annotations_c7e12e694d32b67d26f8e4301f9ec71d}%

%{Phalcon_Mvc_Router_Annotations_7eebb99d9ca319b62c07d8964bbd7342}%

%{Phalcon_Mvc_Router_Annotations_158f362bf1e3fb159d0df7949b70e58f|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_d6dc30b314ef7afb8dbf713d2f52ef81}%

%{Phalcon_Mvc_Router_Annotations_9073cc33b5ba55e5965ef7ffbce5acc6}%

%{Phalcon_Mvc_Router_Annotations_417a8251e9314b6645fb491639a78ee1}%

%{Phalcon_Mvc_Router_Annotations_6b0ee9c28e8efa0dd08e920e70e04922|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_1bddb8ba3418530b9db767f0161fc12d}%

%{Phalcon_Mvc_Router_Annotations_93942c0c37a1ef1b17eb3de50cd0b970}%

%{Phalcon_Mvc_Router_Annotations_16ad75cb10aac90324071cb4369e7c2a}%

%{Phalcon_Mvc_Router_Annotations_228090d870ca9aa8a43c300e6c981d38|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_2c676539e85e0e19da7b13c5801ba8a2}%

%{Phalcon_Mvc_Router_Annotations_2a6c7e15b273c81450d74f94043bb4c4}%

%{Phalcon_Mvc_Router_Annotations_a94e66fc01b7c8d78bc6aa590b478337}%

%{Phalcon_Mvc_Router_Annotations_dfeb1e036d745d7deba89d9f6b5cb7c6|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_e723bfca7c4d8c15382e91e34fda5957}%

.. code-block:: php

    <?php

     $router->setDefaults(array(
    	'module' => 'common',
    	'action' => 'index'
     ));





%{Phalcon_Mvc_Router_Annotations_dcb4d86de78debbf78e593c99eb0723c}%

%{Phalcon_Mvc_Router_Annotations_cabb62d96fd0834fa8ef5102da79ba95}%

%{Phalcon_Mvc_Router_Annotations_2301e3c85d7d73069ab57335158cd777|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_c8bd4141a06a69a156f2682eb6931d5d}%

.. code-block:: php

    <?php

     $router->add('/about', 'About::index');





%{Phalcon_Mvc_Router_Annotations_2ee3676f1bc55121432a1cd630129e57|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_f74b99e1e164260f7718bb4f740c0c71}%

%{Phalcon_Mvc_Router_Annotations_ef169086058688f0cc64ddc1b7cf8b15|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_868af429955c0ecbbaa95f7fa5e67133}%

%{Phalcon_Mvc_Router_Annotations_1767bb9e405cc782b8ff102d7726b47e|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_f32c97339be876b8e10fb86cbe916acc}%

%{Phalcon_Mvc_Router_Annotations_8b5caad76c958947138d245b95716cf2|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_4d752b0f335109eb99ebea41e9977cbc}%

%{Phalcon_Mvc_Router_Annotations_d8138c2bfe6dd447473a81eeb162c68e|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_1cf981139831fbc2661c7e1ba2532c59}%

%{Phalcon_Mvc_Router_Annotations_396ae700e524e6b154fc77b3c86dae62|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_c5e8d939bd9ee0fae4a2047ea3f17f18}%

%{Phalcon_Mvc_Router_Annotations_488745b5f7304d4db5814cd34c78baeb|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_58a4300721cf5feeb2c0899372a9c900}%

%{Phalcon_Mvc_Router_Annotations_0d069bfc5a2788501d9d5a9ebf7a5af6|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_bd6db26afbcc17e967bbde8c762ca756}%

%{Phalcon_Mvc_Router_Annotations_576411306cef32cd604d350ac486e02d|:doc:`Phalcon\\Mvc\\Router <Phalcon_Mvc_Router>`}%

%{Phalcon_Mvc_Router_Annotations_78cbcb15186b08545e4422caeba274ec}%

%{Phalcon_Mvc_Router_Annotations_c26a0c540ff0d006c03f553f3afd1055}%

%{Phalcon_Mvc_Router_Annotations_e9a944524b038eeda5d4cd523610fdd9}%

%{Phalcon_Mvc_Router_Annotations_4aad7a8d0db93e358720b9bab6efe948}%

%{Phalcon_Mvc_Router_Annotations_8521921da5d7b8a4ff1c299d5dbcabe0}%

%{Phalcon_Mvc_Router_Annotations_747a2f601f15e7666a212adaffb6778c}%

%{Phalcon_Mvc_Router_Annotations_a18dd289bb264d04ea72fc170ec4a372}%

%{Phalcon_Mvc_Router_Annotations_23e75cc0b59f30d99a97334c7b3c7a17}%

%{Phalcon_Mvc_Router_Annotations_2c6fb6907ef9185028578e5a754ce3e1}%

%{Phalcon_Mvc_Router_Annotations_9586b25e6d99927227b5135d9e30d7be}%

%{Phalcon_Mvc_Router_Annotations_b7ccc390683274e26381e292617e5300}%

%{Phalcon_Mvc_Router_Annotations_c1c8413105b042094df99c062a5f0f1c}%

%{Phalcon_Mvc_Router_Annotations_5192b5f7cbd96678251c53086304699f}%

%{Phalcon_Mvc_Router_Annotations_aaf92817c69ef5494da42aab24c2e95f|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_9d206e2acf5932da14cc0a4c36baf912}%

%{Phalcon_Mvc_Router_Annotations_c550fb2e9e161dd341597aea549b03e5}%

%{Phalcon_Mvc_Router_Annotations_b0021d449f14a9b028855c9f0ecd6f0a}%

%{Phalcon_Mvc_Router_Annotations_c5cfb3a913f28418eece19a02d33a0b6}%

%{Phalcon_Mvc_Router_Annotations_03660350d3ef62328c81fe20934cc21d}%

%{Phalcon_Mvc_Router_Annotations_28ee6f1a37ce824900177036f8dcc324|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_80f418139edc49845c3be611f06f30dc}%

%{Phalcon_Mvc_Router_Annotations_38dd0161bee18a75b1c6f2f0b34da2d1|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_81a2cf161aaba093d2e2d69c2de50fac}%

%{Phalcon_Mvc_Router_Annotations_b34079baff7d071bffaac12545090184|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Annotations_187eb811f0dd111abca2dcb8421c06cb}%

%{Phalcon_Mvc_Router_Annotations_7202b0a147729e2a0e36de3e851a494c}%

%{Phalcon_Mvc_Router_Annotations_36f78689a8bba7953326923297109939}%

