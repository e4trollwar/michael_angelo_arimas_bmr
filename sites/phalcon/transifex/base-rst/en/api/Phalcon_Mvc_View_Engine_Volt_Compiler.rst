%{Phalcon_Mvc_View_Engine_Volt_Compiler_00a73fc62c75cceccf1498edb8dac2eb}%
====================================================

%{Phalcon_Mvc_View_Engine_Volt_Compiler_0b066e4bf73264afdc3e24f71410b010|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_dbf113fd47e1957ec4529ebd9b5554e2}%

.. code-block:: php

    <?php

    $compiler = new \Phalcon\Mvc\View\Engine\Volt\Compiler();
    
    $compiler->compile('views/partials/header.volt');
    
    require $compiler->getCompiledTemplatePath();




%{Phalcon_Mvc_View_Engine_Volt_Compiler_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_View_Engine_Volt_Compiler_774161d4c73adaac82a68cacd2c8789a|:doc:`Phalcon\\Mvc\\ViewInterface <Phalcon_Mvc_ViewInterface>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_d3b1bb1b579590c4844ba7f6c905b393}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_e97c031a5ceca332dc5c3e17e495f5e7}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_d51f99c39a574eb4da00e89bfda71593}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_b1544d17f44edbf4d54cdddd250612b5}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_81d50dc0ea0c76494724ed7cfd73de7e}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_ccfee2afabf6057c0746c14385e60b0a}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_8cd706d17906efae1c72cecaa708884f}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_f660c56730f18f60e0766eb261011348}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_c1660bc9504c833275cdaddc01f9979d}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_5b83c772c848955add59f5c695007aaa}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_aba0faedf90b1d2ff16851aa7ab965a5|:doc:`Phalcon\\Mvc\\View\\Engine\\Volt\\Compiler <Phalcon_Mvc_View_Engine_Volt_Compiler>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_bcb3f92da359d0a2b74971af48cd2b54}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_d03b1e61f2f6e11d2b06e6f23246008c}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_22a7fcae1092835c7322b12fa81d246e}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_da9e7785ea59922b449dfc16b06a9338|:doc:`Phalcon\\Mvc\\View\\Engine\\Volt\\Compiler <Phalcon_Mvc_View_Engine_Volt_Compiler>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_0a0dee0a005d571abe9d7f59ccd68d92}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_ac34fe57a7a54e14eb1756348f36b155}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_bf7d5a6164165c5ac1c5053a575b010a}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_6a61444719621191ae2bb53cd3a9aae9|:doc:`Phalcon\\Mvc\\View\\Engine\\Volt\\Compiler <Phalcon_Mvc_View_Engine_Volt_Compiler>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_42deb25635a349adcf4cc41d51e79e07}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_0ae451e58ca4099e975f2ba334725208}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_63acdef3293b135ec4fae7213baa75d2}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_36026773df779851324232fe4e09b228|:doc:`Phalcon\\Mvc\\View\\Engine\\Volt\\Compiler <Phalcon_Mvc_View_Engine_Volt_Compiler>`}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_04f2571f8faa4e345691b9959b14351d}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_b2b1275ce2e38693d6ef1178d3d8360d}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_7ee2ccb599e59a420db5b98bd29a020f}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_93f46541d9371b0e5b255b897c51651b}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_cfe219ae28ea97f3ba0f04760383a0e7}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_52d795b7c1b63e1f94ce149de311b1e7}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_1af7412e423cf178b4cb45da162a1fed}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_9ec535daf0f4515af5ed39a4e02e4c1b}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_67156d6d3a95609788ecac8464c045a1}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_3944e8882af7329230fcf4aa0fd1ea5a}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_eafe23cd2cd3f1af1f850c4264a86dbd}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_9eb5ab9dcc1abbcf8ae66454569f3c84}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_0359f1ae0fd26885e41fba27057cdd54}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_33485e14cbea6fbff791c950bb778e4e}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_521b61eaf5eb8d957dd40dc00f002e8d}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_792bb0bfebadbe0cf52a183f977b1a17}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_480354e8dc42adb2e4609d15da4380ce}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_d4443358446d10a4495b95642874fe3b}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_2799c9560e49225ca0187e8fae1a7a15}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_2318f27b982163692927ff6fe37670ce}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_0cd5ce641a70311c88b257feec318ef9}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_eba8caec9c547926eb8634ece8badf93}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_b35b6da2080f7c492857530d0af995c4}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_9373b28dfb0940001c565bec728fc93e}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_e95ada0e59ae89713ea07693a143b3dc}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_5c15d702a45618a8d73be2424bfff673}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_228d35fe5a51158cf40b02c693a28cf9}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_086d9937f801ac6bfec8a112a03eb4f7}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_5e1f9f4e52cd480fc6a1e5703b380bd6}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_b04517eccf8013f932e48c09092ddecb}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_75f71610af0e06b7f11881ed7831ce95}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_b8f810d39d511d8a49c4e7d402ccfc09}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_7eca6c3ae2fa34402acd77fc1d5dbfda}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_d22abd8e4d2c1bdfe13cc4fade60d377}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_15bf7bf765c2c68adf87161d6568b1f7}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_fa66c4fe1b30ee03b337e56efc2ea366}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_2bded6309c6dccbfb2a17543f4ebb566}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_766c026e9b46627c8bf0e315864bbd27}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_20ff9009c01308c7e3eff29807e0a272}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_66b8c68e29999a0a1045152d6df9be20}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_5f464e8aaa94415555f218d43a8a90c0}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_f8d44283782d26d8b85ef0a2f7aad3ff}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_37c18be1719c5a080c405d26dd825d10}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_48bac71ce0c79178df3e607d7ce97f58}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_ce4c26ceabf6a67641dfd8d31f540e10}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_1a528ede945964557fdcd01c3c656ccf}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_04837ed962c81a2e713131705a8c6ea6}%

.. code-block:: php

    <?php

     echo $compiler->compileString('{{ "hello world" }}');





%{Phalcon_Mvc_View_Engine_Volt_Compiler_4fb698159cc81b9f66f02846603665b0}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_9a1649a29a4c59df6556152dea24ef8d}%

.. code-block:: php

    <?php

    $compiler->compile('views/layouts/main.volt', 'views/layouts/main.volt.php');





%{Phalcon_Mvc_View_Engine_Volt_Compiler_441cc9d24ed9b654c48daaf658be0153}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_bebf570e5a381fa3d09bb467b880f6f0}%

.. code-block:: php

    <?php

    $compiler->compile('views/layouts/main.volt');
    require $compiler->getCompiledTemplatePath();





%{Phalcon_Mvc_View_Engine_Volt_Compiler_05e462343bfc514fc7a7751996218e72}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_d9971f72a14abe058bb966b8fae83c2d}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_f64e460bf9c8e5b3b92c41b094f9a458}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_0e975d37d5553c9f8bd3ce36052b4107}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_965678ba96c239892881ed4e4ed2a838}%

%{Phalcon_Mvc_View_Engine_Volt_Compiler_9076837d4445c4b5aa3aced4a3c2e0c3}%

