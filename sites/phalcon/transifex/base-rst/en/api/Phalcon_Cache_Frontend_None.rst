%{Phalcon_Cache_Frontend_None_e107290d4ab37e6c288df92086b44435}%
========================================

%{Phalcon_Cache_Frontend_None_162bdfd7b52f9a8bbf25b7cb2701bc34|:doc:`Phalcon\\Cache\\Frontend\\Data <Phalcon_Cache_Frontend_Data>`}%

%{Phalcon_Cache_Frontend_None_d4caea1dc5a1b9f969431a18993f6133|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Frontend_None_4b5286ca64152a7c70531d1e56fca3d7}%

.. code-block:: php

    <?php

    //{%Phalcon_Cache_Frontend_None_00b0efa0a3129cc403b6077c1179e662%}
    $frontCache = new Phalcon\Cache\Frontend\None();
    
    // {%Phalcon_Cache_Frontend_None_415c404a0afde56e80fc82290caab243%}
    // {%Phalcon_Cache_Frontend_None_27c9c860a0e993fc9cd8fe1f98c2dd13%}
    $cache = new Phalcon\Cache\Backend\Memcache($frontCache, array(
    	"host" => "localhost",
    	"port" => "11211"
    ));
    
    // {%Phalcon_Cache_Frontend_None_626d85a61b2044b590823ba3d8fac1f4%}
    $cacheKey = 'robots_order_id.cache';
    $robots    = $cache->get($cacheKey);
    if ($robots === null) {
    
    	// {%Phalcon_Cache_Frontend_None_fd57b58d202ea6f892fa4543c021e8a0%}
    	// {%Phalcon_Cache_Frontend_None_4aa8dcff400337e4dd2ef094fb66e362%}
    	$robots = Robots::find(array("order" => "id"));
    
    	$cache->save($cacheKey, $robots);
    }
    
    // {%Phalcon_Cache_Frontend_None_062c759655f7a03e81a39817083b59bb%}
    foreach ($robots as $robot) {
    	echo $robot->name, "\n";
    }




%{Phalcon_Cache_Frontend_None_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Frontend_None_9c95fde9fd5b5cc68dba068eb7ea3bb0}%

%{Phalcon_Cache_Frontend_None_ea3a4b3da1853748e71ab57c9046e915}%

%{Phalcon_Cache_Frontend_None_56be1010900f0d9e503c934e74d727f7}%

%{Phalcon_Cache_Frontend_None_6ec82edd259df44f4a5b6bfe41f6f6f9}%

%{Phalcon_Cache_Frontend_None_9b74fa69c19945f14eeb42ec742b967e}%

%{Phalcon_Cache_Frontend_None_d4b7cd788da435df9aaf3bec613fa734}%

%{Phalcon_Cache_Frontend_None_08c177613301040bd72546ff68086470}%

%{Phalcon_Cache_Frontend_None_1ae81dbbfeea219e1d883f5ae0052e6a}%

%{Phalcon_Cache_Frontend_None_fb4ea0b507a29eca01a0f21e824f87cb}%

%{Phalcon_Cache_Frontend_None_1df518f9eabe9f817151b63df9f51b70}%

%{Phalcon_Cache_Frontend_None_722c2e1e16bfe6f0d096aae0c9166480}%

%{Phalcon_Cache_Frontend_None_1ff5bfe3b4c822dd286e8ce81aac1b8b}%

%{Phalcon_Cache_Frontend_None_b145d17dcbbadf9356b39116f7244b29}%

%{Phalcon_Cache_Frontend_None_e3e99c0a1d8cbb1a36ae911dcb475f20}%

%{Phalcon_Cache_Frontend_None_d9632a3c1f7822b67ab24ed1bf397249}%

%{Phalcon_Cache_Frontend_None_3ff217b55d51515949922d3d8f3004eb}%

