


 //insert Query
var app = angular.module("addcompApp", []);
app.controller("addcompCtrl", function($scope, $http) {

    
  
    $scope.insert = function(data) {

        swal({title: "Are you sure?",
            text: "You want to save this record?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Save it?",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false,
            closeOnSuccess: false,
            closeOnSubmit: false,
        },
        function(isConfirm){
            if(isConfirm){
                $http({
                    method:"post",
                    url:"addcompany",
                    data:{
                    'compname': data.compname,
                    'address': data.address,
                    'contact': data.contact
                    },
                }
            ).success(function(data) {
                
               
            });

                swal({ 
                      title: "success",
                       text: "Record Successfully Added",
                        type: "success" 
                      },
                      function(success){
                        if(success){
                            window.location.reload();
                        }
                      }

                      );
            }


            else{

                swal("Cancelled","", "error");
            }


        });

    }
});






 //UPDATE Query
var app = angular.module("updatecompApp", []);
app.controller("updatecompCtrl", function($scope, $http) {
    $scope.id = phpVarsEdit["value1"];
    $scope.compname = phpVarsEdit["value2"];
    $scope.address = phpVarsEdit["value3"];
    $scope.contact = Number(phpVarsEdit["value4"]);
  
    $scope.update = function() {
        swal({title: "Are you sure?",
            text: "You want to update this record?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, update it?",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false,
            closeOnSuccess: false,
            closeOnSubmit: false,

 
        },
        function(isConfirm){
            if(isConfirm){
                $http({
                    method:"POST",
                    url:"editcompanyojt",
                    data:{
                    'compname': $scope.compname,
                    'address': $scope.address,
                    'contact': $scope.contact,
                    'id'     : $scope.id
                    },

                }

            ).success(function(data) {
                
                $scope.id = phpVarsEdit["value1"];
                $scope.compname = phpVarsEdit["value2"];
                $scope.address = phpVarsEdit["value3"];
                $scope.contact = Number(phpVarsEdit["value4"]);
            });
                 swal({ 
                      title: "success",
                       text: "Record Successfully Saved",
                        type: "success" 
                      },
                      function(success){
                        if(success){
                            window.location.reload();
                        }
                      }

                      );
            }


            else{

                swal("Cancelled","", "error");
            }


        });

    }
});





 //Delete Query
var app = angular.module("deletecompApp", []);
app.controller("deletecompCtrl", function($scope, $http) {
    $scope.id = phpVarsDelete["value1"];
    $scope.compname = phpVarsDelete["value2"];
    $scope.address = phpVarsDelete["value3"];
    $scope.contact = Number(phpVarsDelete["value4"]);
    $scope.delete = function() {
        swal({title: "Are you sure?",
            text: "You want to delete this record?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it?",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false,
            closeOnSuccess: false,
            closeOnSubmit: false,
        },
        function(isConfirm){
            if(isConfirm){
                $http({
                    method:"POST",
                    url:"dltcompany",
                    data:{
                    'compname': $scope.compname,
                    'address': $scope.address,
                    'contact': $scope.contact,
                    'id': $scope.id,
                    },
                }
            ).success(function(data) {
                  swal({ 
                      title: "success",
                       text: "Record Successfully Deleted",
                        type: "success" 
                      },
                      function(success){
                        if(success){
                             window.location.href = "http://bmr.web/company/complist";
                        }
                      }
                );
            });

            }


            else{

                swal("Cancelled","", "error");
            }


        });

    }
});





