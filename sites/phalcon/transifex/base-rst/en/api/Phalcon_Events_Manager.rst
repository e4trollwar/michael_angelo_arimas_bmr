%{Phalcon_Events_Manager_f84174e698e66ba57a5aa30aa2e0686f}%
==================================

%{Phalcon_Events_Manager_60cc6a8f9e81cd8f29ca4a2aaf7fba19|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Events_Manager_a32248a6e4ec3abd7154ff4113df7277}%

%{Phalcon_Events_Manager_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Events_Manager_3144831bf8f48d0202089dc0e5edcdda}%

%{Phalcon_Events_Manager_f8417e74b638a037dabb41552b8fa08a}%

%{Phalcon_Events_Manager_76fa04f39d30ae84c46e23feb0144b6c}%

%{Phalcon_Events_Manager_b24ebfa9b73c743359d419c05bf55cc5}%

%{Phalcon_Events_Manager_e39c6a19726f52296f05652fd6b9a25c}%

%{Phalcon_Events_Manager_cf5b2d7ebd0bdcb07ac5e3090d6777f6}%

%{Phalcon_Events_Manager_258b88b0ba3883ca2ccf41e167bb1e17}%

%{Phalcon_Events_Manager_2974a7b794f4a87b375303ca81c24ad8}%

%{Phalcon_Events_Manager_1c9b3d483ecb784991b39a13604ca42c}%

%{Phalcon_Events_Manager_ab710aef412df6a86226c39b84797a6d}%

%{Phalcon_Events_Manager_0e265fc5f2db0c1ef1311d4ed62533ce}%

%{Phalcon_Events_Manager_34752115602eebed401716e7d1d0b40e}%

%{Phalcon_Events_Manager_d8667b9aaa76fd4570d6a8684f63c9fe}%

%{Phalcon_Events_Manager_a3861d9c508261a70d76af1f06a3bee4}%

%{Phalcon_Events_Manager_62b3f501e627d76390ed952ab4d14043|:doc:`Phalcon\\Events\\Event <Phalcon_Events_Event>`}%

%{Phalcon_Events_Manager_9ae33ecc9b1ea9ea85be0ff2f1b8e8a2}%

%{Phalcon_Events_Manager_a6ded3d0ad5a8dca90488c2edaefb074}%

%{Phalcon_Events_Manager_ae06ecd9139d3184967969203d5b22ad}%

.. code-block:: php

    <?php

    $eventsManager->fire('db', $connection);





%{Phalcon_Events_Manager_aa07544390ea080a2348e9afd2a8c39f}%

%{Phalcon_Events_Manager_55713633ca7193afd98c211583ea474e}%

%{Phalcon_Events_Manager_116e1ed5f812fdde17c6d711e66b2ed2}%

%{Phalcon_Events_Manager_45efa7fa36e21a8c37f0af17f6ccb6f3}%

%{Phalcon_Events_Manager_aeda6f3f989168bbb6b77c4d0d81c168}%

%{Phalcon_Events_Manager_68cf0a3bff1e41b907cf955f570c5249}%

