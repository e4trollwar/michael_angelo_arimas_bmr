%{Phalcon_CLI_Dispatcher_c2effd543ed2bd15b376f742104845b0}%
==================================

%{Phalcon_CLI_Dispatcher_44aa304ea627ee41e58834d014ea49e8|:doc:`Phalcon\\Dispatcher <Phalcon_Dispatcher>`}%

%{Phalcon_CLI_Dispatcher_c7755177d10f05aa28ed8deff06f3b8e|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\DispatcherInterface <Phalcon_DispatcherInterface>`}%

%{Phalcon_CLI_Dispatcher_a429b4a82913f4b189259371f42983ee}%

.. code-block:: php

    <?php

    $di = new Phalcon\DI();
    
    $dispatcher = new Phalcon\CLI\Dispatcher();
    
      $dispatcher->setDI($di);
    
    $dispatcher->setTaskName('posts');
    $dispatcher->setActionName('index');
    $dispatcher->setParams(array());
    
    $handle = $dispatcher->dispatch();




%{Phalcon_CLI_Dispatcher_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_CLI_Dispatcher_2ab1c3855cfa4f30ea8681e49a7aa26e}%

%{Phalcon_CLI_Dispatcher_d5edf0dea21fd1b1f0629d8dd8748263}%

%{Phalcon_CLI_Dispatcher_7c266fcd7f0c18b301f30e9d4c347d35}%

%{Phalcon_CLI_Dispatcher_e873d1c91c6aab104a52a4c3fc6ae98e}%

%{Phalcon_CLI_Dispatcher_5373fd4b144517732fdcdd404f1204d4}%

%{Phalcon_CLI_Dispatcher_67b29119d6cdd3af9d6effee3beb4644}%

%{Phalcon_CLI_Dispatcher_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_CLI_Dispatcher_5c2606a8d795c17abb4b48c5433ddad7}%

%{Phalcon_CLI_Dispatcher_1f44d37c37f8351ebd99aed1dbea6d5a}%

%{Phalcon_CLI_Dispatcher_ca6820ca4c1425cbc40c00d50435beb3}%

%{Phalcon_CLI_Dispatcher_10ab5b359d36535738495fa32463ff56}%

%{Phalcon_CLI_Dispatcher_a0c385170bc790376eb52b2db952bd80}%

%{Phalcon_CLI_Dispatcher_e4b7ef06c210834b07405e848979f0c2}%

%{Phalcon_CLI_Dispatcher_11e24802090bba8a329205391b23c0aa}%

%{Phalcon_CLI_Dispatcher_ff7febdd03ffa3067d5635b38f8066f7}%

%{Phalcon_CLI_Dispatcher_5d2d77ecf5349aa4a057808cb20930f9}%

%{Phalcon_CLI_Dispatcher_97a65c0cb9437713ec6d35a2c93d6395}%

%{Phalcon_CLI_Dispatcher_d242f66485f313e18333a2998bcff2d8}%

%{Phalcon_CLI_Dispatcher_1929c78462d1eb20c4f1fcd0a4e2f55c}%

%{Phalcon_CLI_Dispatcher_8b2d1fb2676e50203c253f6df0430ed4}%

%{Phalcon_CLI_Dispatcher_14203f6d3c0bb1c7c639aa766749bf08}%

%{Phalcon_CLI_Dispatcher_185e8fb64dc1557fd158e8d016a352e4|:doc:`Phalcon\\CLI\\Task <Phalcon_CLI_Task>`}%

%{Phalcon_CLI_Dispatcher_479990e37653af4f91d91d68bf5a2624}%

%{Phalcon_CLI_Dispatcher_bd45b4b0794932bb72512a9ab72983be|:doc:`Phalcon\\CLI\\Task <Phalcon_CLI_Task>`}%

%{Phalcon_CLI_Dispatcher_22c12e3edf28137fa22bee5ae266d9b4}%

%{Phalcon_CLI_Dispatcher_d61bb432cae8f72a79f397ea23a42547}%

%{Phalcon_CLI_Dispatcher_55cb5e91cd2440fb655c4beefe8015a9}%

%{Phalcon_CLI_Dispatcher_0f2dc197150589f7ad5b69b9ecd732bb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Dispatcher_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_CLI_Dispatcher_df6decee5ead80aa0cb332242b400526|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_CLI_Dispatcher_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_CLI_Dispatcher_e45a6cb914180ba71f2ce1f8377f9df3|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_CLI_Dispatcher_8d848637cfda8b4cc05eedcec9549316}%

%{Phalcon_CLI_Dispatcher_d85797a2f7460ae3b96b8e89c9079288|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_CLI_Dispatcher_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_CLI_Dispatcher_a8c4b0e4a313525701160fa3dd8197cf}%

%{Phalcon_CLI_Dispatcher_9738538c9d8633d2e28e78c7241d43fc}%

%{Phalcon_CLI_Dispatcher_ffa8a7153dc10b65ca086b4ba74a16c5}%

%{Phalcon_CLI_Dispatcher_c3fd1ddfb7b1ac557e96de064bef65c4}%

%{Phalcon_CLI_Dispatcher_91fba42d02588b8f121ef046fb80cc25}%

%{Phalcon_CLI_Dispatcher_f95b3227d10ff5b60b9e79c4a1730a90}%

%{Phalcon_CLI_Dispatcher_8786c55453e49f18ad6f94dc73119978}%

%{Phalcon_CLI_Dispatcher_2d882d6e8c0d49a6eedb3f517d4cf9b0}%

%{Phalcon_CLI_Dispatcher_901d3df69bfe8f33c68e82a0ef57122a}%

%{Phalcon_CLI_Dispatcher_98e7f3c851b83e681a8cfb1deca52482}%

%{Phalcon_CLI_Dispatcher_f2ce546cbf08bb88ec91fd8ca6603d30}%

%{Phalcon_CLI_Dispatcher_3463b3195e6aa105e172c3d03d03fcbe}%

%{Phalcon_CLI_Dispatcher_02d397ff5ddb0470919be938cf642b26}%

%{Phalcon_CLI_Dispatcher_991cdf375ddb2dae5042bb86bc84464e}%

%{Phalcon_CLI_Dispatcher_a840ea0d68719db6b43dabcc61c1aa16}%

%{Phalcon_CLI_Dispatcher_2c676539e85e0e19da7b13c5801ba8a2}%

%{Phalcon_CLI_Dispatcher_e499cf843f2ebf492f7a2639e3d29baf}%

%{Phalcon_CLI_Dispatcher_8a967fd800b0844321d914a97edb430d}%

%{Phalcon_CLI_Dispatcher_25b075aac35ca5223a99e5d5ada7d87f}%

%{Phalcon_CLI_Dispatcher_9650e6996268a93907bd15bf21b942cc}%

%{Phalcon_CLI_Dispatcher_1d013d976d03d9f2ee548cea51d9e1d9}%

%{Phalcon_CLI_Dispatcher_2b32732a49098826c9a56c09bb082415}%

%{Phalcon_CLI_Dispatcher_8b248790aeb1414f70c2250f5dfeab1d}%

%{Phalcon_CLI_Dispatcher_350fa8429abc55e22b423ce8c779a81a}%

%{Phalcon_CLI_Dispatcher_7739d32c6e50cad1100e9c5e19188d83}%

%{Phalcon_CLI_Dispatcher_7db2670545db59e604902d37b025e571}%

%{Phalcon_CLI_Dispatcher_760c43b7764946217243686eae77e614}%

%{Phalcon_CLI_Dispatcher_04c89e539e998d89ebdf16e24422c3d1}%

%{Phalcon_CLI_Dispatcher_4b993ca8cca107eb491f6a4139228698}%

%{Phalcon_CLI_Dispatcher_021239bbaadf601baf6438ab30e5d9bb}%

%{Phalcon_CLI_Dispatcher_b526d63547e233db8802cff51c618754}%

%{Phalcon_CLI_Dispatcher_c595fd6a180f9acc31cde9a64243f8b4}%

%{Phalcon_CLI_Dispatcher_a199ebbd81f5a6897328dd6f796cb6bf}%

%{Phalcon_CLI_Dispatcher_21b42a0f130610f514a2233b2607901b}%

%{Phalcon_CLI_Dispatcher_b4d85cb5964538f001cb8f2dde18b22a}%

%{Phalcon_CLI_Dispatcher_e7ea1cc26cdf6fd3cf02bb4d111db443}%

%{Phalcon_CLI_Dispatcher_3734b1afbdd0b70f2f547aea42b3990b}%

%{Phalcon_CLI_Dispatcher_6f7e0b27a09591425a1436f9d7847a7c}%

%{Phalcon_CLI_Dispatcher_e2e48d3688b8d21ae7d43566757a5175}%

%{Phalcon_CLI_Dispatcher_0c207e7839fbd02476c700ddcd9e0556}%

.. code-block:: php

    <?php

      $this->dispatcher->forward(array('controller' => 'posts', 'action' => 'index'));





%{Phalcon_CLI_Dispatcher_97b1ff3a13db289afcf472157e587dc0}%

%{Phalcon_CLI_Dispatcher_b030b040fe2839b6721cf7d05c33d291}%

%{Phalcon_CLI_Dispatcher_bf5ca99d80ff63ea6123490d7c4e0be4}%

%{Phalcon_CLI_Dispatcher_e1945211cc7917bd7c3ef1c0c3c9f70a}%

