%{Phalcon_Mvc_Model_MetaData_Apc_40c53e081397508029083e83cb753b2b}%
============================================

%{Phalcon_Mvc_Model_MetaData_Apc_b817bec18bee1c0cb4e1495700d319a4|:doc:`Phalcon\\Mvc\\Model\\MetaData <Phalcon_Mvc_Model_MetaData>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_242af2347f51a3fd486fb0fb68153f5f|:doc:`Phalcon\\Mvc\\Model\\MetaDataInterface <Phalcon_Mvc_Model_MetaDataInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_6f8d5cfa4e4d1441bf18134b70764cd3}%

.. code-block:: php

    <?php

    $metaData = new Phalcon\Mvc\Model\Metadata\Apc(array(
    	'prefix' => 'my-app-id',
    	'lifetime' => 86400
    ));




%{Phalcon_Mvc_Model_MetaData_Apc_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Model_MetaData_Apc_ad74f2d914ee4a573eec52582659a420}%

%{Phalcon_Mvc_Model_MetaData_Apc_e0abd1b7adcd4cc1057ed137c2f09a3e}%

%{Phalcon_Mvc_Model_MetaData_Apc_28b1a820027c8d5238f38ff11cc743a7}%

%{Phalcon_Mvc_Model_MetaData_Apc_3db843532681e0aad64637c2cc33a7d6}%

%{Phalcon_Mvc_Model_MetaData_Apc_176fe76a0e9dfb8cada40cf9d14c8146}%

%{Phalcon_Mvc_Model_MetaData_Apc_1a97eea73ea575ee39d6134df260ed6c}%

%{Phalcon_Mvc_Model_MetaData_Apc_0eac75a1c8f4702eba89299182a2b9ae}%

%{Phalcon_Mvc_Model_MetaData_Apc_f1ed350772a37234fb81e24ffeee6745}%

%{Phalcon_Mvc_Model_MetaData_Apc_1f4890c5be487acdc7064d301c39b7bc}%

%{Phalcon_Mvc_Model_MetaData_Apc_5157b25eb7b88c5de365e8ec9357f7a8}%

%{Phalcon_Mvc_Model_MetaData_Apc_3d051e46f6507a13a2a95dd7608ad974}%

%{Phalcon_Mvc_Model_MetaData_Apc_54a690e551309d1445f1ddcbd58e77d2}%

%{Phalcon_Mvc_Model_MetaData_Apc_cf3c58249188cda629b3e0c33cb9d1d4}%

%{Phalcon_Mvc_Model_MetaData_Apc_f8d0e8f928e50d247123550a6fd035ec}%

%{Phalcon_Mvc_Model_MetaData_Apc_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_MetaData_Apc_38d168e0be68a8fb5da8d35b168503c0}%

%{Phalcon_Mvc_Model_MetaData_Apc_d2d680774b64ffc709ca21da2db23dca}%

%{Phalcon_Mvc_Model_MetaData_Apc_d8fee8e43ea457546cad3dd18aed7b64}%

%{Phalcon_Mvc_Model_MetaData_Apc_3eb183b01d4ac957d5afd78deaf93bbc}%

%{Phalcon_Mvc_Model_MetaData_Apc_8ae93deb83b4690c0e456fa698839aa1}%

%{Phalcon_Mvc_Model_MetaData_Apc_28f113526895f41353bd226e91100253}%

%{Phalcon_Mvc_Model_MetaData_Apc_439af7c78dd5d7550c7639ff961b7d62}%

%{Phalcon_Mvc_Model_MetaData_Apc_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Mvc_Model_MetaData_Apc_3500231aed9965576e532e7c4fd980d3}%

%{Phalcon_Mvc_Model_MetaData_Apc_f116958721070e69de6fc08feebcbeb4}%

%{Phalcon_Mvc_Model_MetaData_Apc_cce44763aa9316221a11cf62545212f7|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Model_MetaData_Apc_244818319ef5aa2a0c15ec3e2650d66f|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Model_MetaData_Apc_c84cf2a95e1cceaec28243d28c6c0feb|:doc:`Phalcon\\Mvc\\Model\\MetaData\\Strategy\\Introspection <Phalcon_Mvc_Model_MetaData_Strategy_Introspection>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_9f44824bd88973bbaf2ad18a8fd175d0}%

%{Phalcon_Mvc_Model_MetaData_Apc_b8379d4e797082b5cf8abc036b4e480e|:doc:`Phalcon\\Mvc\\Model\\MetaData\\Strategy\\Introspection <Phalcon_Mvc_Model_MetaData_Strategy_Introspection>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_f3cf9b9c53d8dda002b7d1f6b19a6fd0}%

%{Phalcon_Mvc_Model_MetaData_Apc_3058038e6abf553aa1e60de2b34e29ff|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_04ab3007cd49e331a1f1693f71b788ac}%

.. code-block:: php

    <?php

    print_r($metaData->readMetaData(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_c4ec93ae32949da2ff2076c072146542|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_e5d94be7443e2f236056aa4f381b0f37}%

.. code-block:: php

    <?php

    print_r($metaData->writeColumnMapIndex(new Robots(), MetaData::MODELS_REVERSE_COLUMN_MAP, array('leName' => 'name')));





%{Phalcon_Mvc_Model_MetaData_Apc_64c1546d51b7c8e358823c59c474f199|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_e3314861684e59667de64a6cbf16388a}%

.. code-block:: php

    <?php

    print_r($metaData->writeColumnMapIndex(new Robots(), MetaData::MODELS_REVERSE_COLUMN_MAP, array('leName' => 'name')));





%{Phalcon_Mvc_Model_MetaData_Apc_af8a9720d62b87bf0d1fdc9068e438e0|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_6c469653b533736665a502e3ac99c0f2}%

.. code-block:: php

    <?php

    print_r($metaData->readColumnMap(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_8134490bf6be9616ff1f29371dd945ab|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_6bc29a64f51f3b01f6e09945094c0244}%

.. code-block:: php

    <?php

    print_r($metaData->readColumnMapIndex(new Robots(), MetaData::MODELS_REVERSE_COLUMN_MAP));





%{Phalcon_Mvc_Model_MetaData_Apc_97de906125b2003fb35b8fa9b0345448|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_c1d0805362967869500f1a68a1e7a766}%

.. code-block:: php

    <?php

    print_r($metaData->getAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_1a7ab1434ca60ccd31fcb9424f454dbd|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_e46d0b16f50db6ab0a82ce2c3409182b}%

.. code-block:: php

    <?php

    print_r($metaData->getPrimaryKeyAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_14fca075acacfbc4cd83fdcc6889dab0|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_783a49534b7a705cefa15dd43fe05687}%

.. code-block:: php

    <?php

    print_r($metaData->getNonPrimaryKeyAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_a014d452c74508314e0e517b164e4b31|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_e128f6f6423fdd9dd0e0cec9a4dc42b9}%

.. code-block:: php

    <?php

    print_r($metaData->getNotNullAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_826cd558a03f6d6c1a55e0ce89bcce17|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_95d0e1d9d682e081c9761d44f389e786}%

.. code-block:: php

    <?php

    print_r($metaData->getDataTypes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_9e7cba94584f247818b5856697f4cd2f|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_cf9b62f97fa668ee794b0fdf95c90911}%

.. code-block:: php

    <?php

    print_r($metaData->getDataTypesNumeric(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_dbf8fabe27b75348497bca4f7c75c582|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_2577ec492ffa5dabeae9628446e067cf}%

.. code-block:: php

    <?php

    print_r($metaData->getIdentityField(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_e4d014581ea3b45aa93dd2632dc8ce36|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_311436393b211aa970fa1a73ce6e0073}%

.. code-block:: php

    <?php

    print_r($metaData->getBindTypes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_e9eac9ee45e7c2869d2087d91d540c1c|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_ed25be665841fe536b7769da498ab68a}%

.. code-block:: php

    <?php

    print_r($metaData->getAutomaticCreateAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_4fe0509ea5fda1acec73074425b8d87a|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_7f8b5c27d46336c0c7e0592b2ff85320}%

.. code-block:: php

    <?php

    print_r($metaData->getAutomaticUpdateAttributes(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_dcd42e302d084331294afdbbb12d8197|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_28120b008f1551a44aa2b6a76d818ffb}%

.. code-block:: php

    <?php

    $metaData->setAutomaticCreateAttributes(new Robots(), array('created_at' => true));





%{Phalcon_Mvc_Model_MetaData_Apc_85d60bd4f0864bd21c90d8e9af4bccd0|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_376dcecabec9d3b30745082e3be09c88}%

.. code-block:: php

    <?php

    $metaData->setAutomaticUpdateAttributes(new Robots(), array('modified_at' => true));





%{Phalcon_Mvc_Model_MetaData_Apc_e3c5bbf0d49b587d020ad556375b335f|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_308d8e4032f381676635193d983802a0}%

.. code-block:: php

    <?php

    print_r($metaData->getColumnMap(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_1c6082a5f0f4820d9bc7e19d7ee1abd8|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_320eda8c5c16b03a7f7d581fc9501921}%

.. code-block:: php

    <?php

    print_r($metaData->getReverseColumnMap(new Robots()));





%{Phalcon_Mvc_Model_MetaData_Apc_5e84c7c14b510d96d71f5f034a886b38|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_MetaData_Apc_cdc7683d6bfc852fd0564914abd270be}%

.. code-block:: php

    <?php

    var_dump($metaData->hasAttribute(new Robots(), 'name'));





%{Phalcon_Mvc_Model_MetaData_Apc_f0dd755d7911814b74fb4bef7685b90f}%

%{Phalcon_Mvc_Model_MetaData_Apc_dbb247008c6c3f3cd3b03f52e537ed38}%

