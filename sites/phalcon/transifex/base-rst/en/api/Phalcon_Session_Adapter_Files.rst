%{Phalcon_Session_Adapter_Files_8219e6a9b423354865e686ac160d76f9}%
==========================================

%{Phalcon_Session_Adapter_Files_c501d852268e49722dedb2a63b3b9920|:doc:`Phalcon\\Session\\Adapter <Phalcon_Session_Adapter>`}%

%{Phalcon_Session_Adapter_Files_6ba787724aad5e9855330c54665297c3|:doc:`Phalcon\\Session\\AdapterInterface <Phalcon_Session_AdapterInterface>`}%

%{Phalcon_Session_Adapter_Files_74ace594b76a473ad66917e6098996ec}%

.. code-block:: php

    <?php

     $session = new Phalcon\Session\Adapter\Files(array(
        'uniqueId' => 'my-private-app'
     ));
    
     $session->start();
    
     $session->set('var', 'some-value');
    
     echo $session->get('var');




%{Phalcon_Session_Adapter_Files_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Session_Adapter_Files_ec9dcc45fc8331482a478e33a6859654}%

%{Phalcon_Session_Adapter_Files_d936b77f9b5c6a0db4eb07f2649f0edf}%

%{Phalcon_Session_Adapter_Files_e5df96dbb211378f03b50016813f1975}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_baf30ffdaca7271f9a3db11195fd1562}%

%{Phalcon_Session_Adapter_Files_e8c06940719da5c629a41b5a5f43b4b1}%

%{Phalcon_Session_Adapter_Files_8da9a1eba1840a211174777c30c63cd8}%

%{Phalcon_Session_Adapter_Files_b62d2bd5ef8cd029adf996cbc7c8e980}%

.. code-block:: php

    <?php

    $session->setOptions(array(
    	'uniqueId' => 'my-private-app'
    ));





%{Phalcon_Session_Adapter_Files_f4c265aa1db59c076a4b97175ef84ddd}%

%{Phalcon_Session_Adapter_Files_5ddbfa842231257606ca4fdaecf60ffd}%

%{Phalcon_Session_Adapter_Files_d1d48ddf3f83812432c420f3c21786d5}%

%{Phalcon_Session_Adapter_Files_523152e8fe94432d635294daaa0edc15}%

%{Phalcon_Session_Adapter_Files_9ff375d52120c68962b6813bd8f00ca9}%

%{Phalcon_Session_Adapter_Files_18e84ad546d026883951245b0db9e254}%

.. code-block:: php

    <?php

    $session->set('auth', 'yes');





%{Phalcon_Session_Adapter_Files_f1948f4133ca8f83363ae5abdd207be0}%

%{Phalcon_Session_Adapter_Files_28faca74b7da1a87deeca1ac7262d7d7}%

.. code-block:: php

    <?php

    var_dump($session->has('auth'));





%{Phalcon_Session_Adapter_Files_becfe0e3193f6c69b344211e76a8c4e8}%

%{Phalcon_Session_Adapter_Files_039ab89230cc3863b7513a1cd3bc0782}%

.. code-block:: php

    <?php

    $session->remove('auth');





%{Phalcon_Session_Adapter_Files_87c76d0b9028eda74bf78fa56f92571a}%

%{Phalcon_Session_Adapter_Files_dfe810a835e75dbc2161344390d6b06b}%

.. code-block:: php

    <?php

    echo $session->getId();





%{Phalcon_Session_Adapter_Files_92015fc35bd8baed6803cded9ba1d315}%

%{Phalcon_Session_Adapter_Files_e395074048c4ddeb356b2b4b80c0293b}%

.. code-block:: php

    <?php

    var_dump($session->isStarted());





%{Phalcon_Session_Adapter_Files_b688174a70f7f0e6461ac614a3f827ae}%

%{Phalcon_Session_Adapter_Files_931e67f27415f11f2fe5ac39b9b7a849}%

.. code-block:: php

    <?php

    var_dump($session->destroy());





%{Phalcon_Session_Adapter_Files_e131070dde4f118e22847095bd4e7586}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_7753bf0533a3c7209665569fbf6e0603}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_0984c1b5e13e447ed29a0e545845dd23}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_012e250aa1e2f1156417ae468ea51bbd}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_4ccd066d8a0a04da2e897f783734924c}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_c985f7860b66e9f10dc30bceada8d2d1}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_2a37007ca6ad0435dbf81231b027ff23}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_769cef11d8e2915ab42903ba62e9722d}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_0f9fa658a2c309a694f13abb6d4cd780}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_8164ed2532a3080caeeeb84b0b7f56ed}%

%{Phalcon_Session_Adapter_Files_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Adapter_Files_7eece78beeb4530a55cd9f5a91847c5a}%

%{Phalcon_Session_Adapter_Files_5298b5913d6b953ed35394a5baf0eae6}%

