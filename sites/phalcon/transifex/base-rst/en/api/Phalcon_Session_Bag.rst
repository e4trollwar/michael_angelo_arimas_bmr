%{Phalcon_Session_Bag_d88e2e1212dd947879c2296261062048}%
===============================

%{Phalcon_Session_Bag_3fb9b97076cf40b135e721d09578fd4b|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Session\\BagInterface <Phalcon_Session_BagInterface>`}%

%{Phalcon_Session_Bag_b779d686a75aa97f1db492d31b7e82d9}%

.. code-block:: php

    <?php

    $user = new \Phalcon\Session\Bag('user');
    $user->name = "Kimbra Johnson";
    $user->age = 22;




%{Phalcon_Session_Bag_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Session_Bag_f782e69295a146e8e708478cfcbd2b62}%

%{Phalcon_Session_Bag_1b4235ddae09b74b596dab8f045b1538}%

%{Phalcon_Session_Bag_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Session_Bag_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Session_Bag_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Session_Bag_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Session_Bag_5da54904dc5c062f0acea9ca0026f99b}%

%{Phalcon_Session_Bag_fea02f6d2991c36f43fbe2ad85ac93ed}%

%{Phalcon_Session_Bag_5a452ef43f018ba13df8d31c38abbf25}%

%{Phalcon_Session_Bag_9c8c1d972cb827f4af6d40568d8eb87a}%

.. code-block:: php

    <?php

     $user->destroy();





%{Phalcon_Session_Bag_be9755b0dfed610232137745db12f39b}%

%{Phalcon_Session_Bag_f6c62c506d04e1cb2fa6674fc49973a5}%

.. code-block:: php

    <?php

     $user->set('name', 'Kimbra');





%{Phalcon_Session_Bag_3690621f0530cf630eb57fe6d7830ffe}%

%{Phalcon_Session_Bag_b1456bad53a73ecdc9fa8b2a0af3f596}%

.. code-block:: php

    <?php

     echo $user->get('name', 'Kimbra');





%{Phalcon_Session_Bag_a3c9a0500a72c7f97963b5e647ac00e1}%

%{Phalcon_Session_Bag_96df6d35db0f51967e865cb272bea15e}%

.. code-block:: php

    <?php

     var_dump($user->has('name'));





%{Phalcon_Session_Bag_927a3f7fbfd10a52ce6315062f17a7c0}%

%{Phalcon_Session_Bag_47f0d49f5c0ad4e88a55ad0514f253d3}%

.. code-block:: php

    <?php

     $user->remove('name');





%{Phalcon_Session_Bag_67283030de138d8819365798f72e21e3}%

%{Phalcon_Session_Bag_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Bag_d7c0a486d12041592a6076776f6a22b1}%

%{Phalcon_Session_Bag_faf1e2d6d5ae7621ee44cc3641219168}%

.. code-block:: php

    <?php

     echo $user->name;





%{Phalcon_Session_Bag_0d67728ad17093e37eab38a7ac5ad200}%

%{Phalcon_Session_Bag_61704c81000f87c01857fd195cc48640}%

.. code-block:: php

    <?php

     $user->name = "Kimbra";





%{Phalcon_Session_Bag_e8c380be30408d8c1977e2cebf74631d}%

%{Phalcon_Session_Bag_5f0ba9ae11490fcfecf404d94dc9dd92}%

.. code-block:: php

    <?php

     var_dump(isset($user['name']));





%{Phalcon_Session_Bag_f7420510728e1bf57eebdce9842dd0d7}%

%{Phalcon_Session_Bag_985a2826611527e9eb78c28959f1bbef}%

.. code-block:: php

    <?php

     unset($user['name']);





%{Phalcon_Session_Bag_f5a043ff4cdbf7e308abd0d2e36ff6cb}%

%{Phalcon_Session_Bag_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Bag_2f89883b4ea5702517b8537bed8db873}%

%{Phalcon_Session_Bag_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Bag_9ab68bc06a3129747a4a59466efb3f27}%

%{Phalcon_Session_Bag_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Bag_3644e01a19440baa19219140adc68825}%

%{Phalcon_Session_Bag_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Session_Bag_dac5cec50eda033a74b25dcc1b83e1ba}%

%{Phalcon_Session_Bag_68cf0a3bff1e41b907cf955f570c5249}%

