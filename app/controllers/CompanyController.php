<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\Model\Query;

class CompanyController extends Controller
{
	public function initialize(){
		$this->view->setTemplateAfter('template');
	}

	public function session(){
		if(!isset($_SESSION["admin"])){
			header('Location: ../login');
		}
	}


	public function addcompanyAction()
	{
		// $this->session();
		$info = json_decode(file_get_contents("php://input"));
		$this->view->setVar('header_title', "Add New Company");
		if(count($info) > 0) {
		 	//VARIABLE
			$compname = $info->compname;
			$address =  $info->address;
			$contact =  $info->contact;
		 	//ADD SAVE NEW DATA ENTRY
			$add = new Company();
			$add->compname 	= $compname;
			$add->address 	= $address;
			$add->contact 	= $contact;

			if($compname  !="" && $address!=""&& $contact!=""){
				$add->save();

			}else{
				return false;
			}
			
		}
	}

	public function editcompanyojtAction($id){
		$info = json_decode(file_get_contents("php://input"));
		$this->view->setVar('header_title', "Edit Company Info");
	

	 $result=$this->db->query("SELECT * FROM company where id='$id' ");
                        
	    while($row=$result->fetchArray())
	    {
	       $this->view->id= $row['id'];
	       $this->view->compname= $row['compname'];
	       $this->view->address= $row['address'];
	       $this->view->contact= $row['contact'];
	    }
						

		if(count($info) > 0) {
		 	//VARIABLE
		
			$compname = $info->compname;
			$address =  $info->address;
			$contact =  $info->contact;
			$aa = $info->id;
		 	//ADD SAVE NEW DATA ENTRY

			if($compname  !="" && $address!=""&& $contact!=""){
				$this->db->query("UPDATE company SET compname='$compname',
													 address='$address',
													 contact='$contact'
													 WHERE id='".$aa."'  ");
				

			}else{
				return false;
			}
			
		}
	}





	public function editcompanyAction($id)
	{
		
		$this->view->setVar('header_title', "Edit Comapny Info");
		$company = Company::find(array("id='".$id."'"));
		$this->view->companyname= $company[0]->campname;
		$this->view->address= $company[0]->address ;
		$this->view->contact= $company[0]->contact ;

		if ($this->request->isPost('update') == true) {
		 	//VARIABLE
			$compname= $this->request->getPost('compname');
			$address= $this->request->getPost('address') ;
			$contact= $this->request->getPost('contact') ;
		 	//ADD SAVE NEW DATA ENTRY
			$add = Company::findFirst('id='.$id.'');
			$add->campname 	= $compname;
			$add->address 	= $address;
			$add->contact 	= $contact;

			if ($add->save() == false) {
				echo "Umh, We can store data: ";
				foreach ($robot->getMessages() as $message) {
					echo $message;
				}
			} else {
				 header('Location: ../setproductprice/'.$id.'');
			}
		}
		
	}

	public function complistAction()
	{
		

		$this->view->setVar('header_title', "Client Company List");

		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
		// The data set to paginate
		$company      = Company::find();

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator   = new PaginatorModel(
			array(
				"data"  => $company,
				"limit" => 5,
				"page"  => $currentPage
				)
			);

		// Get the paginated results
		$this->view->page= $paginator->getPaginate();

	}

	public function compprofileAction($id)
	{
		$this->view->setVar('header_title', "Company Profile");
		$this->view->id= $id;

		$company   = Company::find(array("id='".$id."'"));
		$this->view->compname= $company[0]->compname;
		$this->view->address= $address[0]->address;
		$this->view->contact= $contact[0]->contact;

		////////////////////////LIST OWNED TRUCKS
		if(!isset($_GET["page"])){
			$currentPage=0;
		}else{
			$currentPage = (int) $_GET["page"];
		}
        


	}

	public function orderedhistoryAction($id)
	{
		$this->view->setVar('header_title', "Company Profile");
		$this->view->compid= $id;

		$company      = Company::find(array("id='".$id."'"));
		$this->view->companyname= $company[0]->campname;
		$this->view->address= $company[0]->address ;
		$this->view->contact= $company[0]->contact ;

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /////DAILY REPORT
         $this->view->from ="";//DISPLAY VALUE from********************
         $this->view->to ="";//DISPLAY VALUE from********************
         $this->view->dspval ="";//DISPLAY VALUE from********************
         $this->view->disabled = "disabled" ;//DISABLED BOTTN********************
         $this->view->note = "" ;//Notification********************
         if ($this->request->isPost('showrpt') == true) {
     		 
         	$from= $this->request->getPost('from');//POST from
		 	$this->view->from = $from ;//DISPLAY VALUE from********************
		 	$to= $this->request->getPost('to');//POST to
		 	$this->view->to = $to ;//DISPLAY VALUE to********************

		 	 $this->view->dspval ="displaydaily";//DISPLAY VALUE from********************

		 	// echo $from ."<br>". $to."<br>";

		 	if($from>$to){
		 		$this->view->note = "To Date Must Be Greater than From Date" ;
		 		$this->view->dborder= json_decode(json_encode($data_orders=array()));
		 	}else{
		 		$this->view->disabled = "";
		 		$query  = $this->modelsManager->createQuery("SELECT * FROM orders WHERE orders.company='$id' and orders.date >= '$from' and orders.date <= '$to'");
				$orderdate   = $query->execute();
				

		 		// $orderdate=Orders::find(array($condition));
		 		// // echo count($orderdate);
		 		foreach ( $orderdate as $daily){
		 			$data_orders[] = array(
					'plateno'=> $daily->plateno ,
					'company' => $daily->company,
					'sand' => $daily->sand,
					'cumtr' => $daily->cumtr,
					'pricecumtr' => $daily->pricecumtr,
					'cash'=> $daily->cash ,
					'poamount'=> $daily->poamount ,
					'discount'=> $daily->discount ,
					'drno'=> $daily->drno ,
					'time'=> $daily->time ,
					'date'=> $daily->date ,				);
		 		}
		 		$this->view->dborder= json_decode(json_encode($data_orders));
		 	}

         }else{
         		
         	$this->view->dborder= json_decode(json_encode($data_orders=array()));
         }
	}


	public function setproductpriceAction($id)
	{
		$this->view->setVar('header_title', "Company Profile");
		$this->view->compid= $id;

		$company      = Company::find(array("id='".$id."'"));
		$this->view->companyname= $company[0]->campname;
		$this->view->address= $company[0]->address ;
		$this->view->contact= $company[0]->contact ;

		////////////////////////LIST PRODUCT
		$currentsetdate=Setpricedate::find();/////////////////////////////////////
		foreach ($currentsetdate as $price) {
			$curdate =  $price->date;
			
		}
		$sand   = Sand::find(array("date='".$curdate."'"));
		foreach ($sand as $m) {
			$data[] = array(
				'id' => $m->id ,
				'sandtype' => $m->sandtype,
				'sandcateg' => $m->sandcateg,
				'price' => $m->price,
				);
		}
		$this->view->sand= json_decode(json_encode($data));

		///////////////////////////////////////////////////
		function display($model,$column){
			$sand = $model::find();
			$data = array();
			foreach ($sand as $m) {
				$data[] = array(
					'id' => $m->id ,
					$column => $m->$column,
					);
			}
			return json_decode(json_encode($data));
		}

		function setprice($id){
			$setprice = Setprice::find(array("company='".$id."'"));
			$data = array();
			foreach ($setprice as $m) {
				$data[] = array(
					"sand"=> $m->sand,
					"setprice"=> $m->setprice,
					);
			}
			return json_decode(json_encode($data));
		}

		$this->view->data_type = display("Sandtype","type"); //LIST SAND TYPE
		$this->view->data_categ = display("Sandcateg","category"); //LIST SAND CATEGORY
		$this->view->set_price= setprice($id); //LIST SAND CATEGORY





		/////////////////////////////////////////////////SAVE
		if ($this->request->isPost('save') == true) {
			$checked_count = count($_POST['sandid']);
			$sandid = $_POST['sandid'];

			$price = $_POST['price'];

			for($i=0;$i<=$checked_count;$i++){


				if(@$price[$i]!=""){
		 			//Check If Exist
					$setprice= Setprice::find(array("company='".$id."' and  sand='".@$sandid[$i]."'"));
					if(count($setprice)==0){

						$add = new Setprice();
						$add->company 	= $id;
						$add->sand 		= @$sandid[$i];
						$add->setprice 	= @$price[$i];

						if ($add->save() == false) {
							echo "Umh, We can store data: ";
							foreach ($add->getMessages() as $message) {
								echo $message;
							}
						} else {
							echo "Great, a new data was saved successfully!";
						}
					}else{

						$update = Setprice::findFirst(array("company='".$id."' and  sand='".@$sandid[$i]."'"));
						$update->setprice = @$price[$i];

						if ($update->save() == false) {
							echo "Umh, We can store data: ";
							foreach ($update->getMessages() as $message) {
								echo $message;
							}
						} else {
							echo "Great, a new data was saved successfully!";
						}
					}
				}
			}
		}
	}
	public function dltcompanyAction($id)
	{


		$info = json_decode(file_get_contents("php://input"));

		$this->view->setVar('header_title', "Company Profile");
		 $result=$this->db->query("SELECT * FROM company where id='$id' ");
                        
	    while($row=$result->fetchArray())
	    {
	       $this->view->id= $row['id'];
	       $this->view->compname= $row['compname'];
	       $this->view->address= $row['address'];
	       $this->view->contact= $row['contact'];
	    }

		if(count($info) > 0) {
		 	//VARIABLE
		
			$compname = $info->compname;
			$address =  $info->address;
			$contact =  $info->contact;
			$aa = $info->id;
		 	//ADD SAVE NEW DATA ENTRY

			$this->db->query("DELETE FROM company 
								WHERE id='$aa'  ");
			
		
	}
	



		
	}
	

}
