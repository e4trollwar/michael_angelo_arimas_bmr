%{Phalcon_Mvc_Router_Group_0ab3e3eef9a48ed97f113bd23ea6a9eb}%
=====================================

%{Phalcon_Mvc_Router_Group_cbdf0431b92139e1500a813b22bf149a}%

.. code-block:: php

    <?php

     $router = new Phalcon\Mvc\Router();
    
     //{%Phalcon_Mvc_Router_Group_d3dbf364456e79dff31c012172d8aa25%}
     $blog = new Phalcon\Mvc\Router\Group(array(
     	'module' => 'blog',
     	'controller' => 'index'
     ));
    
     //{%Phalcon_Mvc_Router_Group_43735061c13b24a7f591a1c8b8137f0e%}
     $blog->setPrefix('/blog');
    
     //{%Phalcon_Mvc_Router_Group_5a86a91ac4dd449f3cc00d7b3f775ec4%}
     $blog->add('/save', array(
     	'action' => 'save'
     ));
    
     //{%Phalcon_Mvc_Router_Group_7aa3c893d6c37ebe73797b11b3af0e1d%}
     $blog->add('/edit/{id}', array(
     	'action' => 'edit'
     ));
    
     //{%Phalcon_Mvc_Router_Group_82fc3962e12dabb102c57c8fab55dc3f%}
     $blog->add('/blog', array(
     	'controller' => 'about',
     	'action' => 'index'
     ));
    
     //{%Phalcon_Mvc_Router_Group_cf0d7a079879f96295a227eb381c89f0%}
     $router->mount($blog);




%{Phalcon_Mvc_Router_Group_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Router_Group_0548761db84beb293e69b5495bb10a84}%

%{Phalcon_Mvc_Router_Group_170e097e10203a0b300610d8966846d9}%

%{Phalcon_Mvc_Router_Group_a058c97f3d02ab385ca7f457aa00f207|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Group_0e4323524da445bde6b676949a3f2c43}%

%{Phalcon_Mvc_Router_Group_5584a065a26dfa13a62886d261bb78ec}%

%{Phalcon_Mvc_Router_Group_ccd138f7726b9951d3820e5cc17c261d}%

%{Phalcon_Mvc_Router_Group_6db2c14a8bac46109656126c482ebda0|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Group_2b75d13263ceb966f0538fc67499a20c}%

%{Phalcon_Mvc_Router_Group_6b0eaf94d5d7e5c946d8a4a2d0be788f}%

%{Phalcon_Mvc_Router_Group_741de8a19a857fe1301e4c65eb32d6bf}%

%{Phalcon_Mvc_Router_Group_ffb0b4d95bc5ef457576843214b4db41|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Group_e27e2a1863d5835e0ecbedabfea3c186}%

%{Phalcon_Mvc_Router_Group_ec80e788be04a4f89777a171270aadf3}%

%{Phalcon_Mvc_Router_Group_f76b34c10b4f0c780af7cd3285f06d1e}%

%{Phalcon_Mvc_Router_Group_97d5ad64f66b4caf058a1edb5122b86d|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Group_b301f456110561d90ce1b9722d0dabee}%

%{Phalcon_Mvc_Router_Group_3688081b4c6ab76d691f1ab5b08614c1}%

%{Phalcon_Mvc_Router_Group_a47dbb6140654bd0ae8fae61fbc3b8ac}%

%{Phalcon_Mvc_Router_Group_b643b249425e78b6adceb7c7fb154319|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_334f80e344b095936c39bcf741ac4c09}%

%{Phalcon_Mvc_Router_Group_5837b2f310620a1b82f93caa71911788|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_2f619b83100b8b152318d163ac3b5363}%

%{Phalcon_Mvc_Router_Group_0dd860fc275c43dc36c8624798d372b1|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_03a5a69c970efafb011e0fc32a953af6}%

.. code-block:: php

    <?php

     $router->add('/about', 'About::index');





%{Phalcon_Mvc_Router_Group_6e2de8a07449ba21737b3b2879685370|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_f74b99e1e164260f7718bb4f740c0c71}%

%{Phalcon_Mvc_Router_Group_d2085dd45e82f7d07f095d85c9a32c08|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_868af429955c0ecbbaa95f7fa5e67133}%

%{Phalcon_Mvc_Router_Group_c66c44d522d93a64b24e47939dcec2f6|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_f32c97339be876b8e10fb86cbe916acc}%

%{Phalcon_Mvc_Router_Group_4d3ebb4bc095ca4f5d655d86b435bc91|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_4d752b0f335109eb99ebea41e9977cbc}%

%{Phalcon_Mvc_Router_Group_2138534cbfdbaee4ddbb5b1823b928c8|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_1cf981139831fbc2661c7e1ba2532c59}%

%{Phalcon_Mvc_Router_Group_15ce7b896ca7cd705f83372e6b62e50f|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_c5e8d939bd9ee0fae4a2047ea3f17f18}%

%{Phalcon_Mvc_Router_Group_6c81ff964c4890984ea0cf05a8660167|:doc:`Phalcon\\Mvc\\Router\\Route <Phalcon_Mvc_Router_Route>`}%

%{Phalcon_Mvc_Router_Group_58a4300721cf5feeb2c0899372a9c900}%

%{Phalcon_Mvc_Router_Group_d9f1667c8f837f416787af94d5645e96}%

%{Phalcon_Mvc_Router_Group_e9a944524b038eeda5d4cd523610fdd9}%

%{Phalcon_Mvc_Router_Group_4bbb4832a53210d0a8a0973e0ff3ff2d|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Group_90d6720b705e2eca7bab333ee9b666d3}%

%{Phalcon_Mvc_Router_Group_7ebb48ffa46df471fc1df6d28a607bdb}%

%{Phalcon_Mvc_Router_Group_bd61c1d6f9e567f648af334a5d2cd919}%

%{Phalcon_Mvc_Router_Group_274147945c448d6a6634baecf2d6cc8e|:doc:`Phalcon\\Mvc\\Router\\Group <Phalcon_Mvc_Router_Group>`}%

%{Phalcon_Mvc_Router_Group_8bd531ead967dceb5e6e1ac2f5d65b85}%

%{Phalcon_Mvc_Router_Group_bad4450b78127e6c6b52db7413174386}%

%{Phalcon_Mvc_Router_Group_a478b59d515d6cca3cee408ecba33534}%

