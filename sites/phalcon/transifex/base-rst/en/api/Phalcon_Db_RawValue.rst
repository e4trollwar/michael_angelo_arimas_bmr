%{Phalcon_Db_RawValue_5a39d75da1f46eaedeb14d816177fffb}%
===============================

%{Phalcon_Db_RawValue_fec9af78ea3e992c3c188c31b4d201a7}%

.. code-block:: php

    <?php

    $subscriber = new Subscribers();
    $subscriber->email = 'andres@phalconphp.com';
    $subscriber->created_at = new Phalcon\Db\RawValue('now()');
    $subscriber->save();




%{Phalcon_Db_RawValue_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_RawValue_fcde5f5c0c618ba9ef6830e8b707a557}%

%{Phalcon_Db_RawValue_eea6ffc0f2a5d51950a9bde9aab8aa92}%

%{Phalcon_Db_RawValue_af95a6bc76fccbab053ee607427a2eab}%

%{Phalcon_Db_RawValue_9fed4cd845e302ab6b490d53a6b9241e}%

%{Phalcon_Db_RawValue_a3dc58aadbed5af12d3df33b8da1941c}%

%{Phalcon_Db_RawValue_0e2891d73f1d840070b754fc5904904b}%

