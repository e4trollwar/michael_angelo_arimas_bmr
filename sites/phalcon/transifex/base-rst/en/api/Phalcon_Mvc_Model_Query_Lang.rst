%{Phalcon_Mvc_Model_Query_Lang_ad39f45b31060c9495d46834159f9ee2}%
===================================================

%{Phalcon_Mvc_Model_Query_Lang_863410120f82f04a1b0c928113adb294}%

.. code-block:: php

    <?php

     $intermediate = Phalcon\Mvc\Model\Query\Lang::parsePHQL("SELECT r.* FROM Robots r LIMIT 10");




%{Phalcon_Mvc_Model_Query_Lang_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Query_Lang_242d2f8346e51fc1582447fca9104e49}%

%{Phalcon_Mvc_Model_Query_Lang_83b5938221411ee3a7755d10f35d7aea}%

