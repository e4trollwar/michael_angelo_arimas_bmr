%{Phalcon_Mvc_Model_Resultset_Simple_daebfd2454720d7e00c71311643e4a36}%
================================================

%{Phalcon_Mvc_Model_Resultset_Simple_26f230a14bb3606ce3f99b4964578ce5|:doc:`Phalcon\\Mvc\\Model\\Resultset <Phalcon_Mvc_Model_Resultset>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_0e5bf7a6a72014de32b24fa268988360|:doc:`Phalcon\\Mvc\\Model\\ResultsetInterface <Phalcon_Mvc_Model_ResultsetInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_48468e23c189477a530e67cb7c1e7183}%

%{Phalcon_Mvc_Model_Resultset_Simple_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Mvc_Model_Resultset_Simple_b110d762ef16e0a26e39b1749392c7cc}%

%{Phalcon_Mvc_Model_Resultset_Simple_1334405c9960d60af4d1391cd1aa35ff}%

%{Phalcon_Mvc_Model_Resultset_Simple_55a5e437ff48bc859a9b346789b793a8}%

%{Phalcon_Mvc_Model_Resultset_Simple_3d3f9eff296dfe75e7a9d80248c7acca}%

%{Phalcon_Mvc_Model_Resultset_Simple_d172c6ce9ee5f9392cae87c67ec7d8d8}%

%{Phalcon_Mvc_Model_Resultset_Simple_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Resultset_Simple_d59a32835ef5f33022d6ba4ed7ab1d1a|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`|:doc:`Phalcon\\Db\\Result\\Pdo <Phalcon_Db_Result_Pdo>`|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_d46e152b543a06d625975572745a0e02}%

%{Phalcon_Mvc_Model_Resultset_Simple_93689708a609a9f89674723a8c1f18bd}%

%{Phalcon_Mvc_Model_Resultset_Simple_ad29b3f29d62aeeb7db2ff535139b86a}%

%{Phalcon_Mvc_Model_Resultset_Simple_1ca7d973ccd648724336c77d3a144efb}%

%{Phalcon_Mvc_Model_Resultset_Simple_3533909c6bba964d16657dd0b4b4b198}%

%{Phalcon_Mvc_Model_Resultset_Simple_e4a1eb998f48d9e9518e537dd9bf4090}%

%{Phalcon_Mvc_Model_Resultset_Simple_10e5dfab84184704d2f899db2413b77e}%

%{Phalcon_Mvc_Model_Resultset_Simple_e299d36d06648957330fcf22840f28f9}%

%{Phalcon_Mvc_Model_Resultset_Simple_ee075763e2ce2da226eed1bc77178b50}%

%{Phalcon_Mvc_Model_Resultset_Simple_3324bd7ad836f003ffbb69bfe764f963}%

%{Phalcon_Mvc_Model_Resultset_Simple_0637e194ca8055b68d1862383d6335f8}%

%{Phalcon_Mvc_Model_Resultset_Simple_2f9826a5f5b8f00f13033777e0b28b86}%

%{Phalcon_Mvc_Model_Resultset_Simple_587a0790bfb8c682992164d63484cdc2}%

%{Phalcon_Mvc_Model_Resultset_Simple_936da39b134aca0eb3004d4dcd2cad07}%

%{Phalcon_Mvc_Model_Resultset_Simple_da276b8f032e1bb8ed0a5ab2d42d19aa}%

%{Phalcon_Mvc_Model_Resultset_Simple_60558e35ed1e8b4b0686a573744ddbc1}%

%{Phalcon_Mvc_Model_Resultset_Simple_59caf172c77cbe7767ef9b01f0f59f14}%

%{Phalcon_Mvc_Model_Resultset_Simple_3e23e6b16c3215750115b280dd70604f}%

%{Phalcon_Mvc_Model_Resultset_Simple_9c113a5079ff20ad0bafc23e1b5872b3}%

%{Phalcon_Mvc_Model_Resultset_Simple_7f4d7708c26c8c749bd57f6e9c121673}%

%{Phalcon_Mvc_Model_Resultset_Simple_d4a3553e435c7e74a5b00878654a422c}%

%{Phalcon_Mvc_Model_Resultset_Simple_fa09716e536aa01a1d5071cb0942da2d|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_d349b7c4fed51672ae8a7418498107c1}%

%{Phalcon_Mvc_Model_Resultset_Simple_6f7755ebc4bf9c6fe26c0d713e09cc74|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_ce3ddc01aaa610f5e49a87bc872089e7}%

%{Phalcon_Mvc_Model_Resultset_Simple_91c73021773c25084e582b94b0502957}%

%{Phalcon_Mvc_Model_Resultset_Simple_ce3ddc01aaa610f5e49a87bc872089e7}%

%{Phalcon_Mvc_Model_Resultset_Simple_d8290c8ba18a0f486e0b87dabfacd155}%

%{Phalcon_Mvc_Model_Resultset_Simple_4b657a29293e7e5c2a6fac3729dc1806}%

%{Phalcon_Mvc_Model_Resultset_Simple_69d0c8fc5a607dd257e24c614d6cf72a|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_f49ace34534dc42ef31d0885e4948b27}%

%{Phalcon_Mvc_Model_Resultset_Simple_acce1fc47147e4c38f5e9d61b02d00c0|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_f47a3d422216ccaf8480e2d12484e793}%

%{Phalcon_Mvc_Model_Resultset_Simple_7ed5c1d38f3d49ece10870e16ac6cc01|:doc:`Phalcon\\Mvc\\Model\\Resultset <Phalcon_Mvc_Model_Resultset>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_0cc74caa22d6c02f693257654583a5c1}%

%{Phalcon_Mvc_Model_Resultset_Simple_974d4c48e762d71f37b2dad03ba3a52a}%

%{Phalcon_Mvc_Model_Resultset_Simple_a60dae6e6a9d3048832a81aaa7ad7aca}%

%{Phalcon_Mvc_Model_Resultset_Simple_2571ad629190a8e6835442d870e1c9e2|:doc:`Phalcon\\Mvc\\Model\\Resultset <Phalcon_Mvc_Model_Resultset>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_6bb86d55a16dd81bf45766f15a1a6e94}%

%{Phalcon_Mvc_Model_Resultset_Simple_78905ac2b95886550b81a6e79ae21a3f}%

%{Phalcon_Mvc_Model_Resultset_Simple_d3414d206426516d2132fd030410f370}%

%{Phalcon_Mvc_Model_Resultset_Simple_985407d7e15d471b2d5e4c6c29e79af0|:doc:`Phalcon\\Cache\\BackendInterface <Phalcon_Cache_BackendInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_10985f4916f50d49c95bcef66585df0f}%

%{Phalcon_Mvc_Model_Resultset_Simple_5e2fc751afacb5d207fe4e268ce50f45|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_7d39aa784396da33b1f190e2389cab4a}%

%{Phalcon_Mvc_Model_Resultset_Simple_22fd3aacd7766483692c508749c54385|:doc:`Phalcon\\Mvc\\Model\\MessageInterface <Phalcon_Mvc_Model_MessageInterface>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_f3bb945cd58ec7b508db2654f4133ed6}%

%{Phalcon_Mvc_Model_Resultset_Simple_7cf77fa8032c4c564b17067e7a878a53}%

%{Phalcon_Mvc_Model_Resultset_Simple_29646fd89480ebac97d734c5e2bfda09}%

%{Phalcon_Mvc_Model_Resultset_Simple_5258f0aa1391d612eba182cb4274aa74|:doc:`Phalcon\\Mvc\\Model <Phalcon_Mvc_Model>`}%

%{Phalcon_Mvc_Model_Resultset_Simple_e8845c00d77f057be2d185873b604218}%

