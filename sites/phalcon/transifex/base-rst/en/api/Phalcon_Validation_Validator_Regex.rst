%{Phalcon_Validation_Validator_Regex_3dee747baff09aac11a90dfec66d686a}%
===============================================

%{Phalcon_Validation_Validator_Regex_cac0f4e36b6c7c0d12c2ea9fa8f05795|:doc:`Phalcon\\Validation\\Validator <Phalcon_Validation_Validator>`}%

%{Phalcon_Validation_Validator_Regex_3bc33f5008c906bbf75c850085f3e576|:doc:`Phalcon\\Validation\\ValidatorInterface <Phalcon_Validation_ValidatorInterface>`}%

%{Phalcon_Validation_Validator_Regex_3d9a5b36d65b0a37c6f237f20c519adc}%

.. code-block:: php

    <?php

    use Phalcon\Validation\Validator\Regex as RegexValidator;
    
    $validator->add('created_at', new RegexValidator(array(
       'pattern' => '/^[0-9]{4}[-\/](0[1-9]|1[012])[-\/](0[1-9]|[12][0-9]|3[01])$/',
       'message' => 'The creation date is invalid'
    )));




%{Phalcon_Validation_Validator_Regex_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Validation_Validator_Regex_11680e0816c2b5612e05aaf250906b3f|:doc:`Phalcon\\Validation <Phalcon_Validation>`}%

%{Phalcon_Validation_Validator_Regex_d01ba9899fa892ec2b62154529dce37f}%

%{Phalcon_Validation_Validator_Regex_083f8660fec7a850b97c924c6ec8b3b5}%

%{Phalcon_Validation_Validator_Regex_ec865eb22d34722d5b3dfe1c2403678d}%

%{Phalcon_Validation_Validator_Regex_81b7291a5b45df34e31addf4c20fb319}%

%{Phalcon_Validation_Validator_Regex_13f58da2ee2f606006004d67f4b648cc}%

%{Phalcon_Validation_Validator_Regex_b0f96a5c6a69977cb2d6d47e77611416}%

%{Phalcon_Validation_Validator_Regex_6685002cddbd3be00d9dc4ed9cea3a75}%

%{Phalcon_Validation_Validator_Regex_74181c24b3c3cfcd1f72f02a5f4b5ae3}%

%{Phalcon_Validation_Validator_Regex_2e7772bfbad31074c960e8f21b33e650}%

