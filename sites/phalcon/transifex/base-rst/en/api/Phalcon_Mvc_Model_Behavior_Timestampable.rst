%{Phalcon_Mvc_Model_Behavior_Timestampable_2e1e555c4dbdc6aa5077f7ee05afe44f}%
======================================================

%{Phalcon_Mvc_Model_Behavior_Timestampable_48573e01444a983bf247471f52687e13|:doc:`Phalcon\\Mvc\\Model\\Behavior <Phalcon_Mvc_Model_Behavior>`}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_d7b180be60907402f7ac364916a9d8d8|:doc:`Phalcon\\Mvc\\Model\\BehaviorInterface <Phalcon_Mvc_Model_BehaviorInterface>`}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_d13a75ed46e993ebfdeaaa2a77a12932}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Behavior_Timestampable_a5f6f351001c732cf9502df3dad6a82e|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_58ed1c0293e15e4cbf2c0d57c8a6b355}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_37f886d523a5f41602863cb20f4be497}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_242fcbf2a9480164d7b3f316d1466083}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_32a118749bc461763ef478a71e00a23f}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_7a4e83afe5bb8f1dc4eea7f456c112bf}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_71b21e8b9b6cdbc6fd1292f1299d17a2}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_b297a6ed91b314ccb1f2d358a8dd99df}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_95d85a65ebcf85713381ca3aed80a0d8|:doc:`Phalcon\\Mvc\\ModelInterface <Phalcon_Mvc_ModelInterface>`}%

%{Phalcon_Mvc_Model_Behavior_Timestampable_7473ca3ce3ddeec8ffa479116b26524d}%

