%{Phalcon_Cache_Frontend_Output_dca5fd946ced45c0700388133118b7b2}%
==========================================

%{Phalcon_Cache_Frontend_Output_d4caea1dc5a1b9f969431a18993f6133|:doc:`Phalcon\\Cache\\FrontendInterface <Phalcon_Cache_FrontendInterface>`}%

%{Phalcon_Cache_Frontend_Output_f0ce7bff6015d6a9b6d970421cd4a3db}%

.. code-block:: php

    <?php

     //{%Phalcon_Cache_Frontend_Output_41a11b1a04dfedec02baf80cf4eaf762%}
     $frontCache = new Phalcon\Cache\Frontend\Output(array(
       "lifetime" => 172800
     ));
    
     // {%Phalcon_Cache_Frontend_Output_11e456117d271495bc967de753b45149%}
     // {%Phalcon_Cache_Frontend_Output_955b78a3edefc5b19e177287616694ba%}
     // {%Phalcon_Cache_Frontend_Output_5fcfde64f24f43d708d7cf2548f2076c%}
     $cache = new Phalcon\Cache\Backend\File($frontCache, array(
         "cacheDir" => "../app/cache/"
     ));
    
     // {%Phalcon_Cache_Frontend_Output_eff938041f42201190f587959a23dfd1%}
     $content = $cache->start("my-cache.html");
    
     // {%Phalcon_Cache_Frontend_Output_89f2a437bdfa2001a281e5decbad302e%}
     if ($content === null) {
    
         //{%Phalcon_Cache_Frontend_Output_a03ec28be539470ccf2ca917b1bd647a%}
         echo date("r");
    
         //{%Phalcon_Cache_Frontend_Output_91e2ee7e7ef772b62b39f1f432ad8354%}
         echo Phalcon\Tag::linkTo(
             array(
                 "user/signup",
                 "Sign Up",
                 "class" => "signup-button"
             )
         );
    
         // {%Phalcon_Cache_Frontend_Output_d581137e0d9bdf3758eaaca91853dd0c%}
         $cache->save();
    
     } else {
    
         // {%Phalcon_Cache_Frontend_Output_3c863c5a660c5145280a5be84766e095%}
         echo $content;
     }




%{Phalcon_Cache_Frontend_Output_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Cache_Frontend_Output_b8c38e5ce31cf6a5e383ba61685de98c}%

%{Phalcon_Cache_Frontend_Output_ca7306953fc32fd99d991130cf2a2cca}%

%{Phalcon_Cache_Frontend_Output_e7171f6e73a6324562abd2945b2003ff}%

%{Phalcon_Cache_Frontend_Output_790ba726a84e8cc7d11c01ae6aeda21b}%

%{Phalcon_Cache_Frontend_Output_af5e066e55f22b07f85f34b6ad7cc84a}%

%{Phalcon_Cache_Frontend_Output_1df518f9eabe9f817151b63df9f51b70}%

%{Phalcon_Cache_Frontend_Output_229317f58ccf31222a22a92e5cee6a1f}%

%{Phalcon_Cache_Frontend_Output_b8c4708fd8335e84c359ba0730d831a1}%

%{Phalcon_Cache_Frontend_Output_098b283de06b3d606bc941e79200d4a9}%

%{Phalcon_Cache_Frontend_Output_e3e99c0a1d8cbb1a36ae911dcb475f20}%

%{Phalcon_Cache_Frontend_Output_050f30cd1b1c4a7535f2642b675b5351}%

%{Phalcon_Cache_Frontend_Output_3ff217b55d51515949922d3d8f3004eb}%

%{Phalcon_Cache_Frontend_Output_f3bfdb0512736ca73d4a949b84c89981}%

%{Phalcon_Cache_Frontend_Output_6ec82edd259df44f4a5b6bfe41f6f6f9}%

%{Phalcon_Cache_Frontend_Output_bf8d76d841c9e0ff5bc2145390806e3e}%

%{Phalcon_Cache_Frontend_Output_d4b7cd788da435df9aaf3bec613fa734}%

