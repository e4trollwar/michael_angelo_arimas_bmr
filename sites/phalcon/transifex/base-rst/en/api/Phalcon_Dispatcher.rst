%{Phalcon_Dispatcher_a25d6869360b959777133fe7fbec1ba2}%
======================================

%{Phalcon_Dispatcher_b679c60aad0c54ca8fbab5234bb9f538|:doc:`Phalcon\\DispatcherInterface <Phalcon_DispatcherInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`}%

%{Phalcon_Dispatcher_48a7bd11d4ac66b997bfae9220be2641}%

%{Phalcon_Dispatcher_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Dispatcher_2ab1c3855cfa4f30ea8681e49a7aa26e}%

%{Phalcon_Dispatcher_d5edf0dea21fd1b1f0629d8dd8748263}%

%{Phalcon_Dispatcher_7c266fcd7f0c18b301f30e9d4c347d35}%

%{Phalcon_Dispatcher_e873d1c91c6aab104a52a4c3fc6ae98e}%

%{Phalcon_Dispatcher_5373fd4b144517732fdcdd404f1204d4}%

%{Phalcon_Dispatcher_67b29119d6cdd3af9d6effee3beb4644}%

%{Phalcon_Dispatcher_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Dispatcher_a5d0ee958da5dcfe392fa79afb9d9a90}%

%{Phalcon_Dispatcher_55cb5e91cd2440fb655c4beefe8015a9}%

%{Phalcon_Dispatcher_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Dispatcher_ef07d11d9210658c6d072812fcd69338}%

%{Phalcon_Dispatcher_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Dispatcher_e0aeb6a91e0e0ef96e631d38809dd6e5}%

%{Phalcon_Dispatcher_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Dispatcher_8d848637cfda8b4cc05eedcec9549316}%

%{Phalcon_Dispatcher_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Dispatcher_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Dispatcher_1a8114f1c4dc1f4efbf891d453fc0137}%

%{Phalcon_Dispatcher_9738538c9d8633d2e28e78c7241d43fc}%

%{Phalcon_Dispatcher_204effde948568b64319d61d3fa64da1}%

%{Phalcon_Dispatcher_c3fd1ddfb7b1ac557e96de064bef65c4}%

%{Phalcon_Dispatcher_b0d3976a845e6b6da6452c56c2723b2b}%

%{Phalcon_Dispatcher_f95b3227d10ff5b60b9e79c4a1730a90}%

%{Phalcon_Dispatcher_2886267afc7493f98e2f24990f491cf4}%

%{Phalcon_Dispatcher_2d882d6e8c0d49a6eedb3f517d4cf9b0}%

%{Phalcon_Dispatcher_fcebbf8bb56c7750271e3eda07bd0f9d}%

%{Phalcon_Dispatcher_98e7f3c851b83e681a8cfb1deca52482}%

%{Phalcon_Dispatcher_092ba28e49a8079dfb220725268a0100}%

%{Phalcon_Dispatcher_3463b3195e6aa105e172c3d03d03fcbe}%

%{Phalcon_Dispatcher_9b77cca113aa79d98e4d128ee3f5cf7d}%

%{Phalcon_Dispatcher_991cdf375ddb2dae5042bb86bc84464e}%

%{Phalcon_Dispatcher_f089a0a397fa78c7218a0297585d6606}%

%{Phalcon_Dispatcher_2c676539e85e0e19da7b13c5801ba8a2}%

%{Phalcon_Dispatcher_30f9ec4f6615136e682fa049fc68da35}%

%{Phalcon_Dispatcher_8a967fd800b0844321d914a97edb430d}%

%{Phalcon_Dispatcher_7653c5621328184a0ef6712f5be7b5b7}%

%{Phalcon_Dispatcher_9650e6996268a93907bd15bf21b942cc}%

%{Phalcon_Dispatcher_d394723f82b6031d1921d775a08263c9}%

%{Phalcon_Dispatcher_2b32732a49098826c9a56c09bb082415}%

%{Phalcon_Dispatcher_d88c3713331c25b77ed605f801609a8c}%

%{Phalcon_Dispatcher_350fa8429abc55e22b423ce8c779a81a}%

%{Phalcon_Dispatcher_96141d315dd17a548dcbc2187233250f}%

%{Phalcon_Dispatcher_7db2670545db59e604902d37b025e571}%

%{Phalcon_Dispatcher_abe730735ae0b1c053d5473a579ce09b}%

%{Phalcon_Dispatcher_04c89e539e998d89ebdf16e24422c3d1}%

%{Phalcon_Dispatcher_a1f7b6e42965771b9615bcc237d5cd39}%

%{Phalcon_Dispatcher_021239bbaadf601baf6438ab30e5d9bb}%

%{Phalcon_Dispatcher_53418669b7b3521dfbe2edc4cb6fd58a}%

%{Phalcon_Dispatcher_c595fd6a180f9acc31cde9a64243f8b4}%

%{Phalcon_Dispatcher_5cfb976813fa4cbeed948b8a414e2c28}%

%{Phalcon_Dispatcher_21b42a0f130610f514a2233b2607901b}%

%{Phalcon_Dispatcher_0548ddf7c0b721fadb15d2e79460990c}%

%{Phalcon_Dispatcher_e7ea1cc26cdf6fd3cf02bb4d111db443}%

%{Phalcon_Dispatcher_430c4242a488ae3951c569a43b928321}%

%{Phalcon_Dispatcher_6f7e0b27a09591425a1436f9d7847a7c}%

%{Phalcon_Dispatcher_6823497261d0baa11dcbddcaa81ec64d}%

%{Phalcon_Dispatcher_0c207e7839fbd02476c700ddcd9e0556}%

.. code-block:: php

    <?php

      $this->dispatcher->forward(array('controller' => 'posts', 'action' => 'index'));





%{Phalcon_Dispatcher_5f4ec60e805e0c446f3d164a06523ec7}%

%{Phalcon_Dispatcher_b030b040fe2839b6721cf7d05c33d291}%

%{Phalcon_Dispatcher_9b03cf265c9e22bfb9a21b9c902703a4}%

%{Phalcon_Dispatcher_e1945211cc7917bd7c3ef1c0c3c9f70a}%

