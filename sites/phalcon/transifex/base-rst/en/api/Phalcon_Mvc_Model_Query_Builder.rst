%{Phalcon_Mvc_Model_Query_Builder_b8b4267abbe0ca56a10d88b4acf0da1e}%
=============================================

%{Phalcon_Mvc_Model_Query_Builder_9008490a7224e579fa949ed238797eef|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`}%

%{Phalcon_Mvc_Model_Query_Builder_bd408c5aca10914f39ad952597289b35}%

.. code-block:: php

    <?php

    $resultset = $this->modelsManager->createBuilder()
       ->from('Robots')
       ->join('RobotsParts')
       ->limit(20)
       ->orderBy('Robots.name')
       ->getQuery()
       ->execute();




%{Phalcon_Mvc_Model_Query_Builder_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Model_Query_Builder_e0d4466d6cf227e23feb540b702fd73f}%

%{Phalcon_Mvc_Model_Query_Builder_f70941056095af59e0b7af4b30ad8764}%

.. code-block:: php

    <?php

     $params = array(
        'models'     => array('Users'),
        'columns'    => array('id', 'name', 'status'),
        'conditions' => array(
            array(
                "created > :min: AND created < :max:",
                array("min" => '2013-01-01',   'max' => '2014-01-01'),
                array("min" => PDO::PARAM_STR, 'max' => PDO::PARAM_STR),
            ),
        ),
        // {%Phalcon_Mvc_Model_Query_Builder_d55d678033ddded60d5a0f7777f528b9%}
        'group'      => array('id', 'name'),
        'having'     => "name = 'Kamil'",
        'order'      => array('name', 'id'),
        'limit'      => 20,
        'offset'     => 20,
        // {%Phalcon_Mvc_Model_Query_Builder_6615c7c7fc64761e58c5f3fe06dc2033%}
    );
    $queryBuilder = new Phalcon\Mvc\Model\Query\Builder($params);





%{Phalcon_Mvc_Model_Query_Builder_30b795bb56d213d04b0700f4f1399004|:doc:`Phalcon\\Mvc\\Model\\Query\\BuilderInterface <Phalcon_Mvc_Model_Query_BuilderInterface>`}%

%{Phalcon_Mvc_Model_Query_Builder_94580230c26db2f264111f590b0236a1}%

%{Phalcon_Mvc_Model_Query_Builder_b9ddc2b61c4e8bbf980388b93f750842}%

%{Phalcon_Mvc_Model_Query_Builder_43ccef05c6c2873376cce9ebd81046c8}%

%{Phalcon_Mvc_Model_Query_Builder_6aea679dc3d0d582d1deaf08eed5039c|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Query_Builder_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Model_Query_Builder_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Model_Query_Builder_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Model_Query_Builder_17faf2707ee887433e89c1dfc74f1983|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_2574003a9b19529266aaa61b06ab4f51}%

.. code-block:: php

    <?php

    $builder->columns(array('id', 'name'));





%{Phalcon_Mvc_Model_Query_Builder_62e23edee25721e38b02c774732bae26}%

%{Phalcon_Mvc_Model_Query_Builder_69644b0c70dc6a1a1aff55d19698eaa2}%

%{Phalcon_Mvc_Model_Query_Builder_48dce42257bcb4d908dbee7b6007a0d6|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_55da8193b6aa2522923235ec9e56c770}%

.. code-block:: php

    <?php

    $builder->from('Robots');
    $builder->from(array('Robots', 'RobotsParts'));





%{Phalcon_Mvc_Model_Query_Builder_6e0e4fbaeb17a5770eccf24033398e3f|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_5b659a6614e1bb52afcd97d8d6057564}%

.. code-block:: php

    <?php

    $builder->addFrom('Robots', 'r');





%{Phalcon_Mvc_Model_Query_Builder_3b1fa878a88583fa8a62f0ccf90ba357}%

%{Phalcon_Mvc_Model_Query_Builder_fa873d26c84eb65fd83dbc02bf0a9ff5}%

%{Phalcon_Mvc_Model_Query_Builder_19706fe0da523681db47e1bfb4c05103|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_33ea061efdab4257fe7703fec27206db}%

.. code-block:: php

    <?php

    $builder->join('Robots');
    $builder->join('Robots', 'r.id = RobotsParts.robots_id');
    $builder->join('Robots', 'r.id = RobotsParts.robots_id', 'r');
    $builder->join('Robots', 'r.id = RobotsParts.robots_id', 'r', 'LEFT');





%{Phalcon_Mvc_Model_Query_Builder_fc41e376229e44ef955706c870b226a6|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_6dd1274e7fc80f0662f9b10d14f24c35}%

.. code-block:: php

    <?php

    $builder->innerJoin('Robots');
    $builder->innerJoin('Robots', 'r.id = RobotsParts.robots_id');
    $builder->innerJoin('Robots', 'r.id = RobotsParts.robots_id', 'r');





%{Phalcon_Mvc_Model_Query_Builder_4eb801024cbe27a00fbc6274cf7d133c|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_afd17acf794d8c93344aef95bca8ba90}%

.. code-block:: php

    <?php

    $builder->leftJoin('Robots', 'r.id = RobotsParts.robots_id', 'r');





%{Phalcon_Mvc_Model_Query_Builder_335b139ca9037dc21bbe1f4058df3c0d|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_0c13135b6ff4dd75adacce9c19596a31}%

.. code-block:: php

    <?php

    $builder->rightJoin('Robots', 'r.id = RobotsParts.robots_id', 'r');





%{Phalcon_Mvc_Model_Query_Builder_36671295692ae490988439b62b90a76b|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_7fe94c59b1807965127d4f97a7c2e518}%

.. code-block:: php

    <?php

    $builder->where('name = "Peter"');
    $builder->where('name = :name: AND id > :id:', array('name' => 'Peter', 'id' => 100));





%{Phalcon_Mvc_Model_Query_Builder_f31bccbcf8da2314d5ed65c437fd777f|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_5a65a7e11e91798a7aaad1c5a299db27}%

.. code-block:: php

    <?php

    $builder->andWhere('name = "Peter"');
    $builder->andWhere('name = :name: AND id > :id:', array('name' => 'Peter', 'id' => 100));





%{Phalcon_Mvc_Model_Query_Builder_951b9d2d51e975f145c448badfbac65b|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_e11de6a274afd75e64091efbcde03778}%

.. code-block:: php

    <?php

    $builder->orWhere('name = "Peter"');
    $builder->orWhere('name = :name: AND id > :id:', array('name' => 'Peter', 'id' => 100));





%{Phalcon_Mvc_Model_Query_Builder_dc27b206a6d42168245de5596b9bde1b|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_053efd36d40c71c6d89b754bcb77aed4}%

.. code-block:: php

    <?php

    $builder->betweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_Query_Builder_dea31c3c7be5b86b7753015fbcebf769|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_3af635042b716a4dc8151c2a92beedff}%

.. code-block:: php

    <?php

    $builder->notBetweenWhere('price', 100.25, 200.50);





%{Phalcon_Mvc_Model_Query_Builder_3e0140326f30c79de150f30baca15897|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_fa989e42243a0cf3eda9b98cb8a995f3}%

.. code-block:: php

    <?php

    $builder->inWhere('id', [1, 2, 3]);





%{Phalcon_Mvc_Model_Query_Builder_cec15cadb6983b752d56de3c31f726e2|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_598624af0745dc322869bd1b14451f20}%

.. code-block:: php

    <?php

    $builder->notInWhere('id', [1, 2, 3]);





%{Phalcon_Mvc_Model_Query_Builder_144a78ec52661a855e7272d62a46c89e}%

%{Phalcon_Mvc_Model_Query_Builder_e16b11297c09d247829b72e3939ec90c}%

%{Phalcon_Mvc_Model_Query_Builder_5ee2f3d86068f6e0da6e9237ab65709f|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_aed2fac8e91958324f7020f54821a4c1}%

.. code-block:: php

    <?php

    $builder->orderBy('Robots.name');
    $builder->orderBy(array('1', 'Robots.name'));





%{Phalcon_Mvc_Model_Query_Builder_54f27a13c85d4fd0502f63c86b598f37}%

%{Phalcon_Mvc_Model_Query_Builder_a5b335815d7152b837e45778ecefc0a3}%

%{Phalcon_Mvc_Model_Query_Builder_7e42937240b85c318fd3dc59cf20907e|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_299e9120191e7416797a9d775e8d2e91}%

.. code-block:: php

    <?php

    $builder->having('SUM(Robots.price) > 0');





%{Phalcon_Mvc_Model_Query_Builder_ed7df09012341ea075ee454e08e03c11}%

%{Phalcon_Mvc_Model_Query_Builder_32e845b6b08ede9c356e0b1f17c13dc7}%

%{Phalcon_Mvc_Model_Query_Builder_da7ff6c56daa8631563e185b835fd159|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_91f3ed3a9c5cc7298b78c3693c70cd53}%

.. code-block:: php

    <?php

    $builder->limit(100);
    $builder->limit(100, 20);





%{Phalcon_Mvc_Model_Query_Builder_8f63d19d29d0cb7cb49d32798221afab}%

%{Phalcon_Mvc_Model_Query_Builder_7649637bec95e5a9e667b6b10c14aae1}%

%{Phalcon_Mvc_Model_Query_Builder_0a17088af75594ea0d133d41a39456ab|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_0742f8071f32236e48efa0b5b8eed61b}%

.. code-block:: php

    <?php

    $builder->offset(30);





%{Phalcon_Mvc_Model_Query_Builder_5e2f96e349287e14e677d663402dcd3c}%

%{Phalcon_Mvc_Model_Query_Builder_2c55174825ba9521372e137eab52088f}%

%{Phalcon_Mvc_Model_Query_Builder_5c0b96e8a2c1b78b51bde29fdb4e651e|:doc:`Phalcon\\Mvc\\Model\\Query\\Builder <Phalcon_Mvc_Model_Query_Builder>`}%

%{Phalcon_Mvc_Model_Query_Builder_e8aac96f1bebe775f3c58134767366bb}%

.. code-block:: php

    <?php

    $builder->groupBy(array('Robots.name'));





%{Phalcon_Mvc_Model_Query_Builder_9640c3b69b27ecfe36a9393aed8e5ff8}%

%{Phalcon_Mvc_Model_Query_Builder_954b2a1161fae1f6ab8a18f5d9ddf137}%

%{Phalcon_Mvc_Model_Query_Builder_8d2c446376032886a2d932d453c1888f}%

%{Phalcon_Mvc_Model_Query_Builder_fbed73a8112f4a2fa839cc97dccc5799}%

%{Phalcon_Mvc_Model_Query_Builder_3b69018d31a444587e00baeadf295c71|:doc:`Phalcon\\Mvc\\Model\\Query <Phalcon_Mvc_Model_Query>`}%

%{Phalcon_Mvc_Model_Query_Builder_66d053994e67a0f80fd133d8c4319d47}%

