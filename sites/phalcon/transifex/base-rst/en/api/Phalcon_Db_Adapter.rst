%{Phalcon_Db_Adapter_dc80fd701cb16d80ebe6c04d6713f552}%
=======================================

%{Phalcon_Db_Adapter_7d0733e1381363bac9600138af495f82|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Db_Adapter_d5b3f85019132d2c44517fb814e89cf4}%

%{Phalcon_Db_Adapter_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Db_Adapter_3be72228848b5b0cfac3587dcef26f20}%

%{Phalcon_Db_Adapter_ae6c05b815f8decc795d8a9e168cac0c}%

%{Phalcon_Db_Adapter_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Db_Adapter_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Db_Adapter_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Db_Adapter_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Db_Adapter_508d0dec14ee07aca353747d2473a792}%

%{Phalcon_Db_Adapter_7405adc04f8e073fea4449c9ec0dcdbb}%

%{Phalcon_Db_Adapter_9f679f86d08df0a7ec3a88b248812af9|:doc:`Phalcon\\Db\\DialectInterface <Phalcon_Db_DialectInterface>`}%

%{Phalcon_Db_Adapter_dfbf23645602677fc82754d84973dd18}%

%{Phalcon_Db_Adapter_5b46b483beeddfb9203f5360a98f1366}%

%{Phalcon_Db_Adapter_5fb298f91e097316bdd41a45592bf05d}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Adapter_b11742e0634a5ed06167a6a27fd6bdf6%}
    $robot = $connection->fetchOne("SELECT * FROM robots");
    print_r($robot);
    
    //{%Phalcon_Db_Adapter_76e1f206704b79f65bb244b53a2d14ea%}
    $robot = $connection->fetchOne("SELECT * FROM robots", Phalcon\Db::FETCH_ASSOC);
    print_r($robot);





%{Phalcon_Db_Adapter_d5a36a74379d3dda6d4835b22525118d}%

%{Phalcon_Db_Adapter_533879dcfc578e9d9beccfa4e36332c6}%

.. code-block:: php

    <?php

    //{%Phalcon_Db_Adapter_8ae30418bcb37bf7e95ce3ac42cae77c%}
    $robots = $connection->fetchAll("SELECT * FROM robots", Phalcon\Db::FETCH_ASSOC);
    foreach ($robots as $robot) {
    	print_r($robot);
    }
    
      //{%Phalcon_Db_Adapter_3d0f42eaf81337476a3aa5ecc8e536e9%}
      $robots = $connection->fetchAll("SELECT * FROM robots WHERE name LIKE :name",
    	Phalcon\Db::FETCH_ASSOC,
    	array('name' => '%robot%')
      );
    foreach($robots as $robot){
    	print_r($robot);
    }





%{Phalcon_Db_Adapter_ab83b3292c27d51105f2cabea0e61480}%

%{Phalcon_Db_Adapter_d9fdc1d16462ce210a1f40f64f0db506}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_a6d75204a8bf217cb392074b3ca4cc87%}
     $success = $connection->insert(
         "robots",
         array("Astro Boy", 1952),
         array("name", "year")
     );
    
     //{%Phalcon_Db_Adapter_f7bb4646596b31ceed34225ee4851f2b%}
     INSERT INTO `robots` (`name`, `year`) VALUES ("Astro boy", 1952);





%{Phalcon_Db_Adapter_b363b53f8a94f3def98d41855eb6d660}%

%{Phalcon_Db_Adapter_a358e27bdde98bbf702631fdb73783d5}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_86f7dc0b6663836e4c275c21174bad74%}
     $success = $connection->update(
         "robots",
         array("name"),
         array("New Astro Boy"),
         "id = 101"
     );
    
     //{%Phalcon_Db_Adapter_f7bb4646596b31ceed34225ee4851f2b%}
     UPDATE `robots` SET `name` = "Astro boy" WHERE id = 101





%{Phalcon_Db_Adapter_1c8d3cf3e58e389ab42b5d5f78304e02}%

%{Phalcon_Db_Adapter_26df2fdb3c3a0c35d6a2b2a9ce6cdd6d}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_b8901a82eceebe8d7e34dd317f881fd1%}
     $success = $connection->delete(
         "robots",
         "id = 101"
     );
    
     //{%Phalcon_Db_Adapter_7992087a502ec3273b0b5e847292ea81%}
     DELETE FROM `robots` WHERE `id` = 101





%{Phalcon_Db_Adapter_dcd0543b55d4946db59a4240b10ada74}%

%{Phalcon_Db_Adapter_cef775cddd996a0683f57c5242219e20}%

%{Phalcon_Db_Adapter_8840498d1022a28356e91127956e4f2b}%

%{Phalcon_Db_Adapter_5a4a993ea6e204ee5f9f2fa04da81a1f}%

.. code-block:: php

    <?php

     	echo $connection->limit("SELECT * FROM robots", 5);





%{Phalcon_Db_Adapter_eac5f83f82ca57a88484d52843fa25fb}%

%{Phalcon_Db_Adapter_733ee4acdd7aca845798bc20c0cb008f}%

.. code-block:: php

    <?php

     	var_dump($connection->tableExists("blog", "posts"));





%{Phalcon_Db_Adapter_0d4f75e38e916631454afb4a73e53e87}%

%{Phalcon_Db_Adapter_b471ca007fa5635663442ca313939f1f}%

.. code-block:: php

    <?php

     var_dump($connection->viewExists("active_users", "posts"));





%{Phalcon_Db_Adapter_cf8e2d041b6c93ed62d2c1f329d795d0}%

%{Phalcon_Db_Adapter_52d3aac013e30a8356f7bf56e85433d2}%

%{Phalcon_Db_Adapter_cdbf0c7a0d7d45d33cb674d050537cf2}%

%{Phalcon_Db_Adapter_2d4abc0c85ba918abd9ea6c24091abfc}%

%{Phalcon_Db_Adapter_db7b3673e77643893c485a13809265cb}%

%{Phalcon_Db_Adapter_a7fc71aafb1ecd0ff268d7c38c662c06}%

%{Phalcon_Db_Adapter_36e3cd45666d52b46cb15b9f52da6724}%

%{Phalcon_Db_Adapter_d22fc1207292217bf710c07457192c75}%

%{Phalcon_Db_Adapter_153b6bf3d0dd33595b953f9d6e479728}%

%{Phalcon_Db_Adapter_000156e030a02184d59d8da1371f3849}%

%{Phalcon_Db_Adapter_8de8f139f793829576929659c0ce7b7f}%

%{Phalcon_Db_Adapter_a804956605ee1dddfa70dc8a56187382}%

%{Phalcon_Db_Adapter_7d996fba3bae93ddcdc6d160fd692312|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_f42b09293fb9b3b4ef977cfa3b19da02}%

%{Phalcon_Db_Adapter_6ccbc731f6bb5169f472ca812f829f16|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_658ec0a8201eb7d0360751eea7126854}%

%{Phalcon_Db_Adapter_cd19792d2e6b0eba298f45028091c6fb}%

%{Phalcon_Db_Adapter_7a3c7e655fe7f34e1803d59d587e7d55}%

%{Phalcon_Db_Adapter_73e67fca06817f9232c45da5ac66e38b|:doc:`Phalcon\\Db\\IndexInterface <Phalcon_Db_IndexInterface>`}%

%{Phalcon_Db_Adapter_206b49008e22eeb21c78be67c9703395}%

%{Phalcon_Db_Adapter_9525b8e96229ef29b858ef3ccea192f8}%

%{Phalcon_Db_Adapter_9f5ae026beab44ce8e3068331ef0a022}%

%{Phalcon_Db_Adapter_682050f4a8a680b2a70ac785c2451fcb|:doc:`Phalcon\\Db\\IndexInterface <Phalcon_Db_IndexInterface>`}%

%{Phalcon_Db_Adapter_f5ff85edfe2b24efde0ce1938797471a}%

%{Phalcon_Db_Adapter_ed52ad90fcdc4b212d371d14d7efe5c2}%

%{Phalcon_Db_Adapter_f52cd75354fb5d64e978ee59cdd0dfe9}%

%{Phalcon_Db_Adapter_fc79341ab1bde38d86d3fa0bef0af01c|:doc:`Phalcon\\Db\\ReferenceInterface <Phalcon_Db_ReferenceInterface>`}%

%{Phalcon_Db_Adapter_7091106149e8afa191924640a7c537f5}%

%{Phalcon_Db_Adapter_0cb9eacddb70d0fa9d5880f54284203a}%

%{Phalcon_Db_Adapter_0f0c2b54afb93884d0bd5c1e081ff67f}%

%{Phalcon_Db_Adapter_1e2995e761c9d63f65abea294829b4df|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_55e20593fbd175ee67322cc1187c8ade}%

%{Phalcon_Db_Adapter_65715be9ff5d7304382d86644161dba8}%

%{Phalcon_Db_Adapter_1f97bb21531bf7a74abf40f789c10861}%

.. code-block:: php

    <?php

     	print_r($connection->listTables("blog"));





%{Phalcon_Db_Adapter_85f0633329bc102318ac47b0c9d170c2}%

%{Phalcon_Db_Adapter_f23a83679c69fb3e27ee6904d11d4bc5}%

.. code-block:: php

    <?php

    print_r($connection->listViews("blog")); ?>





%{Phalcon_Db_Adapter_1603f0e08e0614b7fef23ff29274d875|:doc:`Phalcon\\Db\\Index <Phalcon_Db_Index>`}%

%{Phalcon_Db_Adapter_dded571f895472a2faafd35a6988b25b}%

.. code-block:: php

    <?php

    print_r($connection->describeIndexes('robots_parts'));





%{Phalcon_Db_Adapter_8c2718be0715cc5f5bea692fd92a2563|:doc:`Phalcon\\Db\\Reference <Phalcon_Db_Reference>`}%

%{Phalcon_Db_Adapter_996d3196cca64adc57b2236c50c7df36}%

.. code-block:: php

    <?php

     print_r($connection->describeReferences('robots_parts'));





%{Phalcon_Db_Adapter_195e9744ae3402a6dc1adf987c2e28ba}%

%{Phalcon_Db_Adapter_f427319993001e5061cbca9bf42bae34}%

.. code-block:: php

    <?php

     print_r($connection->tableOptions('robots'));





%{Phalcon_Db_Adapter_8cc5794b8f07ff355f363c2820586d92}%

%{Phalcon_Db_Adapter_4240f3e368588a9a9ed238bf19a11863}%

%{Phalcon_Db_Adapter_73e8073d69068e14c2a9c033f0f4dca2}%

%{Phalcon_Db_Adapter_a382c706189656fce42f2a9cd80784e5}%

%{Phalcon_Db_Adapter_fb6968fceee91f07186eb76cf9949717}%

%{Phalcon_Db_Adapter_8ea57471de99c4626beb0068a3a0cc82}%

%{Phalcon_Db_Adapter_d5afc9fff79c547406618cc109eee42d|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`}%

%{Phalcon_Db_Adapter_65f84254266985664e7b0041fcd54b9a}%

%{Phalcon_Db_Adapter_4bc465912ccc54101f88cf05233f7836}%

%{Phalcon_Db_Adapter_936a103d73aac2ac6a8061e5ce579c62}%

%{Phalcon_Db_Adapter_c88ba9caddd6ace0b1cf26fccbcc7376}%

%{Phalcon_Db_Adapter_b68c46d2d0f254d7846ede4084807435}%

%{Phalcon_Db_Adapter_21c80568c16beb118e0ea5790efa72cd|:doc:`Phalcon\\Db\\RawValue <Phalcon_Db_RawValue>`}%

%{Phalcon_Db_Adapter_44713ee7c895fe92dedce30931309159}%

.. code-block:: php

    <?php

     //{%Phalcon_Db_Adapter_7465da78909b003fe9c9634390518500%}
     $success = $connection->insert(
         "robots",
         array($connection->getDefaultIdValue(), "Astro Boy", 1952),
         array("id", "name", "year")
     );





%{Phalcon_Db_Adapter_8846fc9d5808d88aa7801ca7eca7c4b5}%

%{Phalcon_Db_Adapter_74864962afc9a0a54b670ed3f97bd879}%

%{Phalcon_Db_Adapter_37cb7c342254076862a44f99331cb1e4}%

%{Phalcon_Db_Adapter_bf6837aa9e7072ebf3ab752d8a0d0a06}%

%{Phalcon_Db_Adapter_d785a587cd222a938d8e1d8ba5e7eda4}%

%{Phalcon_Db_Adapter_c3985835d1e4654795856a386b108e02}%

%{Phalcon_Db_Adapter_3c1bc036f1cf0b37850ceea8360b74c4}%

%{Phalcon_Db_Adapter_a6e492ae4d5dc538bf27544d2adaf1c5}%

%{Phalcon_Db_Adapter_dc06acd0d27092bedf1e5a8db3916506}%

%{Phalcon_Db_Adapter_57c1971d37fe7408651d816f3304544f}%

%{Phalcon_Db_Adapter_517202ea49f4cbe4abd62b9a95597e7b}%

%{Phalcon_Db_Adapter_6cc3615bd0f815d37c62512c3de3aa59}%

%{Phalcon_Db_Adapter_cb6dcb0da2b2e0eeee381a5daea68d68}%

%{Phalcon_Db_Adapter_57c1971d37fe7408651d816f3304544f}%

%{Phalcon_Db_Adapter_b2ee9abdc05b28f2ed3c47b27a7358cc}%

%{Phalcon_Db_Adapter_57c1971d37fe7408651d816f3304544f}%

%{Phalcon_Db_Adapter_9b6af70c1c9980c497b487b7e2dbbccd}%

%{Phalcon_Db_Adapter_ac7d79c5075d89af44c82d00bbf623f7}%

%{Phalcon_Db_Adapter_cf8fa61cea03eaa093206587c04dbdf0}%

%{Phalcon_Db_Adapter_2b0bc4b05d6684386da7ee7849b1306e}%

%{Phalcon_Db_Adapter_ccbb99531a4085dd0829a56d2a11868c}%

%{Phalcon_Db_Adapter_ea9e5a3971093d702c8cbaa22e770b92}%

%{Phalcon_Db_Adapter_1878658e3e21b2289bf9c33ce5978f33|:doc:`Phalcon\\Db\\ResultInterface <Phalcon_Db_ResultInterface>`}%

%{Phalcon_Db_Adapter_d0db7cfc7e9f9fcef6ac08225f724d62}%

%{Phalcon_Db_Adapter_f138aa642a8c1d8523e25acea7135f9a}%

%{Phalcon_Db_Adapter_65c8b6f8fc099ae9eef6d8a63212b602}%

%{Phalcon_Db_Adapter_f34ea75dd6502b62425783eedbaa8a36}%

%{Phalcon_Db_Adapter_842906af81f0819e30bb02262385fee6}%

%{Phalcon_Db_Adapter_bbd689069ed0b62bb8d74a3f26d90057}%

%{Phalcon_Db_Adapter_f20b22b99baac361fa82726aee551eee}%

%{Phalcon_Db_Adapter_05aa158d3df02a7985d18ef067621c5c}%

%{Phalcon_Db_Adapter_84c64462ca45fa0db805774782541586}%

%{Phalcon_Db_Adapter_5456f17bff62f2e44daafc250293a9ab}%

%{Phalcon_Db_Adapter_dd14b6410368ede674ffe11a88c290ed}%

%{Phalcon_Db_Adapter_70dcba781b316cad6ec37dc73fbad80a}%

%{Phalcon_Db_Adapter_6db2397bd9be15c685a03b65eb6cefcf}%

%{Phalcon_Db_Adapter_c307bcf18092b859dbbaf5b2ca77a3d8}%

%{Phalcon_Db_Adapter_a2b86c428948f345a7b67c2d7954ff78}%

%{Phalcon_Db_Adapter_9cb26c7340cfbf42abfe3191ff03c904}%

%{Phalcon_Db_Adapter_fe5dce48c5b13658e4f75935b6eb2fa3}%

%{Phalcon_Db_Adapter_3d4961a2af85af77c7fc86905ff78734}%

%{Phalcon_Db_Adapter_10cd19f3c7f746aaccef62a5110cb83e}%

%{Phalcon_Db_Adapter_44c9cd8e5f62ec32ed1b4b2abbbd3f90}%

%{Phalcon_Db_Adapter_ca13799e68b46cb24770c655cb161130}%

%{Phalcon_Db_Adapter_7a9decc8829b054c5a769e4148a27ef6}%

%{Phalcon_Db_Adapter_3ba7834c4caeb2287efa9bc1d0608513}%

%{Phalcon_Db_Adapter_6c9192f7590fad516265ed1d96a7c6eb}%

%{Phalcon_Db_Adapter_98e9136cd119cd76e168d9a89697a175}%

%{Phalcon_Db_Adapter_0911d2f5db66ce76c0fb960adc7a33e4|:doc:`Phalcon\\Db\\ColumnInterface <Phalcon_Db_ColumnInterface>`}%

%{Phalcon_Db_Adapter_3f828550aec898161d6a15f4b09ccb88}%

