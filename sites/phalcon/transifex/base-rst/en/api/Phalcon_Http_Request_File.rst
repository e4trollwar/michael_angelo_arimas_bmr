%{Phalcon_Http_Request_File_ace3408426d749b0cfc7c318441842d8}%
======================================

%{Phalcon_Http_Request_File_bcd7ef25c8b8985deb41662916b62e0a}%

%{Phalcon_Http_Request_File_1fa81c59bb76ed5f56bc5972622c7e30|:doc:`Phalcon\\Http\\Request\\FileInterface <Phalcon_Http_Request_FileInterface>`}%

%{Phalcon_Http_Request_File_cb3c740936081d62fb03a904e14e612f}%

.. code-block:: php

    <?php

    class PostsController extends \Phalcon\Mvc\Controller
    {
    
    	public function uploadAction()
    	{
    		//{%Phalcon_Http_Request_File_22c08743ae48bdf32897571caee40414%}
    		if ($this->request->hasFiles() == true) {
    			//{%Phalcon_Http_Request_File_e0a0204d39fe3aeddc6baccbf6415391%}
    			foreach ($this->request->getUploadedFiles() as $file){
    				echo $file->getName(), " ", $file->getSize(), "\n";
    			}
    		}
    	}
    
    }




%{Phalcon_Http_Request_File_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Http_Request_File_467510dec18b4186c05c3472dd77136c}%

%{Phalcon_Http_Request_File_b22dfc792440b7681355451eca5046c1}%

%{Phalcon_Http_Request_File_6ff51604371a9e42725d7f9994350974}%

%{Phalcon_Http_Request_File_84314f764932f310fed8a23479d8f3b4}%

%{Phalcon_Http_Request_File_bad4450b78127e6c6b52db7413174386}%

%{Phalcon_Http_Request_File_9703a4a1bc7906f570fd23aec2da627c}%

%{Phalcon_Http_Request_File_642c073f8c958677f83d13727bed3059}%

%{Phalcon_Http_Request_File_671cb68a5c12643ea95896f840480927}%

%{Phalcon_Http_Request_File_9b6af70c1c9980c497b487b7e2dbbccd}%

%{Phalcon_Http_Request_File_13b677ca5d95bcc830b2bb048787d716}%

%{Phalcon_Http_Request_File_0af5d3523e70adf01f38f1d8ed801aa0}%

%{Phalcon_Http_Request_File_b0e0601453e56dbd0fad7a0616aee8d3}%

%{Phalcon_Http_Request_File_e76da9491a631a6b8a3b258edfb07802}%

%{Phalcon_Http_Request_File_2f4573a702961e62d9043461d5124aa7}%

%{Phalcon_Http_Request_File_fec438ce12a8a7fd10c6da124b49aaeb}%

%{Phalcon_Http_Request_File_21c76369deca7167f8e3f2a94e206195}%

%{Phalcon_Http_Request_File_4e1a28d2673f648ab645b6ce13efb554}%

%{Phalcon_Http_Request_File_6b357714fdcc8015f7f51bf41424a505}%

%{Phalcon_Http_Request_File_87744a24b32bcb4cc2c88cc43fb33971}%

%{Phalcon_Http_Request_File_3a0fa35ade78d867a610d7ea85acb3d8}%

%{Phalcon_Http_Request_File_af88624b3c2d2b11939b039bb00ed1aa}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_c1a98b8e22d6d66a7113fc6032020ade}%

%{Phalcon_Http_Request_File_991f6edd24b1dda0d31b2fb40c5fcdd9}%

%{Phalcon_Http_Request_File_7980134b3bf6ee17f283578f61abd4a4}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_1daf592178e9dcfd1aefc6bb3ddffe65}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_eaab3308bc7f5407660be959cb004a6f}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_9002262ec6cf36927b7d6ae22d3afb6d}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_4a5e137fdfbf5a0124d6b2493875d7e1}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_5f63d02f7a3567a2084f2dd9ca4cd2c7}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_25c0623a030b7f3a029e593e750270a6}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_5bf2658b06bbaa919537441986515bfe}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_073e7aad8d37d297390be53270472a96}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_11b24d1b219dbb40a388c72d11cd051b}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_f6b7e5461865c30443bcf9d143ef3933}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_0e88ec3b9a923b5b136f63674a2c4d8f}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_0c72dd1a91eaf674193d22f776dd44ee}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_db622e0b98dedf470c0ebe25dcae0972}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_875ee4e6ad45b7b54f0c635d09709c25}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_504df078176028ec5e9b7f12ec1222a5}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_9d4982d8cc59d36fd2b1b0f5fb6f5078}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_cae14a6e740a80477b838076be4e2623}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_4c9ec4863d180bf9fd5352d8d0b3772f}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_6cee1b3e82d31be27aa89c0ca5346429}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_9640b51cce5c89a14ca91e0a4e931338}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_5939b74e9cf8115cb47b1dd25beb0ec6}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_5d6733e5fbf6ba70ad00641a139695fb}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_7a07e2ab56fc37331c487d2a761c7482}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_5af5b2635023f977e6297397f58242f6}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Http_Request_File_3235851db293423bbf46a4e65bce6fa9}%

%{Phalcon_Http_Request_File_68cf0a3bff1e41b907cf955f570c5249}%

