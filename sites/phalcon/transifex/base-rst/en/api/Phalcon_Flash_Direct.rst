%{Phalcon_Flash_Direct_6b5e0249a6616ca450b2e49e3e9e59d4}%
================================

%{Phalcon_Flash_Direct_650e853ed528ad34ec155fc14dd2158c|:doc:`Phalcon\\Flash <Phalcon_Flash>`}%

%{Phalcon_Flash_Direct_0ca476cfd15cdced0a9f07a48bc05ac5|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Direct_e3f87f37a0b3a3d4c3549bbe78160d71}%

%{Phalcon_Flash_Direct_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Flash_Direct_885eddb6cb68a9590417602181af5185}%

%{Phalcon_Flash_Direct_30e1e23e103579f01d6e69a089fa2879}%

%{Phalcon_Flash_Direct_7055f64235da4d2d063dbb0cdf056e82}%

%{Phalcon_Flash_Direct_3527d7ad93db44794390a7e8cd6350dc}%

%{Phalcon_Flash_Direct_d1a4e406d0f85ca72d6f64d56d18f5e7|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Direct_e612daac8bf7fcddafa12e533e332a89}%

%{Phalcon_Flash_Direct_36cf107f43e3e9d89a208fd76729bfd9|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Direct_82537bac1a539b2f419f860c0a1e616c}%

%{Phalcon_Flash_Direct_8fbb66fffb0d8814130ef9ce28472fa2|:doc:`Phalcon\\FlashInterface <Phalcon_FlashInterface>`}%

%{Phalcon_Flash_Direct_c14da91f6f71b7c71f50da51d2f1c14a}%

%{Phalcon_Flash_Direct_f2c88f1773e1e571d65a97e113b9a832}%

%{Phalcon_Flash_Direct_508f3a050e254578328bc958df18cb4a}%

.. code-block:: php

    <?php

     $flash->error('This is an error');





%{Phalcon_Flash_Direct_075283601b82def0f7964940af2d2bfb}%

%{Phalcon_Flash_Direct_f5bb0b1e9d9d0f9558960a1e42afcf04}%

.. code-block:: php

    <?php

     $flash->notice('This is an information');





%{Phalcon_Flash_Direct_2778b44ae3d3a209cf9f94bea9d83efa}%

%{Phalcon_Flash_Direct_26239272d000aa42450fb5cc48f612ae}%

.. code-block:: php

    <?php

     $flash->success('The process was finished successfully');





%{Phalcon_Flash_Direct_3cbf30a72bda1909c61c7bffaba66e9d}%

%{Phalcon_Flash_Direct_545dfddf25dafd6fe2ad13a5294c1da4}%

.. code-block:: php

    <?php

     $flash->warning('Hey, this is important');





%{Phalcon_Flash_Direct_437d6c35e4ba290aa4e98338916ad0fc}%

%{Phalcon_Flash_Direct_bb78ddd310175faeaab1d79dc924b66e}%

