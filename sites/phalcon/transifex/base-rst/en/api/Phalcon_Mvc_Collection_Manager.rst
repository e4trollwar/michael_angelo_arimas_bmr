%{Phalcon_Mvc_Collection_Manager_11c9e63ab59f196aafd124fab442a683}%
===========================================

%{Phalcon_Mvc_Collection_Manager_408a97f9bc33c13241a979e1a782e449|:doc:`Phalcon\\DI\\InjectionAwareInterface <Phalcon_DI_InjectionAwareInterface>`|:doc:`Phalcon\\Events\\EventsAwareInterface <Phalcon_Events_EventsAwareInterface>`|:doc:`Phalcon\\Mvc\\Collection\\ManagerInterface <Phalcon_Mvc_Collection_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_Manager_d831250c900cfae424d2ad3f43ee8a77}%

.. code-block:: php

    <?php

     $di = new Phalcon\DI();
    
     $di->set('collectionManager', function(){
          return new Phalcon\Mvc\Collection\Manager();
     });
    
     $robot = new Robots($di);




%{Phalcon_Mvc_Collection_Manager_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Mvc_Collection_Manager_5c934e5ad3391cd32b87774194b6385c|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Collection_Manager_94e6c414d0fd6a0f7ccd98b5e0248793}%

%{Phalcon_Mvc_Collection_Manager_fb4b36349b36e0079624df03390ac3cb|:doc:`Phalcon\\DiInterface <Phalcon_DiInterface>`}%

%{Phalcon_Mvc_Collection_Manager_c271689200fb8dd3892f100a2a46b43f}%

%{Phalcon_Mvc_Collection_Manager_421452d52c36a085aadfe9d09e3ed903|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_Manager_994924f85ea614b243b7b4d954e37a48}%

%{Phalcon_Mvc_Collection_Manager_84183f084c3c8e2bd3ac9cc7dd34a305|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_Manager_fddf8905978e673c97a301a70bb79d53}%

%{Phalcon_Mvc_Collection_Manager_05349edbd36ad5dcffb0b5d3c3492afe|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`}%

%{Phalcon_Mvc_Collection_Manager_6f33293960d052d3768dc3059dc5491b}%

%{Phalcon_Mvc_Collection_Manager_4abbe4b9699d85890a431d8304a38b47|:doc:`Phalcon\\Events\\ManagerInterface <Phalcon_Events_ManagerInterface>`|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_3864b73bbb521140066799837212cb9a}%

%{Phalcon_Mvc_Collection_Manager_8d153d5010ade3e78234d47e1563d40c|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_4b067296c7c4bfbd5a48116388c19b89}%

%{Phalcon_Mvc_Collection_Manager_77cbfb0063227b0eead190619a684ff1}%

%{Phalcon_Mvc_Collection_Manager_c52ee46fe0a73bff1a27e102d1c0f49c}%

%{Phalcon_Mvc_Collection_Manager_ed9fe126cbfea841ca33fd88dfa59c96|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_11886d1443ed07ef878302e83d33dd30}%

%{Phalcon_Mvc_Collection_Manager_f85bfc6c931af9c111c5b9881dc38355|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_2fa4a6e0306b53b434e0791ac72cc09d}%

%{Phalcon_Mvc_Collection_Manager_da8c4ce403ca6462d8fa8cf14f134693|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_7c8f580e0d9b3029bc0066d6c2445f51}%

%{Phalcon_Mvc_Collection_Manager_c132ef20dcc9596e1384f7308574825c|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_a5a33de749796a6093af6715e763ede1}%

%{Phalcon_Mvc_Collection_Manager_e5a3e656bb888257daa64ff17a724918|:doc:`Phalcon\\Db\\AdapterInterface <Phalcon_Db_AdapterInterface>`|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_2bd73e2486a34dc90c75dfa92142b08c}%

%{Phalcon_Mvc_Collection_Manager_33be0e46851893ad3f9e8f1faa794de3|:doc:`Phalcon\\Mvc\\CollectionInterface <Phalcon_Mvc_CollectionInterface>`}%

%{Phalcon_Mvc_Collection_Manager_5ab646fcc0177d33b88cec54073f262e}%

