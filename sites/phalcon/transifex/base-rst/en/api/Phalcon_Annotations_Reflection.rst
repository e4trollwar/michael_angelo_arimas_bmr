%{Phalcon_Annotations_Reflection_368babb67bf5a844d010518054cf9201}%
==========================================

%{Phalcon_Annotations_Reflection_be69e4a1338e187d43dfdde5028e2230}%

.. code-block:: php

    <?php

     //{%Phalcon_Annotations_Reflection_1d45c012f96e9f7ce39e26a31172ba3c%}
     $reader = new \Phalcon\Annotations\Reader();
     $parsing = $reader->parse('MyComponent');
    
     //{%Phalcon_Annotations_Reflection_138b015df5dce96f75a57b1a3f3a7923%}
     $reflection = new \Phalcon\Annotations\Reflection($parsing);
    
     //{%Phalcon_Annotations_Reflection_553824fe5d7264e97259e0ba0b8566cb%}
     $classAnnotations = $reflection->getClassAnnotations();




%{Phalcon_Annotations_Reflection_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Annotations_Reflection_d36de06ef843b4f5a7d703936a25cb04}%

%{Phalcon_Annotations_Reflection_fe0c36cd692c27d08b3fd128d5966330}%

%{Phalcon_Annotations_Reflection_fc4eb63bacdebeec7b5c928cba2f9083|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Reflection_dbacbb81928432adbbadf557416dd52d}%

%{Phalcon_Annotations_Reflection_447ce33cbfcedb7123f32313855e73bc|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Reflection_7f396e59bad0bcebe9e99e212ce506cc}%

%{Phalcon_Annotations_Reflection_25ce0d27b620a0b14f2523e180e36a9e|:doc:`Phalcon\\Annotations\\Collection <Phalcon_Annotations_Collection>`}%

%{Phalcon_Annotations_Reflection_ce745d99cde2a64bb58fca03793dd3ff}%

%{Phalcon_Annotations_Reflection_f71fcf92204810e58ca3f4feeb302416}%

%{Phalcon_Annotations_Reflection_87cdc7c2b6b170c05a7b9f7681bfaaba}%

%{Phalcon_Annotations_Reflection_f5939c605def4a1b3144d7bee7300e5c}%

%{Phalcon_Annotations_Reflection_0a22d71cab5d4f1546de120f1c6ab85d}%

