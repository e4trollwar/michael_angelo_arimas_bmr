%{Phalcon_Config_Adapter_Json_8e9c9006b40477bb7eb675a08527611b}%
========================================

%{Phalcon_Config_Adapter_Json_ea6c93ca1a7d7149d6682d7966a401a4|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Json_fca1ea0b1fe3f9769124d410df005401}%

%{Phalcon_Config_Adapter_Json_50a48f7b1dad5e72a58e1c1994c71d65}%

.. code-block:: php

    <?php

    {"phalcon":{"baseuri":"\/phalcon\/"},"models":{"metadata":"memory"}}

  You can read it as follows:  

.. code-block:: php

    <?php

    $config = new Phalcon\Config\Adapter\Json("path/config.json");
    echo $config->phalcon->baseuri;
    echo $config->models->metadata;




%{Phalcon_Config_Adapter_Json_5e4c5b6a561164dae62457a48035d6fc}%
-------

%{Phalcon_Config_Adapter_Json_c044a39a61c4cca5faedb1dcc3867532}%

%{Phalcon_Config_Adapter_Json_5848ee93bdbdd4ed7432dfc7cffbb53f}%

%{Phalcon_Config_Adapter_Json_ad184d301499efce8f3964ca6dc4bb3a}%

%{Phalcon_Config_Adapter_Json_cef9ef423d3226f1df89e6c695359390}%

.. code-block:: php

    <?php

     var_dump(isset($config['database']));





%{Phalcon_Config_Adapter_Json_3712567de59f17bfbe84e4a63effe296}%

%{Phalcon_Config_Adapter_Json_0b8c0846e2f4b6b32161721525431ec3}%

.. code-block:: php

    <?php

     echo $config->get('controllersDir', '../app/controllers/');





%{Phalcon_Config_Adapter_Json_250aecef2df720bcebe5f7cb2ae8bd78}%

%{Phalcon_Config_Adapter_Json_cb9d856c70df42bb2d3346be63f239cc}%

.. code-block:: php

    <?php

     print_r($config['database']);





%{Phalcon_Config_Adapter_Json_f2d8b16bf9da301de67f85a740661e95}%

%{Phalcon_Config_Adapter_Json_a8bce44be354ca47d470b0631b08fccf}%

.. code-block:: php

    <?php

     $config['database'] = array('type' => 'Sqlite');





%{Phalcon_Config_Adapter_Json_29871184433a14db720da0828895a713}%

%{Phalcon_Config_Adapter_Json_bee829db6cea0d7a64fd97770ff67610}%

.. code-block:: php

    <?php

     unset($config['database']);





%{Phalcon_Config_Adapter_Json_8f080354fc2bf1de28b68db6d7fd3c80|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Json_cb6ab50e7931dd69b9e01ede416ae818}%

.. code-block:: php

    <?php

    $appConfig = new Phalcon\Config(array('database' => array('host' => 'localhost')));
    $globalConfig->merge($config2);





%{Phalcon_Config_Adapter_Json_82d6f2943519bac0b9df5c24b7205c04}%

%{Phalcon_Config_Adapter_Json_12680a2b6379e22d5147df5a8a4f0267}%

.. code-block:: php

    <?php

    print_r($config->toArray());





%{Phalcon_Config_Adapter_Json_807079f12d6081992cea44ccadb3a6fb}%

%{Phalcon_Config_Adapter_Json_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Json_8a1c48ad387accfb23e5b14ab8a2204b}%

%{Phalcon_Config_Adapter_Json_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Json_68f9c10a09334876e0988c8d46977b50|:doc:`Phalcon\\Config <Phalcon_Config>`}%

%{Phalcon_Config_Adapter_Json_b23e852f6995564547321fb333c04912}%

%{Phalcon_Config_Adapter_Json_17ab47f2caca58e3466bc9754b2378df}%

%{Phalcon_Config_Adapter_Json_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Json_99bec0673999e64b60b71ba5935a345b}%

%{Phalcon_Config_Adapter_Json_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Json_6b74253a88eb7ec2b979fd16b3c976ba}%

%{Phalcon_Config_Adapter_Json_68cf0a3bff1e41b907cf955f570c5249}%

%{Phalcon_Config_Adapter_Json_45106ea4943377cec724075ab9030769}%

%{Phalcon_Config_Adapter_Json_68cf0a3bff1e41b907cf955f570c5249}%

