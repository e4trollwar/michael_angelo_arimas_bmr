%{Phalcon_Logger_56aa6e4233e90c0b8c6c16bd9b931adc}%
==================================

%{Phalcon_Logger_baa5d077b552ed3b1d73b4037110cf85}%

.. code-block:: php

    <?php

    $logger = new Phalcon\Logger\Adapter\File("app/logs/test.log");
    $logger->log("This is a message");
    $logger->log("This is an error", Phalcon\Logger::ERROR);
    $logger->error("This is another error");




%{Phalcon_Logger_738beff01dce70a909cb28318d655dcf}%
---------

%{Phalcon_Logger_7a664238f44662937d7607b42f5139a8}%

%{Phalcon_Logger_b9e8eec851ce8ae8c4a6336e19730f7c}%

%{Phalcon_Logger_feebc60c3c0167f7703bddf6cc8e2165}%

%{Phalcon_Logger_c62d871d323b665ea4aa28b4a38b6afe}%

%{Phalcon_Logger_65ac7821a0210ee33cdcac067164266a}%

%{Phalcon_Logger_cad85f628d3ec94a388ecf3728a23003}%

%{Phalcon_Logger_1ae279983babcaed9adca4bd713edcab}%

%{Phalcon_Logger_8310a3356035d6650c80c897d7adaa1b}%

%{Phalcon_Logger_af76b7989402bcb89205f34db53cebfc}%

%{Phalcon_Logger_58094a0b5c99858ca4fa240d6a31b538}%

%{Phalcon_Logger_14baeb73e242163264c32b50702c4463}%

